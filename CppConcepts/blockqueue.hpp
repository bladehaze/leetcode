#include <condition_variable>
#include <queue>
#include <mutex>
using namespace std;

template<typename T>
class blockqueue{
private:
  queue<T> _que;
  size_t _size;
  mutex _mut;
  condition_variable _condpush;
  condition_variable _condpop;
public:
  blockqueue(size_t s){
    _que.resize(s);
  }
  void push(T x){
    unique_lock<mutex> _locker(_mut);
    _condpop.wait(_locker, [this](){return this->_que.size() < _size;});
    _que.push(x);
    _locker.unlock();
    _condpush.notify_one();
  }
  T pop(T x){
    unique_lock<mutex> locker(_mut);
    _condpush.wait(locker, [this](){return !this->_que.empty();});
    T item = _que.front();
    _que.pop();
    locker.unlock();
    _condpop.notify_one();
    return item;
  }
};
