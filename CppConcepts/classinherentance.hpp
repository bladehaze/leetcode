#include <iostream>
using namespace std;

class base{
public:
  base(){
    cout << " base constructor " << endl;
    dosth(); // dosth base would be called.
    //dosthpure(); this will give an error.
  }
  virtual ~base(){ 
  //~base(){
    cout << "base destructor " << endl;
  }
  virtual void dosthpure()=0;
  virtual void dosth(){
    cout << "base do sth" << endl;
  }
  void dosthelse(){
    cout << "base do sth else" << endl;
  }
  virtual void dosthdefault(){ cout << "what if derived doesn't implement virtual" << endl;}
};

class derived : public base{
public:
  derived(){
    cout << " derived constructor " << endl;
    dosth();
    dosthpure();
  }
  ~derived(){
    cout << " derived destructor " << endl;
  }
  void dosth(){
    cout << " derirved do sth" << endl;
  }

  void dosthelse(){
    cout << " derived do sth else" << endl;
  }
  void dosthelse() const{
    cout << " derived do sth else const" << endl;
  }
  void dosthpure(){
    cout << " derived dosthpure " << endl;
  }
};


