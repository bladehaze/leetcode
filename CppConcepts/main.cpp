#include "classinherentance.hpp"
#include "singleton.hpp"
#include <iostream>
#include "blockqueue.hpp"

using namespace std;
class emptyclass{

};
//void dosth(const singleton x){ // and also this needs to be commented out to compile, const is not overloadable.
  // this is wrong because x wouldn't be able to be passed by value
//  cout << " sth " << endl;
//}
void dosth(const singleton& x){
  // this is wrong because x wouldn't be able to be passed by value
  cout << " sth " << endl;
}

int main(){
  
  base* p = new derived(); 
  cout << "============= constructor finished ===============" << endl;
  p->dosth(); // if dosth is virtual in base, this will call derived.
  p->dosthelse();
  // dynamic cast check the type.
  derived* q = dynamic_cast<derived*> (p);
  q->dosthelse();
  q->dosthdefault();
  // const key word can be overloaded.
  const derived* r = dynamic_cast<derived*>(p);
  r->dosthelse();
  // however not the const r version
  derived* const s = dynamic_cast<derived*>(p);
  s->dosthelse();
  cout << "============= finished use, call destructor ================" << endl;
  // if base destructor is not virtual, only base destructor is called. 
  // if base destructor is virtual the destructor are called in sequence.
  delete p;
  dosth(singleton::getinstance());
  emptyclass a;
  cout << (sizeof a) << endl;
}
