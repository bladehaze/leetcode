#include <iostream>

using namespace std;

class singleton{
public:
  static singleton & getinstance(){
    static singleton S;
    return S;
  }
private:
  singleton(){
    cout << "singleton constructor called" << endl;
  }
  singleton(const singleton & x);
  singleton operator=(const singleton & x);
  //void operator=(const singleton & x){};
};
