#include<string>
#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>
#include<queue>
#include<unordered_set>
#include <stdlib.h>
using namespace std;
int main(){
  ifstream infile("input001.txt");// result: 4243395
  //ifstream infile("knapsack1.txt");// result: 2493893
  string line;
  vector<vector<int> > ss;
  while( getline(infile,line)){
    stringstream stream(line);
    vector<int> tmp;
    string tok;
    while(getline(stream, tok, ' ')){
      tmp.push_back(atoi(tok.c_str()));
    }
    ss.push_back(tmp);
  }

  int N = ss[0][0]; // num of disks
  int K = ss[0][1]; // num of poles;
  vector<int> init = ss[1];
  vector<int> target = ss[2];
  auto trans = [](vector<int> res){
    long z = 0;
    for(auto & x : res)
      z = z*10 + x;
    return z;
  };
  int targetint = trans(target);
  queue<vector<int> > que;
  queue<vector<long> > hist;
  unordered_set<int> checked;
  que.push((init));
  checked.insert(trans(init));
  
  while(!que.empty()){
    auto x = que.front(); que.pop();
    auto y = hist.front(); hist.pop();
    
    // all available moves.
    unordered_set used;
    for(int i = 0 ; i < x.size(); ++i){
      used.insert(x[i]);
      tmpx = x[i];
      for(int j = i+1; j < x.size(); ++j)
	{
	if(used.find(x[j]) == used.end())
	  {
	    x[i] = x[j];
	    int rex = trans(x);
	    if(checked.find(rex) == checked.end()){
	      checked.insert(rex);
	      que.push_back(x);
	      hist.push_back(rex);
	    }
	    x[i] = tmpx;
	  }
	}
    }

    if(trans(x) == targetint) return;
    
  }
}
