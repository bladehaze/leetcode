#include<memory>
#include<vector>
#include<algorithm>
#include<unordered_map>
using namespace std;

const int ORD = 0;
const int MAN = 1;
const int DIR = 2;
const int MAXRANK = 3;

struct Call{
  int id;
};
typedef shared_ptr<Call> callptr;

class Employee{
  int _id;
  size_t _rank;
  shared_ptr<Call> _call;
public:
  Employee(int id, size_t rk):_id(id),_rank(rk){}
  virtual bool isavailable(){return _call == nullptr;};
  virtual void takecall(callptr call){_call = call;}
  virtual size_t getrank(){return _rank;}
  virtual void clear(){_call = nullptr;}
};
typedef shared_ptr<Employee> emptr;

class Associate:public Employee{
public:
  Associate(int id):Employee(id,ORD){};
};
class Manager:public Employee{
public:
  Manager(int id):Employee(id,MAN){};
};
class Director:public Employee{
public:
  Director(int id):Employee(id,DIR){};
  void takecall(callptr call){
    Employee::takecall(call);
  }
};

class CallCenter{
  vector<emptr> _employee;
  unordered_map<int,emptr > _map;
public:
  
  bool dispatchCall(callptr call){
    for(int i = 0; i < MAXRANK; ++i){
      auto p = find_if(_employee.begin(),_employee.end(),
		       [&i](emptr x){return x->isavailable() && x->getrank()==i ;});
      if(p == _employee.end()) continue;
      (*p)->takecall(call);
      _map[call->id] = *p;
      return true;
    }
  }
  void clear(callptr call){
    auto p = _map.find(call->id);
    if( p == _map.end()) return;
    p->second->clear();
    _map.erase(p);
  }
  
};

int main(){

}
