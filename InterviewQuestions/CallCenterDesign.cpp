#include <vector> 
#include <string>
#include <algorithm>
using namespace std;

class Call{
  
};

class Employee{
  string Name;
  string PhoneNumber;
public:
  Employee(string name, string phone){
    Name = name;
    PhoneNumber = phone;
    isnotbusy = true;
  }
  bool isnotbusy;
  string getname() const {return Name;}
  virtual void handleCall(Call) = 0;
};
void Employee::handleCall(Call x){
  isnotbusy = false;
  return ;
};

class Respondent: public Employee{
  void handleCall(Call x){
  }
};

class Director : public Employee{
  void handleCall(Call x){
  }
};

class Manager : public Employee{
  void handleCall(Call x){
  }
};

class CallHandler{
public:
  static CallHandler & getinstance(vector<Employee*> respondent, vector<Employee*> managers,vector<Employee*> directors){
    static CallHandler S(respondent,managers,directors);
    return S;
  }
  bool dispatchCall(Call x){
    for(auto & p : people){
      auto z = find_if(p.begin(),p.end(),[](Employee* x){ return x->isnotbusy;});
      if(z != p.end()){
	(*z)->handleCall(x); break;
      }
    }
  }
  void AddManager(Manager* x){
    people[1].push_back(x);
  }
private:
  vector<vector<Employee*> > people;
  CallHandler(vector<Employee*> respondent, vector<Employee*> managers,vector<Employee*> directors){ 
    people.push_back(respondent);
    people.push_back(managers);
    people.push_back(directors);
  }
  CallHandler(const CallHandler& x);
  CallHandler operator=(const CallHandler& x);
  ~CallHandler(){
    for(auto & x : people){
      for(auto & p : x)
	delete p;
    }
  }
};

