
#include "questions.hpp"

using namespace std;




int main(){
  string s = "2*3+1/3";
  cout << evaluateexpression(s) << endl;

  int* z = new int(1);
  {
    s_ptr<int> sp(z);
    s_ptr<int> sp2 = sp;
    s_ptr<int> q = new int(23);
  }
  {
    cout << Fibo<60>::value << endl;
  }
  {
    cout << "1000 length " << lowestcost(1000) << endl;
  }
  {
    if(validate(string("10")))
      cout << "correct" << endl;
    else
      cout << "not correct" << endl;
  }
  {
    cout << eval("1+2*3") << endl;
  }
  {
    //    auto res = combinationpar(3,2,2);
    //for(auto &c : res)
    //  cout << c << endl;
  }

  cout << "Get first n largest sum" << endl;
  {
    vector<int> a = {5,4,1};
    vector<int> b = {5,3,1};
    auto res = getsum(a,b,5);
    for(auto& x : res){
      cout << x << endl;
    }
    vector<int> all;
    for(int i = 0 ; i < a.size(); ++i)
      for(int j = 0 ; j < b.size(); ++j)
	all.push_back(a[i]+b[j]);
    sort(all.rbegin(),all.rend());
    for(auto & x : all)
      cout << x << endl;
  }

  {
    cout <<"numneeded " << returnnum("aaaabba") << endl;
    cout <<"numneeded " << returnnum("baabbba") << endl;
    cout <<"numneeded " << returnnum("aaaabaaaa") << endl;
    cout <<"numneeded2 " << returnnum2("aaaabba") << endl;
  }

  {
    cout << "next of 0 " << getnext("11") << endl;
    cout << "next of 121 " << getnext("121") << endl;
    cout << "next of 9 " << getnext("9") << endl;
    
    auto res = getall(16,162);
    for(auto & x : res)
      cout << x << endl;
  }
  {
    int x[] = {1,2,5,6,7,8};
    cout << find_nearest(3, &x[0], 6) << endl;
    
  }
  {
    string res = countAndSay(2);
  }

}
