#include <vector>
#include <memory>
#include <unordered_map>
using namespace std;

class User{
  int id;
public:
  bool isactive();
  int getid();
};
typedef shared_ptr<User> userptr;
class Book{
  int id;
public:
  int getid();
};
typedef shared_ptr<Book> bookptr;

class UserManager{
  unordered_map<int,userptr> _alluser;
  userptr _activeuser;
public:
  void addUser(userptr user);
  void removeUser(userptr user);
  bool hasUser(userptr user);
  bool hasUser(int id);
  userptr getUser(int id);
};
typedef shared_ptr<UserManager> userManagerptr;

class Library{
public:
  bool hasBook(bookptr book);
  bookptr searchBook(int id);
  void addBook(bookptr book);
  void removeBook(bookptr book);
};
typedef shared_ptr<Library> libraryptr;

class Display{
public:
  void displayBook(bookptr book);
  void displayUser(userptr user);
};
typedef shared_ptr<Display> displayptr;

class OnlineBookReader{
  libraryptr _library;
  userManagerptr _userManager;
  displayptr _display;
  userptr _activeUser;
  bookptr _activeBook;
public:
  OnlineBookReader(libraryptr library, userManagerptr userManager, displayptr display):_library(library), _userManager(userManager),_display(display){}
  bool setActiveUser(userptr user){
    if(_activeUser != nullptr || !_userManager->hasUser(user)) return false;
    _activeUser = user;
    _display->displayUser(user);
  }
  bool setActiveBook(bookptr book){
    if(_activeUser== nullptr || !_library->hasBook(book)) return false;
    _activeBook = book;
    _display->displayBook(_activeBook);
  }
  libraryptr getLibrary(){return _library;}
  userManagerptr getUserManager(){return _userManager;}
  void resetActiveBook(){_activeBook = nullptr;}
  void resetActiveUser(){_activeUser = nullptr;}
  void reset(){resetActiveBook(); resetActiveUser();}
};

int main(){

}
