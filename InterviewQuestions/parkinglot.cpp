#include <vector>
#include <string>
#include <memory>
#include <algorithm>
#include <unordered_map>
using namespace std;

class Vehicle;
class Spot{
  shared_ptr<Vehicle> _current;
  int _location;
  int _size;
public:
  Spot(int location):_location(location){};
  bool isavailable() const {return _current == nullptr;}
  int getlocation(){return _location;}
  void park(shared_ptr<Vehicle> car){_current = car;};
  void clear(){ _current = nullptr;}
  int getsize() const {return _size;}
};
class ParkingLot{
  vector<Spot> _spots;
  unordered_map<string,vector<Spot>::iterator> _mapz;
public:
  bool canpark(shared_ptr<Vehicle> car) const;
  Spot park(shared_ptr<Vehicle> car); // this could change the instance.
  void clear(shared_ptr<Vehicle> car);
};
class Vehicle{
protected:
  string _plate;
  int _spotneeded;
public:
  Vehicle(int size, string plate):_spotneeded(size),_plate(plate){}
  std::string getplate(){return _plate;}
  bool virtual canpark(const Spot & x) const {return _spotneeded <= x.getsize();}
};
bool ParkingLot::canpark(shared_ptr<Vehicle> car) const {
  return any_of(_spots.begin(),_spots.end(),[&](Spot x){return x.isavailable() && car->canpark(x);});
}
Spot ParkingLot::park(shared_ptr<Vehicle> car){
  // here some optimization can be done, fill the small spots first, then larger spots.
  auto p = find_if(_spots.begin(),_spots.end(),[&](Spot x){return x.isavailable() && car->canpark(x);});
  p->park(car);
  _mapz[car->getplate()] = p;
  return *p;
}
void ParkingLot::clear(shared_ptr<Vehicle> car){
  auto p = _mapz.find(car->getplate());
  if(p == _mapz.end()) return;
  p->second->clear();
  _mapz.erase(p);
}
class compact : public Vehicle{
public:
  compact(string plate):Vehicle(1,plate){};
};


int main(){


}
