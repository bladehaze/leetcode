#include "questions.hpp"
#include <stdlib.h>
#include <algorithm>
#include <limits.h>
#include <stack>
#include <iostream>
using namespace std;





double evaluateexpression(string s)
{
  auto getnextindex =[](string s, int index){
    int i = index;
    while(i< s.size() && isdigit(s[i])) ++i;
    return i;
  };
  function<double(int)> func 
    =[&](int index){
    int nexti = index;
    double pre = 1.0;
    char c = '*';
    do{
      nexti = getnextindex(s,index);
      if(c=='*')
	pre *= atoi(s.substr(index,nexti-index).c_str());
      if(c=='/')
	pre /= atoi(s.substr(index,nexti-index).c_str());
      c = s[nexti++];
      index = nexti;
    }while(nexti < s.size() &&(c == '*' || c == '/'));
    if(nexti >= s.size()) return pre;
    if(c == '+') return pre + func(nexti);
    if(c == '-') return pre - func(nexti);
  };
  return func(0);
}


int longestmono(vector<int> x){
  if(x.empty()) return 0;
  if(x.size() == 1) return 1;
  vector<int> diffs;
  for(int i = 1; i < x.size(); ++i)
    diffs.push_back(x[i] - x[i-1]);
  auto count = [](int target, const vector<int>& res){
    int tmplength = 0;
    int maxlength = 0;
    for(const auto & x: res){
      if(x==target) 
	++tmplength;
      else
	tmplength = 0;
      maxlength = max(maxlength,tmplength);
    }
    return maxlength+1;
  };
  return max(count(-1,diffs),count(1,diffs));
}


int lowestcost(int n){
  vector<int> z(n+1,0);
  z[2] = 2;
  for(int i = 3; i <=n; ++i){
    int minv = INT_MAX;
    for(int j = 1; j <= i/2; ++j){
      int tmpc = z[j] + z[i-j];
      minv = min(tmpc,minv);
    }
    z[i] = i+ minv;
  }
  return z[n];
}

bool validate(const string& expr) {
    string expr0 = "(" + expr + ")";
    function<bool(int&)> isvalidnum = [&]
        (int& i){
        while(isspace(expr0[i])){++i;}
        if(!isdigit(expr0[i])) return false;
        int j = i;
        while(isdigit(expr0[j])) ++j;
        if(j-i>1 && expr0[i]=='0') return false;
        i = j;
        return true;
    };
    function<bool(string&)> isvalid
        =[&](string & s){
        if(s.size() %2 == 0) return false;
        for(int i = 0 ; i < s.size(); i+=2)
            if(s[i] != '0') return false;
        for(int i = 1 ; i < s.size(); i+=2)
            if(s[i] != '+') return false;
        return true;
    };
    
    vector<char> sta;
    int i = 0;
    while(expr0[i]!='\0'){
        if(expr0[i] == '+' || expr0[i] == '-'){
            sta.push_back('+'); ++i; continue;
        }
        if(expr0[i] == '('){sta.push_back(expr0[i++]); continue;}
        if(expr0[i] == ')'){
            string s;
            while(!sta.empty() && sta.back()!='('){
                s += sta.back();
                sta.pop_back();
            }
            if(sta.empty() || sta.back()!='('||!isvalid(s)) return false;
            sta.pop_back();
            sta.push_back('0');
            ++i;
            continue;
        }
        if(!isvalidnum(i)) return false;
        sta.push_back('0');
    }
    return sta.size()==1 && sta[0] == '0';
    
}

// union and interset of sorted array.
//有2个大小为n和m的有序数组（升序）a和b，实现2个函数arrayUnion和arrayIntersect计算它们的并集和交集，结果也必须有序。

//提示：时间复杂度O(n+m)，空间复杂度O(1)。
vector<int> arrayUnion(vector<int> &a, vector<int> &b) {
    vector<int> res;
    int ai = 0;
    int bi = 0;
    while(ai < a.size() || bi < b.size()){
        if(!res.empty() && a[ai] == res.back()){++ai;continue;}
        if(!res.empty() && b[bi] == res.back()){++bi;continue;}
        if(ai>=a.size() ){
            for(;bi < b.size();++bi)
                if(res.empty() || b[bi] != res.back())
                    res.push_back(b[bi]);
            return res;
        }
        if(bi>=b.size() ){
            for(;ai < a.size();++ai)
                if(res.empty() || a[ai] != res.back())
                    res.push_back(a[ai]);
            return res;
        }
        if(a[ai] < b[bi]) 
            res.push_back(a[ai++]);
        else
            res.push_back(b[bi++]);
    }
    return res;
}

vector<int> arrayIntersect(vector<int> &a, vector<int> &b) {
    vector<int> res;
    int ai = 0;
    int bi = 0;
    while(ai < a.size() && bi < b.size()){
        if(a[ai]==b[bi]) {
            if(res.empty() || a[ai]!=res.back())
                res.push_back(a[ai]);
            ++ai;
            continue;
        }
        if(a[ai]>b[bi])
            ++bi;
        if(a[ai]<b[bi])
            ++ai;
    }
    return res;
}

//1. infix expression evaluation of integers and + - * /; 

int eval(string input){ // infix evaluation.
  function<int(int&)> getnum = [&](int& ind){
    int num = 0;
    while(ind < input.size() && isdigit(input[ind])){
      num = num*10 + (input[ind] - '0');
      ++ind;
    }
    return num;
  };
  int pre = 0;
  vector<pair<int,char> > sta;
  char endsym = '^';
  int i = 0;
  do{
    int tmp = getnum(i);
    if(i>=input.size()){sta.push_back(make_pair(tmp,endsym)); break;}
    char c  = input[i++];
    if(c=='+' || c=='-') {sta.push_back(make_pair(tmp,c));continue;}
    // evaluate * /
    do{
      int nextz = getnum(i);
      switch(c){
      case '*':
	tmp *= nextz; break;
      case '/':
	tmp /= nextz; break;
      }
      if(i < input.size()) 
	c = input[i++];
      else
	c = endsym;
    }while(i < input.size() && c != '+' && c!='-');
    sta.push_back(make_pair(tmp,c));
  }while(i < input.size());

  char lastop = '^';
  int z = 0;
  for(auto op : sta){
    if(lastop =='^'){
      lastop = op.second;
      z = op.first;
    }else{
      switch(lastop){
      case '+':
	z += op.first; lastop = op.second;
	break;
      case '-':
	z -= op.first; lastop = op.second;
	break;
      case '@':
	break;
      }
    }
  }
  return z;
}



//有n1个()，n2个[]，n3个{}，枚举出所有的合法括号组合。
//注意不是求合法的括号个数。。。

vector<string> combinationpar(int n1,int n2,int n3){
  vector<string> res;
  function<void(int,int,int,string,vector<char>&)> func
    =[&](int l1, int l2, int l3, string s, vector<char>& cache){
    if(l1==0 && l2 == 0 && l3 ==0){
      for(int i = cache.size()-1; i>=0;--i){
	switch(cache[i]){
	case '(': s += ')'; break;
	case '[': s += ']'; break;
	case '{': s += '}'; break;
	}
      }
      res.push_back(s);
      return;
    }
    if(!cache.empty()){
      char c = cache.back();
      char rc;
      switch(c){
      case '(': rc =')'; break;
      case'[' : rc =']'; break;
      case'{' : rc ='}'; break;
      }
      cache.pop_back(); func(l1,l2,l3,s + rc,cache); cache.push_back(c);
    }
    if(l1 >0) {cache.push_back('('); func(l1-1,l2,l3,s+'(',cache); cache.pop_back();}
    if(l2 >0) {cache.push_back('['); func(l1,l2-1,l3,s+'[',cache); cache.pop_back();}
    if(l3 >0) {cache.push_back('{'); func(l1,l2,l3-1,s+'{',cache); cache.pop_back();}
  };
  vector<char> sta;
  func(n1,n2,n3,string(),sta);
  return res;

}
//21.    两个int array, size 都是n,每个array各挑一个数相加，可以得到n^2个sum,
//返回这些sum中最大的n个，O(nlogn)解法??

vector<int> getsum(vector<int> a,vector<int> b,int m){
  sort(a.begin(),a.end(),std::greater<int>());
  sort(b.begin(),b.end(),std::greater<int>());
  vector<pair<int,int> > heap;
  heap.push_back(make_pair(0,0));
  vector<int> res;
  auto comparer = [&](pair<int,int> a1, pair<int,int> a2) { 
    return (a[a1.first] + b[a1.second]) < (a[a2.first]+b[a2.second]);};
  unordered_set<int> visited;
  while(res.size() < m){
    // pop max defined by comparer.
    pop_heap(heap.begin(),heap.end(),comparer);
    auto ma = heap.back(); heap.pop_back();
    res.push_back(a[ma.first] + b[ma.second]);

    // push next possible candidates. if visited, skip.
    int key1 = (ma.first+1)*b.size() + ma.second;
    if(visited.find(key1) == visited.end() && ma.first+1 < a.size()){
      heap.push_back(make_pair(ma.first+1,ma.second));
      push_heap(heap.begin(),heap.end(),comparer);
      visited.insert(key1);
    }
    int key2 = ma.first*b.size() + ma.second + 1;
    if(visited.find(key2) == visited.end() && ma.second+1<b.size()){
      heap.push_back(make_pair(ma.first,ma.second+1));
      push_heap(heap.begin(),heap.end(),comparer);
      visited.insert(key2);
    }
  }
  return res;
}

int returnnum(string s){
  int N = s.size();
  vector<int> A(N,0); // index of last a.
  int last = 0;
  for(int i = 0 ; i < N; ++i){
    if(s[i] != 'a') 
      ++last;
    A[i] = last;
  }
  last = 0;
  int minn = min(N - A.back(),A.back());
  for(int j = N-1; j > 0; --j){//j is index of first b
    if(s[j] != 'b')
      ++last;
    minn = min(minn, A[j-1] + last);
  }
  return minn;
}
int returnnum2(string s){
  int N = s.size();
  int righta = count_if(s.begin(),s.end(),[](char c) {return c=='a';});
  int minn = min(righta,N-righta);
  int leftb = 0;
  for(int i = 0 ; i < N; ++i){
    if(s[i] == 'a') 
      --righta;
    if(s[i] == 'b')
      ++leftb;
    minn = min(minn,righta+leftb);
  }
  return minn;
}

bool needtoadd(string s){
  int i = 0;
  int j = s.size()-1;
  while(i < j)
    if(s[i++] < s[j--]) return true;
  return false;
}
string getnext(string s){
  int isodd = s.size() % 2;
  int half = stoi(s.substr(0,(s.size()+1)/2));
  string res = to_string(half+1);
  string ires = res.substr(0,res.size()-isodd);
  reverse(ires.begin(),ires.end());
  res += ires;
  if(res.size() > s.size()+1){
    res = string(s.size()+1,'0');
    res[0] = '1'; 
    res.back() = '1';
  }
  return res;
}

vector<string> getall(int m, int n){
  string nstr = to_string(n);
  vector<string> res;
  function<string(string,bool)> func = [](string s,bool addone){
    int isodd = s.size() % 2;
    int half = stoi(s.substr(0,(s.size()+1)/2));
    string res = to_string(half+(addone?1:0));
    string ires = res.substr(0,res.size()-isodd);
    reverse(ires.begin(),ires.end());
    res += ires;
    if(res.size() > s.size()+1){
      res = string(s.size()+1,'0');
      res[0] = '1'; 
      res.back() = '1';
    }
    return res;
  };
  string last = func(nstr,false);
  if(stoi(last) < n) 
    last = func(last,true);
  while(res.size() < m){
    res.push_back(last);
    last = func(last,true);
  }
  return res;
}

/*
今天面试被鄙视了，说我的代码不符合他们找的经验，：〈
反正鄙视我了，我就把这题放出来，大家看看怎么做最好，对后人也是帮助

Complete the body of the following function given it's prototype in

either C or C++.  You may change the prototype or constrain the input

as long as you justify your choices.



  /** Return the index in array data of the element closest to or

      equal to value.  The array data has exactly data_count

      elements. */



size_t find_nearest (int value, const int* data, size_t data_count){
  if(data_count == 0) throw 1;
  return min_element(data,data+data_count, [&](int x, int y){return abs(x-value) < abs(y-value);}) -data;
}

/* 

1. An online stream, with infinite numbers, tell me the most frequent number
.
2. streaming logging, 设计方案能随时求最近分钟和小时和天的top clicks.
3. how to add a counter to www.google.
4. LOCAL MINIMUM. EXTEND到N*N的ARRAY的LOCAL MINIMUM的算法.
5. scramble string. How to do it in polynomial time?
6. 给一个list of sentences, 然后找出一个pair,common words 最大。举例：

This is a good day
This is a bad day
That was good day

return 第一个和第二个句子，因为有四个common words.

7.given a text file with 3 columns -- all integers:

id,parent,weight

each line is a node, 'parent' refers to 'id' of another node.

Print out, for each node, the total weight of a subtree below this node.

Someone says 不用建树，直接边扫描边打印就好, how?
*/

/*

Input:
[a1,a2,a3...,an,b1,b2...bn]
Output: [a1,b1,a2,b2.....an,bn] , solution should be in-place. 

*/

void shuffle(vector<int> x){
  
}
// 字母压缩就是比如abbbccddeefgh，转换为a1b3c2d2e2f1g1h1
void compress(char* str){
  
}
// 2. Given a list of words, find two strings S & T such that:
//     a. S & T have no common character
//     b. S.length() * T.length() is maximized
int getmaxproduct(vector<string> words){
  sort(words.rbegin(),words.rend(),[](string a, string b){return a.size() < b.size();});
  vector<size_t> marks(words.size(),0);
  transform(words.begin(),words.end(),marks.begin(), 
	    [](string s){return accumulate(s.begin(),s.end(),0,
					   [](size_t cur, char c){return (1 << (c-'a')) | cur ;})
		;}
	    );
  for(int i = 0 ; i < words.size(); ++i){
    int ma = -1;
    for(int j = 0;  j < i; ++j){
      int k = i - j;
      if(marks[j] & marks[k]) continue;
      ma = max(ma, (int)(words[j].size()*words[k].size()));
    }
    if(ma > 0) return ma;
  }
  return -1;
}

// Given a byte array, which is an encoding of characters. Here is the rule:
//     a. If the first bit of a byte is 0, that byte stands for a one-byte
// character
//     b. If the first bit of a byte is 1, that byte and its following byte
// together stand for a two-byte character
// Now implement a function to decide if the last character is a one-byte
// character or a two-byte character
// Constraint: You must scan the byte array from the end to the start.
// Otherwise it will be very trivial

enum BTYPE{ONE,TWO,INVALID};
const static char type = 1 << 3;
BTYPE gettype(char* x, int n){
  if(n < 0 ) return INVALID;
  if(n == 0) return ONE;
  if (*x & type){
    if(!(*(x+1) & type)) return INVALID;
    if(gettype(x+2,n-2) != INVALID) return TWO;
  }else{
    if(!(*(x+1) & type)) return ONE;
    if(gettype(x+1,n-1) != INVALID) return ONE;
    if(gettype(x+2,n-2) != INVALID) return TWO;
  }
}
string countAndSay(int n) {
  string next=""; 
  string cur = "1";
  --n;
  while(n){
    auto begin = cur.begin();
    do{
      char t = *begin;
      auto begin0 = find_if(begin,cur.end(), [&t](char c){return c!= t;});
      next += to_string((int)(begin0-begin))+t;
      begin = begin0;
    }while(begin!=cur.end());
    cur = next;
    next.clear();
    --n;
  }
  return cur;
}

// generate function giving integer 0,1 with 50-50 chance from a biased function which generate 
// 1 with p probability, 0 with 1-p probability.
const double accuracy = 1e-10;
int generate(function<int()> func, double p, double p0){
  if(fabs(p-p0) < accuracy) return func();
  if(p > p0){
    if(func()==1) 
      return 1;
    else
      return generate(func, (p-p0)/(1-p0), p0);
  }else
    return 1 - generate(func, 1-p,p0);
}

