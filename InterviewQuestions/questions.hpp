#include<iostream>
#include<vector>
#include<unordered_map>
#include<unordered_set>
#include<algorithm>
using namespace std;
double evaluateexpression(string express);
vector<string> combinationpar(int n1,int n2,int n3);
/* 
3.    Write a C++ program that would find and print the first longest ascending or descending contiguous subsequence for a vector of integers. For example, given a vector with

4, 2, 1, 2, 3, 4, 3, 5, 1, 2, 4, 6, 5
      ----------
*/

int longestmono(vector<int> x);

/*
  design a smart pointer.
*/
template<typename T> 
class s_ptr{
private:
  T* _obj;
  int* _count; 
public:
  ~s_ptr(){
    if(*_count==0){
      delete _count; 
      delete _obj;
    }else
      --(*_count);
  };

  s_ptr(T* p){
    _obj = p; 
    _count = new int(0);
  };
  s_ptr(const s_ptr& p){
    _count = p._count;
    _obj = p._obj;
    ++(*_count);
  };
  s_ptr& operator=(const s_ptr& p ){
    _count = p._count;
    _obj = p;
    ++(*_count); 
  };
  T& operator*(){
    return *_obj;
  };
  T* operator->() {
    return _obj;
  };
};

// Fibonocci number.
template<int s>
struct Fibo
{
  static const long long value = Fibo<s-1>::value + Fibo<s-2>::value;
};
template<>
struct Fibo<0>{
  static const long long value = 0;
};
template<>
struct Fibo<1>{
  static const long long value = 1;
};

// Minimum cut cost problem 有一个长为
//L的木料需要割开，割的位置在一个数组里A[1...N]，从一个地方切开的cost是当前所
//切木料的长度，按不同的顺序切割，得到的total cost是不一样的，问我怎么切cost最
//小

int lowestcost(int n);



bool validate(const string& expr);
int eval(string input);
vector<int> getsum(vector<int> a,vector<int> b,int count);
int returnnum(string s);
int returnnum2(string s);
string getnext(string s);
vector<string> getall(int m, int n);
size_t find_nearest (int value, const int* data, size_t data_count);
string countAndSay(int n);
