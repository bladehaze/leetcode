

// 有个二叉数，任意节点的右子树要么null要么是leaf。现在要像这样rotate这颗树：
//     1         
//    /\
//   3  5  
// /\                   
// 7 8

// ->
//      1
//     /
//    3-5
//   /
// 7-8
// =>
//      7
//     /\
//    8 3
//      /\
//     5  1


// 要求输入是1的root,输出是7的node为root的树。
// 纠结的地方是，假如迭代的话下一层迭代返回的node怎么正确连回上层的子树。
// 唉面的四家之一准备好久一上来没想清楚解法，画了半天，就跪了。



TreeNode* rotate(TreeNode* r){
  function<TreeNode*(TreeNode*,TreeNode*,TreeNode*)> func = 
    [&](TreeNode* pre, TreeNode* nl, TreeNode* nr){
    if(nl==nullptr) return pre;
    auto nnl = nl->left;
    auto nnr = nl->right;
    nl->right = pre;
    nl->left = nr;
    return func(nl,nnl,nnr);
  };
  return func(r, r->left,r->right);
}
