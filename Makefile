main: main.cpp Solutions.cpp
	g++ -g -std=c++11 main.cpp Solutions.cpp -o leetcode.exe -I.
check_syntax:
	gcc -o nul -S ${CHK_SOURCES}
clean:
	rm leetcode.exe