#include<iostream>
#define SHOW(SS) std::cout << SS << std::endl
using namespace std;
class Base{
public:
  Base(){cout << "Base construct." << endl;};
  inline virtual void Run(){cout << "Base Run" << endl;}
  void RUNX(){SHOW("RunX");}
  virtual ~Base(){cout << "Base destruct." << endl;}
};
class Derived : public Base{
public:
  Derived(){cout << "Derived construct." << endl;}
  void Run(){SHOW("Derived RUN");}
  void RUNX(){SHOW("RUNXDERIVED");}
  ~Derived(){cout << "Derived destruct." << endl;}
};
