#include "leetsolution.hpp"
#include <unordered_map>
#include <string.h>
#include <iostream>
#include <sstream>
using namespace std;
vector<int> leetsolution::twoSum(vector<int> &numbers, int target){
  vector<int> res;
  unordered_map<int,int> mapz;
  for(int i = 0 ; i < numbers.size(); ++i)
    mapz[numbers[i]] = i;
  for(int i = 0 ; i< numbers.size(); ++i){
    if(mapz.find(target-numbers[i])!=mapz.end()){
      if(mapz[target-numbers[i]] == i) continue;
      res.push_back(i);
      res.push_back(mapz[target-numbers[i]]);
      return res;
    }
  }
}


ListNode *addTwoNumbers(ListNode *l1, ListNode *l2){
  ListNode* p = new ListNode(0);
  ListNode* res = p;
  int carry = 0;
  while(l1!= NULL && l2!=NULL){
    int z = carry + l1->val + l2->val;
    p->next = new ListNode(z%10);
    carry = z/10;
    p = p->next;
    l1 = l1->next;
    l2 = l2->next;
  }
  l1 = l1 == NULL ? l2 : l1;
  while(l1 != NULL){
    int z = carry + l1->val;
    p->next = new ListNode(z%10);
    carry = z/10;
    p = p ->next;
    l1 = l1->next;
  }
  if(carry > 0 )
    p->next = new ListNode(carry);
  return res->next;
}

#define INTLOGIC(A,B,m2,n2,m,n,mn,nn,An,Bn)	\
  if(m==1 && n ==1){				\
    if(kth == 0)				\
      return A[0] < B[0]? A[0] : B[0];		\
    if(kth == 1)				\
      return A[0] < B[0]? B[0] : A[0];		\
  }						\
  if(m2+n2+1 > kth){				\
    nn = n2;					\
    Bn = B;					\
  }else	if(m+n2<=kth)				\
    {						\
      Bn = B + n2;				\
      nn = n - n2;				\
      kth -= n2;				\
    }else{					\
    Bn = B; nn = n;				\
  }						\
  if(m2 >=kth){					\
    mn = m2+1;					\
    An = A;					\
  }else if(m2+n2 <= kth){			\
    An= A + m2;					\
    mn = m-m2;					\
    kth -= m2;					\
  }else{					\
    An = A; mn = m;				\
  }


double findMedianSortedArrays(int A[], int m, int B[], int n){
  //find kth element.
  function<int(int*,int,int*,int,int)> func =
    [&func](int* A, int m, int* B, int n, int kth){ 
    if( m ==0 ) return B[kth];
    if( n ==0 ) return A[kth];
    int m2 = m/2,n2= n/2;
    int* An; int* Bn;
    int mn, nn;

    if(A[m2] <= B[n2]){ 
      // B[n2] is with index m2+n2+1 or higher, n -1 - n2 element higher index, so m + n2 index or lower.
      // A[m2] has (n-n2) + (m-m2-1) elements with higher index than A[m2]
      // A[m2] has index m2+n2 or lower. has index m2 or higher.
      INTLOGIC(A,B,m2,n2,m,n,mn,nn,An,Bn);
    }else{
      INTLOGIC(B,A,n2,m2,n,m,nn,mn,Bn,An);
    }
    return func(An,mn,Bn,nn,kth);
  };


  int N = m+n;
  if(N%2 == 1){
    return func(&A[0],m,&B[0],n,N/2);
  }else{
    return (func(&A[0],m,&B[0],n,N/2) + func(&A[0],m,&B[0],n,(N-1)/2))/2.0;
  }

}

static int lengthOfLongestSubstring(string s){
  unordered_set<char> hasfound;
  int count = 0;
  int maxcount = 0;
  int j = 0;
  for(int i = 0 ; i < s.size(); ++i){
    if(hasfound.find(s[i]) != hasfound.end()){
      maxcount = max(maxcount, count);
      while(j<i){
	hasfound.erase(s[j]);
	count--;
	++j;
	if(s[j-1]==s[i])
	  break;
      }
    }
    hasfound.insert(s[i]);
    ++count;
  }
  return max(maxcount,count);
}
string longestPalindrome(string s){
  bool DP[s.size()][s.size()];
  memset(DP,0,s.size()*s.size()*sizeof(bool));
  int maxlength ;
  string thestring;
  for(int i = s.size() - 1; i>=0; ++i)
    for(int j = i ; j < s.size() ; ++j)
      if(s[i]==s[j] &&(j-i<2 || DP[i+1][j-1]))
	{
	 DP[i][j] = true;
	 if(j-i+1 > maxlength) thestring = s.substr(i,j-i+1);
	 maxlength = max(maxlength,j-i+1);
	}
  return thestring;
}

bool isPalindrome(int x) { // 9-28-2013
  // Start typing your C/C++ solution below
  // DO NOT write int main() function
  if(x==0) return true; // zero is Palindrome.
  if(x<0) return false; // negative number is not Palindrome, .. man.. why?
  if(x%10==0) return false; // if zero is last digit, it is definite not one. and this cause problem for 10 case.
  int reversed = 0;
  while(reversed < x){
    reversed = reversed*10+x%10;
    // if it is palindrome, reverse == x(odd) or reverse == x/10(even)
    if(reversed==x || reversed == (x/=10)) return true;
  }
  return false;
	
}

void nextPermutation(vector<int> &num) { // * test this again. 9-28-2013
  // Start typing your C/C++ solution below
  // DO NOT write int main() function
  if(num.size()<=1) return ;
  int i = num.size()-1;
  while(i-1>=0 && num[i-1]>=num[i]) --i;
  reverse(num.begin()+i,num.end());
  if(i==0) return;
  --i;
  auto x = upper_bound(num.begin()+i+1,num.end(),num[i]);
  if(x==num.end()){
    num.insert(num.end(),num[i]);
    num.erase(num.begin()+i);
  }else{
    swap(*x,num[i]);
  }
}

ListNode *partition(ListNode *head, int x){ // partition List 9-29-2013
  if(head==nullptr) return head;
  ListNode* p = new ListNode(0);
  p->next = head;
  ListNode* f = p ;
  ListNode* res = p;
  while(p->next != nullptr && p->next->val < x){
    f = f->next;
    p = p->next;
  }
  while(f->next != nullptr){
    if(f->next->val >= x){
      f = f->next;
      continue;
    }
    // insert fast->next to slow->next, and move slow -> next;
    ListNode* node = f->next;
    f->next = f ->next->next;

    node->next = p->next;
    p->next = node;
    
    p = p->next;
  }
  return res->next;
}

int canCompleteCircuitI(vector<int> &gas, vector<int> &cost) { // 9-29-2013
        // Note: The Solution object is instantiated only once and is reused by each test case.
  for(int i = 0 ; i < gas.size(); ++i)
    gas[i] -= cost[i];
  int sum = 0;
  for_each(gas.begin(),gas.end(),[&](int n ){return sum+=n;});
  
  if(sum<0) return -1;
  sum = 0;
  int j = -1;
  for(int i = 0; i < gas.size() ; ++i){
    sum += gas[i];
    if(sum < 0){
      j=i; sum = 0; continue;
    }
  }
  return j+1;
}
int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
  int sum = 0;
  int total = 0;
  int j = -1;
  for(int i = 0; i < gas.size() ; ++i){
    sum += gas[i]-cost[i];
    total += gas[i]-cost[i];
    if(sum < 0){
      j=i; sum = 0; continue;
    }
  }
  return total>=0? j+1 : -1;
}
double powI(double x, int n) {
  // Start typing your C/C++ solution below
  // DO NOT write int main() function
  x = n>0?x:(1.0/x);
  function<double(int)> func
    =[&x,&func](unsigned n){ // used to be int, that won't work.
    if(n==0) return 1.0;
    if(n%2 == 0){
      double z = func(n>>1);
      return z*z;
    }else{
      return x*func(n-1);
    }
  };
  return func(abs(n));
}

double pow(double x, int n){ //9-29-2013
  x = n>0?x:(1.0/x);
  unsigned m = abs(n); // this is important, signed integer have undefined behavior
  double res = 1.0;
  for(;m; x*=x, m>>=1){
    if(m&1){
      res *= x;
    }
  }
  return res;
}


vector<string> anagrams(vector<string> &strs){ //09-30/2013
  unordered_map<string, vector<string> > mapz;
  vector<string> res;
  for(auto &x : strs){
    auto s = x;
    sort(s.begin(),s.end());
    mapz[s].push_back(x);
  }
  for(auto &x : mapz){
    if(x.second.size()>1)
      for(auto & z : x.second)
	res.push_back(z);
  }
  return res;
}
int numDecodingsI(string s){ // number of ways to decode.09-30-2013, old solution.
  if(s.size()==0) return 0;
  vector<int> ways(s.size()+1,0);
  ways[s.size()] = 1;
  for(int i = s.size()-1;i>=0;--i){
    if(s[i]=='0') continue;
    if(i==s.size()-1) {ways[i] = 1; continue;}
    ways[i] = ways[i+1] + ((s[i]-'0')*10 + (s[i+1]-'0')<27 ? ways[i+2]:0);
  }
  return ways[0];
}
int numDecodings(string s) { // I like this better because it is clearer, 09-30-2013
  // Start typing your C/C++ solution below
  // DO NOT write int main() function
  auto isvalidcode1 = [](int z) {return z >  0 && z <=26;};
  auto isvalidcode2 = [](int z) {return z >=10 && z <=26;};
  if(s.size() <1) return 0;
  if(s[0] == '0') return 0;
  int lastn1 = 1;
  int lastn2 = 1;
  for(int i = 1; i < s.size(); ++i){
    int z = (s[i-1] - '0')*10 + s[i] - '0';
    if(!isvalidcode2(z) && !isvalidcode1(s[i]-'0')) return 0;
    if(isvalidcode2(z) && !isvalidcode1(s[i]-'0')){lastn1 = lastn2; continue;}
    if(!isvalidcode2(z) && isvalidcode1(s[i]-'0')){lastn2 = lastn1; continue;}
    lastn2 += lastn1;
    swap(lastn1,lastn2);
  }
  return lastn1;
}
ListNode *swapPairs(ListNode *head) {// 09-30-2013, no problem.
        // Start typing your C/C++ solution below
        // DO NOT write int main() function
        if(head==nullptr) return head;
        if(head->next == nullptr) return head;
        ListNode*p = head->next;
        head->next = p->next;
        p->next = head;
        head->next = swapPairs(head->next);
        return p;
    }

ListNode* merge(ListNode* a, ListNode* b){
  if(a==nullptr) return b;
  if(b==nullptr) return a;
  if(a->val > b->val) swap(a,b);
  a->next = merge(a->next,b);
  return a;
}

ListNode* mergeSort(ListNode* head){ // 9-30-2013
  if(head==nullptr) return head;
  if(head->next == nullptr) return head;
  ListNode* f = head->next;
  ListNode* s = head;
  int n = 0;
  while(f !=nullptr){
    f = f->next;
    if(n &1) s=s->next;
    ++n;
  }
  ListNode* next = s->next;
  s->next = nullptr;
  return merge(mergeSort(head), mergeSort(next));
}
int candyI(vector<int> &ratings) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
  int N = ratings.size();
  int DP[N][N+1];
  //memset(DP,0,sizeof(int)*(N)*(N+1));
  for(int i = 0; i <N; ++i)
    DP[i][0] = N+1;
  for(int i = 1; i<=N; ++i)
    DP[0][i] = i;
  
  for(int i = 1; i<N; ++i)
    {
      int lastMin = N+1;
      if(ratings[i]>ratings[i-1]){ // if ith children has higher rating
	for(int j = 1; j <=N; ++j){
	  DP[i][j] = j + lastMin;
	  lastMin = min(lastMin,DP[i-1][j]);
	}
	continue;
      }
      
      if(ratings[i]<ratings[i-1]){
	for(int j = N; j>=1; --j){
	  DP[i][j] = lastMin + j;
	  lastMin = min(lastMin,DP[i-1][j]);
	}
	continue;
      }
      for(int j = 0; j <= N; ++j)
	lastMin = min(lastMin, DP[i-1][j]);
      for(int j = 1; j <= N; ++j)
	DP[i][j] = j + lastMin;
    }
  int lastMin = N+1;
  for(int i = 1; i <= N; ++i)
    lastMin = min(DP[N-1][i],lastMin);
  return lastMin;
}
int candyII(vector<int>& ratings){
  int N = ratings.size();
  vector<int> DP(N+1);
  fill(DP.begin(),DP.end(),0);
  //memset(DP,0,(N+1)*sizeof(int));
  DP[0] = N+1;
  for(int i = 1 ; i <= N; ++i){
    DP[i] = i;
  }
  for(int c = 1; c < N; ++c){
    int lastMin = N+1;
    if(ratings[c] > ratings[c-1]){ // higher rating.
      for(int i = 1; i <=N; ++i){
	lastMin = min(lastMin, DP[i-1]);
	DP[i] = i + lastMin;
      }
      continue;
    }
    if(ratings[c] < ratings[c-1]){ // lower rating.
      for(int i = N-1; i >=0; --i){
	lastMin = min(lastMin, DP[i+1]);
	DP[i] = i + lastMin;
      }
      DP[0] = DP[N] = N+1;
      continue;
    }
    // equal rating.
    for(int i = 1; i <= N; ++i){
      lastMin = min(lastMin,DP[i]);
    }
    for(int i = 1; i <= N; ++i){
      DP[i] = i + lastMin;
    }
  }
  auto z = min_element(DP.begin(),DP.end());
  return *z;
}
int candy(vector<int>& ratings){
  int N = ratings.size();
  if(N<=1) return N;
  vector<int> DP(N);
  DP[0] = 1;
  for(int i = 1; i < N ; ++i){
    if(ratings[i] > ratings[i-1]) {
      DP[i] = DP[i-1] + 1;
    }else{
      DP[i] = 1;
    }
  }
  for(int i = N-2; i >= 0 ; --i){
    if(ratings[i] > ratings[i+1]){
      DP[i] = max(DP[i+1] + 1,DP[i]);
    }
  }
  int sum = 0;
  for(int i = 0 ; i < N; ++i)
    sum += DP[i];
  return sum;
}
int singleNumberI(int A[], int n) {
  int i = 0 ; 
  while(i < n){
    int j;
    for(j = i+1 ; j < n; ++j){
      if(A[j] != A[i]) continue;
      swap(A[i+1],A[j]);
      i += 2; 
      break;
    }
    if(j==n) return A[i];
  }
}
int singleNumber(int A[], int n) {
  int res = 0;
  for(int i = 0 ; i < n ; ++i)
    res ^= A[i];
  return res ^ 0;
}


int singleNumber3(int A[], int n) { // 10-03-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  int sum0 = 0;
  int sum1 = 0;
  for(int i = 0 ; i < n ; ++i)
    {
      sum1 ^= sum0 & A[i];
      sum0 ^= A[i];
      unsigned mask = (sum1 ^ sum0);
      sum0 &= mask; 
      sum1 &= mask; 
    }
  return sum0 ^ sum1;
}

vector<string> letterCombinations(string digits) {
        // Note: The Solution object is instantiated only once and is reused by each test case.
  vector<string> listx = {"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
  vector<string> res;
  function<void(int,string)> func = [&](int index,string history){
    if(index == digits.size()){res.push_back(history); return;}
    int x = digits[index] - '0' - 2;
    for(const auto & c : listx[x]){
      history.push_back(c);
      func(index+1,history);
      history.pop_back();
    }
  };
  string his;
  func(0,his);
  return res;
}

int removeDuplicates(int A[], int n) { //10-11-2013
  if(n<=2) return n;
  int slow,fast;
  int last = A[0];
  slow = fast = 1;
  int count = 1;
  while(fast < n){
    if(A[fast++] == last)
      ++count;
    else{
      count = 1;
      last = A[fast-1];
    }
    if(count > 2 )  continue;
    A[slow++] = last;
  }
  return slow;
}
vector<string> restoreIpAddresses(string s) {//10-13-2013
  vector<string> res;
  function<void(int,int,string)> func = [&](int index,int length, string hist){
    if(length > 4) return ;
    if(index == s.size() && 4 == length ){res.push_back(hist); return;}
    for(int l = 1; l <= 3; ++l){
      if(s[index] == '0' && l>1) continue;
      if(index+l>s.size()) continue;
      if(atoi(s.substr(index,l).c_str()) >= 256) continue;
      func(index+l,length+1,hist.empty()? (hist+s.substr(index,l)): (hist+"."+s.substr(index,l)));
    }
  };
  string ss;
  func(0,0,ss);
  return res;
}
ListNode *reverseKGroupII(ListNode *head, int k) {// const space solution, the old recursion solution is not.
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  function<ListNode*(ListNode*, int)> reverseKafter 
    =[](ListNode* r, int n)->ListNode*{
    if(n<=1 || r==nullptr || r->next==nullptr) return nullptr;
    int count = 0;
    ListNode* p =r;
    while(p!=nullptr){
      p=p->next;
      ++count;
      if(count == n) break;
    }
    if(count < n || p==nullptr) return nullptr;
    ListNode* res;
    int i = 0;
    while(r->next != p){
      ListNode* q = r->next;
      r->next = q->next;
      q->next = p->next;
      p->next = q;
      if(i ==0) res = q;
      ++i;
    }
    return res;
  };
  ListNode dummy(0);
  dummy.next = head;
  ListNode* p = &dummy;
  while(p!=nullptr && p->next!=nullptr){
    p = reverseKafter(p,k);
  }
  return dummy.next;
}
ListNode *reverseKGroup(ListNode *head, int k) { //10-13-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  if(head == nullptr) return head;
  if(k==1) return head;
  ListNode dummy(0);
  dummy.next = head;
  ListNode* p;
  p = &dummy;
  int count = 0;
  while(p != nullptr){
    p = p->next;
    ++count;
    if(count == k) break;
  }
  if(count < k || p==nullptr) return head; // note p==nullptr, lastbug here.
  p->next = reverseKGroup(p->next,k);
  
  while(dummy.next != p){
    ListNode* tmp = p->next;
    p->next = dummy.next;
    dummy.next = dummy.next->next;
    p->next->next = tmp;
  }
  return dummy.next;
}

vector<vector<int> > threeSum(vector<int> &num) { // 10-13-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  vector<vector<int> > res;
  if(num.size() < 3) return res;
  sort(num.begin(),num.end());
  for(int i = 0 ; i < num.size() ;++i){
    if(i!=0 && num[i]==num[i-1]) continue;
    int newtarget = -num[i];
    int l = i+1; 
    int r = num.size()-1;
    while(l < r){
      if(num[l] + num[r] == newtarget){
	res.push_back({num[i],num[l],num[r]});
	int lx = num[l];
	while(l < num.size() && num[l] == lx) ++l;
	int rx = num[r];
	while(r >= 0  && num[r] == rx) --r;
      }else if(num[l] + num[r] < newtarget){
	++l;
      }else{
	--r;
      }
    }
  }
  return res;
}


vector<vector<string> > solveNQueens(int n) { // a way faster solution. 208 ms vs 60 ms.
  // Note: The Solution object is instantiated only once and is reused by each test case.
  unsigned int biz = 1;
  queue<vector<unsigned int> > que;
  que.push(vector<unsigned int>());
  unsigned int filter = (1 << n)-1;
  auto getvalid = [&](const vector<unsigned int>& hist){
    unsigned int mark = 0;
    for(int i = 0; i < hist.size(); ++i){
      unsigned int shift = hist.size()-i;
      mark |= hist[i];
      mark |= hist[i] << shift;
      mark |= hist[i] >> shift;
    }
    return (mark & filter)^filter; // 1s are valide position. 
  };
  vector<vector<unsigned int> > res;
  while(!que.empty()){
    vector<unsigned int> l = que.front(); que.pop();
    if(l.size() == n) {res.push_back(l); continue;}
    unsigned int m = getvalid(l);
    unsigned int z = 1;
    for(int i = 0 ; i < n ; ++i){
      if(z&m) {
	l.push_back(z);
	que.push(l);
	l.pop_back();
      }
      z <<=1;
    }
  }
  vector<vector<string> > boards;
  for(auto & x : res){
    vector<string> tmp;
    for(auto & y : x){
      unsigned int z = 1;
      string r;
      for(int i = 0 ; i < n; ++i){
	r.push_back(z&y ? '.' : 'Q');
	z <<=1;
      }
      tmp.push_back(r);
    }
    boards.push_back(tmp);
  }
  return boards;
  
}


vector<vector<int> > subsetsWithDup(vector<int> &S) {//10-16-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  vector<vector<int> > res;
  sort(S.begin(),S.end());
  
  function<void(int,vector<int>&)> func
    =[&](int index, vector<int>& hist){
    //if(index == S.size()) return;
    res.push_back(hist);
    for(int i = index; i < S.size(); ++i){
      if(i!=index && S[i] == S[i-1]) continue;
      hist.push_back(S[i]);
      func(i+1,hist); //use i, use index is wrong, last bug here.
      hist.pop_back();
    }
  };
  vector<int> tmp;
  func(0,tmp);
  return res;
}

vector<Interval> maxoverlap(vector<Interval> inputs){ // find the region has the maximum interval overlap, 
  typedef pair<int,int> ENDS;
  // all end points sort.
  vector<ENDS> preends;
  for(const auto & x: inputs){
    preends.push_back(make_pair(1,x.start));
    preends.push_back(make_pair(-1,x.end));
  }
  sort(preends.begin(),preends.end(),[](ENDS a, ENDS b){
      return a.second < b.second; });
  
  //combine same end points.
  vector<ENDS> ends;
  int i = 0; 
  while(i < preends.size()){
    if(ends.empty() || preends[i].second != ends.back().second)
      ends.push_back(preends[i]); 
    else
      ends.back().first += preends[i].first;
    ++i;
  }
  //count to find the maxmum interval
  vector<vector<ENDS> > marks;
  int sum = 0;
  for(int i = 0 ; i < ends.size(); ++i){
    if(ends[i].first==0) continue;
    sum += ends[i].first;
    if(ends[i].first>0){
      while(marks.size() < sum)
	marks.push_back(vector<ENDS>());
      marks[sum-1].push_back(ends[i]);
    }
    else
      marks[sum-ends[i].first-1].push_back(ends[i]);
  }
  // output.
  vector<Interval> res;
  for(int i = 0 ; i < marks.back().size()/2; ++i){
    res.push_back(Interval(marks.back()[2*i].second,marks.back()[2*i+1].second)); 
  }
  return res;
}


string multiplyold(string num1, string num2) {//10-18-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  reverse(num1.begin(),num1.end());
  reverse(num2.begin(),num2.end());
  string resx;
  int res = 0 ;
  for(int i = 0 ; i < num1.size() + num2.size()-1; ++i){
    for(int j = 0 ; j <= i; ++j)
      if(j <num1.size() && (i-j) <num2.size())
	res += (num1[j]-'0')*(num2[i-j]-'0');
    resx.push_back((res%10) +'0');
    res /= 10;
  }
  string head = to_string(res);
  reverse(head.begin(),head.end());
  resx.insert(resx.end(),head.begin(),head.end());
  while(resx.size() > 1 && resx.back()=='0')
    resx.pop_back();
  reverse(resx.begin(),resx.end());
  return resx;
}
string multiply(string num1, string num2) {
  // Note: The Solution object is instantiated only once and is reused by each test case.
  reverse(num1.begin(),num1.end());
  reverse(num2.begin(),num2.end());
  string res;
  int sum = 0;
  for(int i = 0 ; i < num1.size() + num2.size()-1; ++i){
    for(int j = 0; j <= i ; ++j)
      if(j < num1.size() && (i-j) < num2.size())
	sum += (num1[j]-'0') *(num2[i-j]-'0');
    res.push_back(sum%10 +'0');
    sum /= 10;
  }
  string rest = to_string(sum);
  reverse(rest.begin(),rest.end());
  res.insert(res.end(),rest.begin(),rest.end());
  while(res.size() > 1 && res.back()=='0')
    res.pop_back();
  reverse(res.begin(),res.end());
  return res;
}

int divide(int dividend, int divisor){ //10-20-2013
  long long a = abs((double)dividend);;
  long long b = abs((double)divisor);
  
  long long ret = 0;
  while (a >= b) {
    long long c = b;
    for (int i = 0; a >= c; ++i, c <<= 1) {
      a -= c;
      ret += 1 << i;
    }
  }
  
  return ((dividend^divisor)>>31) ? (-ret) : (ret);
  
}
string simplifyPath(string path) { //10-20-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  string tmp;
  stringstream ss(path);
  vector<string> res;
  while(getline(ss,tmp,'/')){
    if(tmp.empty() || tmp == ".") continue;
    if(tmp == ".."){
      if(!res.empty())
	res.pop_back();
      continue;
    }
    res.push_back(tmp);
  }
  return res.empty()?"/":accumulate(res.begin(),res.end(),string(),[](string cur, string x) {return cur + "/" + x ;});
}


vector<int> inorderTraversal(TreeNode *root) { //10-20-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  vector<int> res;
  if(root==nullptr)return res;
  stack<TreeNode*> sta;
  unordered_set<TreeNode*> set;
  sta.push(root);
  while(!sta.empty()){
    if(sta.top()->left != nullptr && set.find(sta.top()->left)==set.end()){
      sta.push(sta.top()->left); continue;
    }
    if(set.find(sta.top())==set.end()){
      res.push_back(sta.top()->val);
      set.insert(sta.top());
      continue;
    }
    if(sta.top()->right!=nullptr && set.find(sta.top()->right) == set.end()){
      sta.push(sta.top()->right); continue;
    }
    sta.pop();
  }
  return res;
}
int firstMissingPositive(int A[], int n) { //10-21-2013
  int i = 0;
  while(i < n){
    if(A[i]-1 < n && A[i]-1 >=0 && A[A[i]-1] != A[i])
      {swap(A[A[i]-1],A[i]);continue;}
    ++i;
  }
  for(i =0 ; i < n; ++i)
    if(A[i]!=i+1)
      return i+1;
  return i+1;
}
int minCut(string s){//10-21-2013
  int len = s.size();
  int DP[len+1];
  for(int i = 0 ; i <=len ; ++i)
    DP[i] = len-i;
  bool palin[len][len];
  memset(palin,0,len*len*sizeof(bool));
  for(int i = len-1; i >=0 ;--i)
    for(int j = i ; j < len; ++j)
      if(s[i] == s[j] &&(j-i < 2 || palin[i+1][j-1])){
	palin[i][j] = true;
	DP[i] = min(DP[i],DP[j+1]+1);
      }
  return DP[0]-1;
}
vector<int> searchRange(int A[], int n, int target) {//10-21-2013
  int i,j;
  i =0 ; j = n-1; // lower
  int lower = -1;
  while(i <= j){
    int m = i + (j-i)/2;
    if(A[m] == target){
      lower = m;  
      if(m==0 || A[m-1] != target) break;
      goto LEFT;
    }
    if(A[m] > target) goto LEFT;
    if(A[m] < target) goto RIGHT;
  LEFT:
    j = m-1; continue;
  RIGHT:
    i = m+1; continue;
  }
  
  i =0 ; j = n-1; // high
  int higher = -1;
  while(i <= j){
    int m = j -(j-i)/2;
    if(A[m] == target){
      higher = m;  
      if(m==n-1 || A[m+1] != target) break;
      goto RIGHT0;
    }
    if(A[m] > target) goto LEFT0;
    if(A[m] < target) goto RIGHT0;
  LEFT0:
    j = m-1; continue;
  RIGHT0:
    i = m+1; continue;
  }
  
  return vector<int>({lower,higher});
}
int romanToInt(string s) {//10-22-2013
  vector<int> nums = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
  vector<string> sym = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
  int res = 0;
  int start = 0;
  while(start < s.size()){
    for(int i = 0; i < sym.size(); ++i)
      if(s.substr(start,1) == sym[i] || (start+1 < s.size() && s.substr(start,2) == sym[i])){
	res += nums[i];
	start += sym[i].size();
	break;
      }
  }
  return res;
}
string intToRoman(int num) { //10-22-2013
  vector<int> nums = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
  vector<string> sym = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
  string res = 0; 
  int index = 0;
  while(num > 0){
    if(num >= nums[index]){
      res += sym[index];
      num -= nums[index];
    }else{
      ++index;
    }
  }
  return res;
}
int atoi(const char *str) { //10-22-2013
  while(*str != '\0' && (isspace(*str))) ++str; // delete white space.
  
  int sign = *str == '-' ? -1 : 1; // get sign
  if(*str == '+' || *str =='-') ++str;
  
  long long z = 0;
  while((*str != '\0') && isdigit(*str) ){
    z = z*10 + sign*(*str-'0');
    ++str;
  }
  if(z > (long)INT_MAX) return INT_MAX;
  if(z < (long)INT_MIN) return INT_MIN;
  return z;
}
bool search(int A[], int n, int target) { // 10-22-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  int i = 0; int j = n-1;
  while(i <= j){
    int m = (i+j)/2;
    if(A[m] == target) return true;
    if(A[i] < A[m]){ // left sorted.
      if(A[m] > target && A[i] <= target)
	j = m-1;
      else
	i = m+1;
    }else if(A[i] > A[m]){ // right sorted.
      if(A[m] < target && A[j] >= target)
	i = m+1;
      else
	j = m-1;
    }else
      ++i;
  }
  return false;
}
string minWindow(string S, string T) { //10-22-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  unordered_map<char,int> needtofind;
  for(int i = 0 ; i < T.size(); ++i)
    needtofind[T[i]] = 0;
  for(int i = 0 ; i < T.size(); ++i)
    needtofind[T[i]] +=1;
  
  unordered_map<char,int> hasfound;
  for(auto & c: S)
    hasfound[c] = 0;
  
  int slow = 0;
  int targetindex = T.size()-1;
  int minlen = INT_MAX;
  string minstr;
  for(int i = 0; i <S.size(); ++i){
    hasfound[S[i]] += 1;
    if(needtofind.find(S[i]) == needtofind.end() // if char not needed or extra, 
       || needtofind[S[i]] < hasfound[S[i]])
      ++targetindex;
    if(i < targetindex) continue;
    while(needtofind.find(S[slow]) == needtofind.end() // remove extra chars. 
	  || hasfound[S[slow]] > needtofind[S[slow]])
      --hasfound[S[slow++]];
    if(i - slow + 1 < minlen){
      minlen = i - slow + 1;
      minstr = S.substr(slow,i-slow+1);
    }
    --hasfound[S[slow++]];
    ++targetindex;
  }
  return minstr;
}
void connectI(TreeLinkNode *root) { //10-22-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  if(root==nullptr || root->left == nullptr || root->right==nullptr) return ;
  root->left->next = root->right;
  if(root->next!=nullptr)
    root->right->next = root->next->left;
  connectI(root->left);
  connectI(root->right);
}

void connectIa(TreeLinkNode *root) {//10-22-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  while(root != nullptr){
    auto cross = root;
    while(cross != nullptr){
      if(cross->left != nullptr)
	cross->left->next = cross->right;
      if(cross->next != nullptr && cross->right!=nullptr)
	cross->right->next = cross->next->left;
      cross = cross->next;
    }
    root = root->left;
  }
}

void connectII(TreeLinkNode *root) {//10-22-2013
  // Note: The Solution object is instantiated only once and is reused by each test case.
  while(root != nullptr){
    TreeLinkNode* next= nullptr;
    TreeLinkNode* pre = nullptr;
    for(; root!=nullptr; root = root->next){
      if(root->left == nullptr && root->right==nullptr) continue;
      if(next == nullptr ){
	next = root->left == nullptr? root->right:root->left;
      }
      if(root->left!=nullptr){
	if(pre!=nullptr) pre->next = root->left;
	pre = root->left;
      }
      if(root->right!=nullptr){
	if(pre!=nullptr) pre->next = root->right;
	pre = root->right;
      }
    }
    root = next;
  }
}

void reorderList(ListNode *head) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  if(head==nullptr) return;
  function<void(ListNode*)> reverseafter = 
    [](ListNode* r){
    if(r==nullptr||r->next==nullptr) return;
    ListNode* f = r;
    while(f->next != nullptr) f = f->next;
    while(r!=f && r->next != f){
      ListNode* p = r->next;
      r->next = p->next;
      p->next = f->next;
      f->next = p;
    }
  };
  function<ListNode*(ListNode*,ListNode*)> mingle = [&](ListNode* f, ListNode* s){
    if(f==nullptr) return s;
    if(s==nullptr) return f;
    f->next = mingle(s,f->next);
    return f;
  };
  int count = 0;
  ListNode* p = head;
  while(p != nullptr){
    p = p->next;
    ++count;
  }
  int halfindex = (count-1)/2;
  count = 0;
  p = head;
  while(p != nullptr){
    if(count == halfindex) break;
    p=p->next;
    ++count;
  }
  reverseafter(p);
  ListNode* q = p->next;
  p->next = nullptr;
  mingle(head,q);
        
}
void rotate(vector<vector<int> > &matrix) {
  // the best solution, just needs to get the rotation transformation right once.
  int n = matrix.size();
  int J = n-(n/2);
  int I = n/2;
  for(int i = 0 ; i < I; ++i)
    for(int j = 0 ; j < J; ++j){
      int ii = i, jj = j;
      for(int k = 0 ; k < 3; ++k){
	swap(matrix[ii][jj], matrix[n-1-jj][ii]);
	int z = jj; // bug here, the value calculation needs to have a cache.
	jj = ii; 
	ii = n-1-z; 
      }
    }
}
vector<int> preorderTraversal(TreeNode *root) {
        // IMPORTANT: Please reset any member data you declared, as
        // the same Solution instance will be reused for each test case.
  if(root == nullptr) return {};
  vector<int> res;
  stack<TreeNode*> stack;
  stack.push(root);
  while(!stack.empty()){
    TreeNode* top = stack.top(); stack.pop();
    res.push_back(top->val);
    if(top->right!=nullptr) stack.push(top->right);
    if(top->left !=nullptr) stack.push(top->left);
  }
  return res;
}
vector<int> postorderTraversal(TreeNode *root) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  if(root==nullptr) return {};
  stack<TreeNode*> sta;
  sta.push(root);
  vector<int> res;
  while(!sta.empty()){
    auto x = sta.top(); sta.pop();
    res.push_back(x->val);
    if(x->left != nullptr)
      sta.push(x->left);
    if(x->right != nullptr)
      sta.push(x->right);
  }
  reverse(res.begin(),res.end());
  return res;
}

int firstMissingPositiveI(int A[], int n) { 
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  int i = 0;
  while(i < n){
    int z = A[i];
    if(z <= 0 || z >n || z == i+1){
      ++i; continue;
    }
    if(z != A[z-1])
      swap(A[i],A[z-1]);
    else
      ++i;
  }
  for(int i = 0 ; i < n; ++i)
    if(i+1!=A[i]) return i+1;
  return n+1;
}

ListNode *reverseBetween(ListNode *head, int m, int n) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  ListNode dummy(0);
  dummy.next = head;
  ListNode* slow = &dummy;
  ListNode* fast = &dummy;
  int s = 0;
  while(fast!= nullptr && fast->next != nullptr){
    ++s;
    if(s <= n)
      fast = fast->next;
    if(s < m)
      slow = slow->next;
    if(s==n){
      while(slow->next != fast){
	ListNode* p = slow->next;
	slow->next = p->next;
	p->next = fast->next;
	fast->next = p;
      }
      break;
    }
  }
  return dummy.next;
}


bool isBalanced(TreeNode *root) {
  function<int(TreeNode*)> func = [&](TreeNode* ro){
    if(ro==nullptr) return 0;
    int l = func(ro->left);
    int r = func(ro->right);
    if(abs(l-r)>1 || l < 0 || r < 0) return -1;
    return max(l,r) + 1;
  };
  return func(root) >=0;
}

vector<int> inorderTraversalI(TreeNode *root) {
  // morris traversal
  vector<int> res;
  TreeNode* pre = nullptr;
  while(root){
    if(!root->left){ // if left is null root is left most node. go to right node.
      res.push_back(root->val); 
      root = root->right ;
    }else{          // if left is not null, append root to the right most node's right child of the left tree.
      pre = root->left; 
      while(pre->right!=nullptr && pre->right!=root) // either we can actually append, or there is a circle,
	pre = pre->right;
      if(pre->right == root){ // if there is a circle, un-wind, the current root's left nodes has finished traversing, push root value go to right.
	pre->right = nullptr;
	res.push_back(root->val);
	root = root->right;
      }else{    // if we can append, append, go to left.
	pre->right = root;
	root = root->left;
      }
    }
  }// this algorithm garanteed that root->right is not nullptr if the travsal hasn't finished.
  return res;
}
vector<string> wordBreak(string s, unordered_set<string> &dict) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
        
  if(s.empty()) return {string()};
  bool dp[s.size()+1];
  memset(dp,0,sizeof dp);
  dp[s.size()] = true;
  for(int i = s.size()-1; i >= 0; --i){
    for(int j = i;j<s.size(); ++j){
      if(dp[j+1] && dict.find(s.substr(i,j-i+1)) != dict.end()){
	dp[i] = true;
	break;
      }
    }
  }
  // doens't have a solution.
  if(!dp[0]) return {string()};
  vector<string> res;
  function<void(int,string)> func = [&]
    (int ind, string hist){
    if(ind == s.size()){res.push_back(hist); return;}
    for(int i = ind+1; i <= s.size(); ++i){
      if(!dp[i] || dict.find(s.substr(ind,i-ind)) == dict.end()) continue;
      func(i, hist + (hist.empty()?"":" ") + s.substr(ind,i-ind));
    }
  };
  func(0,"");
  return res;



  queue<pair<string,int> > que;
  que.push(make_pair(string(),0));
  while(!que.empty()){
    auto x = que.front(); que.pop();
    string t = x.first;
    int ind = x.second;
    if(ind == s.size()){res.push_back(x.first); continue;}
    for(int i = ind+1; i <= s.size(); ++i){
      if(!dp[i]) continue;
      string tx = s.substr(ind,i-ind);
      if(dict.find(tx) == dict.end()) continue;
      t += (t.empty()?"":" ") + tx;
      que.push(make_pair(t,i));
    }
  }
}

int minDistance(string word1, string word2) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  int dp[word1.size()+1][word2.size()+1];
  for(int i = 0; i <= word1.size(); ++i)
    dp[i][0] = i;
  for(int i = 0; i <= word2.size(); ++i)
    dp[0][i] = i;
  for(int j = 1; j <= word2.size(); ++j){
    for(int i = 1; i <= word1.size(); ++i){
      if(word1[i-1] == word2[j-1]){
	dp[i][j] = min({dp[i-1][j-1]  , dp[i][j-1]+1, dp[i-1][j]+1  }); 
	cout << dp[i][j] << endl;
      }
      else{ 
	dp[i][j] = min({dp[i-1][j-1]+1, dp[i-1][j]+1, dp[i][(j-1)]+1}); 
      }
    }
  }
  dp[word1.size()][word2.size()];
}


vector<int> findSubstring(string S, vector<string> &L) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  vector<int> res;
  if(L.empty()) return res;
  int len = L.back().size();
  int totallen = len * L.size();
  if(S.size() < totallen) return res;
  // try to avoid the usage of substr function.
  unordered_map<int,int> mapz;
  unordered_map<string,int> mapi;
  for(int i = 0 ; i < L.size(); ++i)
    mapi[L[i]] = i;
  for(auto & s : L)
    ++mapz[mapi[s]];    
        
  vector<int> mark(S.size(),-1); // mark string at each location, -1 if not in the set.
  for(int i = 0 ; i <= S.size()-len; ++i){
    string tmpstr = S.substr(i,len);
    if(mapi.find(tmpstr)!= mapi.end())
      mark[i] = mapi[tmpstr]; 
  }
        
  function<bool(int,int,unordered_map<int,int>&)> func = 
    [&](int ind,int count,unordered_map<int,int> & ma){
    if(count == L.size()) return true;
    if(mark[ind] < 0) return false; // not in set.
    if(ma[mark[ind]] == 0 ) return false; // already deducted.
    --ma[mark[ind]];
    return func(ind+len,count+1,ma);
  };
  for(int i = 0 ; i <= (S.size() - totallen); ++i){
    auto tmpmap = mapz;
    if(func(i,0,tmpmap))
      res.push_back(i);
  }
  return res;
}
vector<vector<int> > fourSum(vector<int> &num, int target) {
  sort(num.begin(),num.end());
  vector<vector<int> > res;
  for(int i = 0 ; i < num.size(); ++i){
    if(i !=0 && num[i] == num[i-1]) continue;
    for(int j = i+1 ; j < num.size(); ++j){
      if(j!=i+1 && num[j] == num[j-1]) continue;
      int ntarget = target - num[i] - num[j];
      int k = j+1; 
      int l = num.size()-1;
      while(k < l){
	int mtarget = num[k] + num[l];
	if(mtarget == ntarget){
	  res.push_back({num[i],num[j],num[k],num[l]});
	  int p = k;
	  while(num[p] == num[k]) ++k;
	}
	if(mtarget < ntarget) ++k; // if 2sum is less than target, 
	if(mtarget > ntarget) --l;
      }
    }
  }
  return res;
}

void solveSudokuII(vector<vector<char> > &board) {
  // IMPORTANT: Please reset any member data you declared, as
  // the same Solution instance will be reused for each test case.
  function<bool(int,int)> checkindex = [&board](int i,int j){
    //check row;
    int mark = 0;
    for(int l = 0 ; l < 9; ++l){
      if(board[i][l] == '.') continue;
      int c = board[i][l] - '0';
      if(mark & (1 << c)) return false;
      mark ^= (1<<c);
    }
    mark = 0;
    for(int l = 0 ; l < 9; ++l){
      if(board[l][i] == '.') continue;
      int c = board[l][i] - '0';
      if(mark & (1 << c)) return false;
      mark ^= (1<<c);
    }
    mark = 0;
    int I = (i/3)*3;
    int J = (j/3)*3;
    for(int l = 0 ; l < 9; ++l){
      if(board[I + l/3][J+l%3] == '.') continue;
      int c = board[I + l/3][J+l%3] - '0';
      if(mark & (1 << c)) return false;
      mark ^= (1 << c);
    }
    return true;
  };
  function<bool(int)> func = [&]
    (int l){
    if(l == 81) return true;
    int i = l / 9;
    int j = l % 9;
    if(board[i][j] != '.') return func(l+1);
    for(char c = '1' ; c <= '9' ; ++c){
      board[i][j] = c;
      if(!checkindex(i,j)) continue;
      if(func(l+1)) return true;
      board[i][j] = '.';
    }
    return false;
  };
  func(0);
}

int maxPoints(vector<Point> &points) {
  if(points.size()<=2) 
    return points.size();
  struct line{
    int m;
    int n;
    int p;
    int q;
    line(int mx,int nx,int px,int qx):m(mx),n(nx),p(px),q(qx){}
  };

  struct hashline{
    size_t operator()(line const& l) const { 
      return hash<int>()(l.m) ^ (hash<int>()(l.n) + 1) ^(hash<int>()(l.p) + 2)^(hash<int>()(l.q)+3);
      //return hash<int>(l.m);
    }
  };
  struct linecomp{
    bool operator()(line const& z, line const& x) const{
      return z.m == x.m && z.n == x.n && z.p == x.p && z.q == x.q;
    }
  };


  function<int(int,int)> gcd = [&](int x, int y){
    if(y == 0) return x;
    x %=y;
    return gcd(y,x);
  };
  auto toline = [&gcd](pair<Point,Point> linex){
    int x = linex.first.x - linex.second.x;
    int y = linex.first.y - linex.second.y;
    int m,n,p,q;//m/n is slope, p/q is x-intercept.
    m = n = p = q = 0;
    if(x==0 && y != 0){
      n = 0; m = 1;
      q = 1; p = linex.first.x;
    }
    if(x!=0 && y == 0){
      n = 1; m = 0;
      q = 0; p = linex.first.y;
    }
    if(x==0 && y == 0 && linex.first.x!=0 && linex.first.y!=0){
      int z= gcd(linex.first.x,linex.first.y);
      n = 0; m = 0; p = linex.first.x/z; q = linex.first.y/z;
    }
    if(x!=0 && y != 0){
      int z = gcd(x,y);
      n = x / z;
      m = y / z;
      m = n > 0 ? m : -m;
      n = n > 0 ? n : -n;
      p = n * linex.first.y - m * linex.first.x;
      q = n;
      z = gcd(p,q);
      p /=z;
      q /=z;
      if(p==0) q = 1;
    }
    return line(m,n,p,q);
  };
  int maxz = 0;
  unordered_map<line,int,hashline,linecomp> mapz;
 
 
 
  for(int i = 0 ; i <points.size(); ++i){
     
    unordered_set<line,hashline,linecomp> sets;
    for(int j = 0; j < points.size(); ++j){
      if(i==j) continue;
      auto l = toline(make_pair(points[i],points[j]));
      sets.insert(l);
    }
    for(auto & l : sets){
      int z = ++mapz[l];
      if(maxz < z) maxz = z;
    }
  }
   
   
   
  return maxz;
}

void solve(vector<vector<char>> &board) {
  if(board.empty() || board.front().empty()) return;
  int M = board.size();
  int N = board.front().size();
  vector<pair<int,int> > d = {make_pair(1,0),make_pair(-1,0),make_pair(0,1),make_pair(0,-1)};
  function<void(int,int)> mark = [&mark,&board,&d,&M,&N](int i, int j){
    if(i < 0 || i >= M || j < 0 || j >= N) return;
    if(board[i][j] != 'O') return;
    board[i][j] = '?';
    for(auto &x : d)
      mark(i + x.first,j + x.second);
  };
  for(int i = 0 ; i < M; ++i){
    mark(i,0);
    mark(i,N-1);
  }
  for(int i = 0; i < N; ++i){
    mark(0,i);
    mark(M-1,i);
  }
  for(int i = 0; i < M; ++i){
    for(int j = 0; j < N; ++j){
      if(board[i][j] == 'O') board[i][j] = 'X';
      if(board[i][j] == '?') board[i][j] = 'O';
    }
  }
}

int maxPointsII(vector<Point> &points) {
  if(points.empty()) return 0;
  sort(points.begin(),points.end(), [](Point x, Point y){return abs(x.x) < abs(y.x) || ( abs(x.x) == abs(y.x) && abs(x.y) < abs(y.y));});
  points.push_back(Point(abs(points.back().x)+1, abs(points.back().y)+1));
  function<int(int,int)> gcd = [&](int x, int y){
    if(x > y) return gcd(y, x);
    if(x == 0) return y;
    return gcd(y % x, x);
  };
  function<tuple<int,int,int>(int,int,int)> func = [&](int i, int j, int k){
    int x = gcd(abs(i),abs(j));
    int y = gcd(abs(x),abs(k));
    if(y == 0) return tuple<int,int,int>(i,j,k);
    return i >= 0 ? tuple<int,int,int>(i/y,j/y,k/y) : tuple<int,int,int>(-i/y,-j/y,-k/y);
  };
  struct tuplehash{
    size_t operator()(tuple<int,int,int> x) const{
      return (hash<int>()(get<0>(x)) + 1) ^ (hash<int>()(get<1>(x)) + 2) ^ (hash<int>()(get<2>(x)) + 3);
    }
  };
  unordered_map<tuple<int,int,int>,int,tuplehash> mapz;
  for(int i = 0 ; i < points.size()-1; ++i){
    unordered_set<tuple<int,int,int>,tuplehash> sets;
    for(int j = 1; j < points.size(); ++j){
      int a = points[i].y - points[j].y;
      int b = points[j].x - points[i].x;
      int c = points[j].y*(points[i].x - points[j].x) - points[j].x*(points[i].y - points[j].y);
      sets.insert(func(a,b,c));
    }
    for(auto & l : sets){
      ++mapz[l];
    }
  }
        
  return max_element(mapz.begin(),mapz.end(),[](pair<tuple<int,int,int>,int> x, pair<tuple<int,int,int>,int> y){
      return x.second < y.second;
    })->second;
}
