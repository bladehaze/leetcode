#include<vector>
#include<queue>
#include<unordered_set>
#include<stdio.h>
#include<string>
#include<algorithm>
#include<limits.h>
#include<stack>
#include<list>
#include<unordered_map>
#include<iostream>
#include<functional>
using namespace std;
struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};
struct Interval {
  int start;
  int end;
  Interval() : start(0), end(0) {}
  Interval(int s, int e) : start(s), end(e) {}
};
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

struct TreeLinkNode {
 int val;
 TreeLinkNode *left, *right, *next;
 TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};

struct Point {
    int x;
    int y;
    Point() : x(0), y(0) {}
    Point(int a, int b) : x(a), y(b) {}
};

class leetsolution{
public:
  static vector<int> twoSum(vector<int> &numbers, int target);
};
static int lengthOfLongestSubstring(string s);
double findMedianSortedArrays(int A[], int m, int B[], int n);

bool isPalindrome(int x);
void nextPermutation(vector<int> &num);
int candy(vector<int> &ratings);
int singleNumber3(int A[], int n);
vector<vector<string> > solveNQueens(int n);
vector<vector<int> > subsetsWithDup(vector<int> &S);
vector<Interval> maxoverlap(vector<Interval> inputs);
string multiply(string num1, string num2);
int divide(int dividend, int divisor);
string simplifyPath(string path);
vector<int> inorderTraversal(TreeNode *root);
int romanToInt(string s);
string intToRoman(int num);
string minWindow(string S, string T);
void connectI(TreeLinkNode *root);
void connectII(TreeLinkNode *root);
void reorderList(ListNode *head);

class LRUCache{
  list<pair<int,int> >  _cache;
  unordered_map<int, list<pair<int,int> > :: iterator > _mapz;
  const int _size;
public:
  LRUCache(int capacity): _size(capacity){}

  int get(int key) {
    if(_mapz.find(key) == _mapz.end()) return -1;
    movetohead(_mapz[key]);
    return _cache.front().second;
  }

  void set(int key, int value) {
    if(_mapz.find(key) != _mapz.end()) { // 1, key already exist, update,move to front.
      _mapz[key]->second = value; 
      movetohead(_mapz[key]);
    }else{
      if(_mapz.size()== _size){ // use back element to update.
         _mapz.erase(_cache.rbegin()->first);
         *_cache.rbegin() = make_pair(key,value);
         movetohead(--_cache.end());
      }else{                    // add new element to front if still got room
        _cache.push_front(make_pair(key,value));
      }
      _mapz[key] = _cache.begin();
    }
  }

  void movetohead(list<pair<int,int> > :: iterator itor){
    if(itor != _cache.begin())
      _cache.splice(_cache.begin(), _cache, itor);
  }
};


vector<string> wordBreak(string s, unordered_set<string> &dict);
int minDistance(string word1, string word2);
vector<vector<int> > fourSum(vector<int> &num, int target);
void solveSudokuII(vector<vector<char> > &board) ;
int maxPoints(vector<Point> &points);
