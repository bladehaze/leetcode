#include<iostream>

#include "leetsolution.hpp"
#define SHOW(str) cout << str << endl;
using namespace std;
int main(){
  SHOW("median of sorted array");
  {
    int A[] = {3,4};
    int B[] = {1,2,5,6};
    double res = findMedianSortedArrays(A,2,B,4);
    SHOW("result.");
    SHOW(res);
  }
  SHOW("ispalindrom integer");{
    if(isPalindrome(123))
      cout << " yes it is palindrom integer" << endl;
    else
      cout << " no it isn't" << endl;
  }
  SHOW("nextPermutation");
  {
    vector<int> num = {2,3,1};
    nextPermutation(num);
    for(auto & x : num)
      cout << x << endl;
  }
  SHOW("power ");
  {
    cout << pow(8.88023,3) << endl;
  }
  SHOW("candy");
  {
    vector<int> ratings = {2,2,2,1};
    int res = candy(ratings);
    cout << res << endl;
  }
  SHOW("single number 3"){
    int A[4] = {2,2,3,2};
    int res = singleNumber3(A,4);
    cout << res << endl;
  }

  SHOW("NQUEENS");
  {
    auto res = solveNQueens(4);
  }

  SHOW("Get");
  {
    vector<int> data = {1,2,2};
    auto res = subsetsWithDup(data);
    cout << res.size() << endl;
    for(auto &x : res){
      for(auto &y : x)
	cout << y << " ";
      cout << endl;
    }
  }

  SHOW("Max Overlap");{
    //vector<Interval> inputs = {Interval(1,7),Interval(2,4),Interval(5,8),Interval(4,5),Interval(3,6)};
    vector<Interval> inputs = {Interval(1,7),Interval(2,4),Interval(5,8)};
    //vector<Interval> inputs = {Interval(1,7),Interval(1,7),Interval(1,7)};
    auto res = maxoverlap(inputs);
    cout << res.size() << endl;
    for(auto & x : res)
      cout << x.start << "-" << x.end << endl;
  }

  SHOW("multiple string");
  {
    string xx = multiply("29612","60594950");
    cout << xx << endl;
  }

  SHOW("divide two integer");
  {
    int z = divide(4,2);
  }
  SHOW("simpily path");{
    auto resx = simplifyPath("///sadf//");
  }
  SHOW("LRUCache");
  {
    auto ca = LRUCache(2);
    ca.set(2,1);
    ca.set(1,1);
    ca.set(2,3);
    ca.set(4,1);
    
    //    return 0;
    cout << " ..... " << endl;

    int v = ca.get(1);
    cout << v << endl;
    v = ca.get(2);
    cout << v << endl;


  }
  {
    unordered_set<string> mapz = {"a"};
    wordBreak(string("a"), mapz);

  }

  {
    cout << "mindistance " << minDistance("a","a") << endl;

  }

  {
    vector<int> v = {-2,-1,0,0,1,2};
    auto z = fourSum(v, 0);
    for(auto & r : z){
      for(auto & x : r)
	cout << x ;
      cout << endl;
    }
  }

  {
    vector<Point> points = {Point(0,0), Point(-1,-1),Point(0,0)};
    int z = maxPoints(points);
    cout << "total points #" << z << endl;
  }


  return 0;
}
