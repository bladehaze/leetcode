#include "rev3.hpp"

using namespace std;



void solve(vector<vector<char>> &board) {
  if(board.empty() || board.front().empty()) return;
  int M = board.size();
  int N = board.front().size();
  vector<pair<int,int> > d = {make_pair(1,0),make_pair(-1,0),make_pair(0,1),make_pair(0,-1)};
  function<void(int,int)> mark = [&mark,&board,&d,&M,&N](int i, int j){
    if(i < 0 || i >= M || j < 0 || j >= N) return;
    if(board[i][j] != 'O') return;
    board[i][j] = '?';
    for(auto &x : d)
      mark(i + x.first,j + x.second);
  };
  for(int i = 0 ; i < M; ++i){
    mark(i,0);
    mark(i,N-1);
  }
  for(int i = 0; i < N; ++i){
    mark(0,i);
    mark(M-1,i);
  }
  for(int i = 0; i < M; ++i){
    for(int j = 0; j < N; ++j){
      if(board[i][j] == 'O') board[i][j] = 'X';
      if(board[i][j] == '?') board[i][j] = 'O';
    }
  }
}

// elegant solution using a heap
ListNode *mergeKLists(vector<ListNode *> &lists) {
  auto comp = [](ListNode* a, ListNode* b){return a == nullptr || (b && a->val > b->val);};
  make_heap(lists.begin(),lists.end(), comp);
  ListNode dummy(0);
  ListNode* p = &dummy;
  while(!lists.empty() && lists.front()){
    p->next = lists.front();
    p = p->next;
    pop_heap(lists.begin(),lists.end(), comp);
    lists.back() = lists.back()->next;
    push_heap(lists.begin(),lists.end(), comp);
  }
  return dummy.next;
}

