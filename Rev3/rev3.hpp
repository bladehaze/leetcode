#include<vector>
#include<queue>
#include<unordered_set>
#include<stdio.h>
#include<string>
#include<algorithm>
#include<limits.h>
#include<stack>
#include<list>
#include<unordered_map>
#include<iostream>
#include<functional>
#include <string.h>
using namespace std;
    string minWindow(string S, string T) {
        unordered_map<char,int> needs;
        for(auto & x : T)
            ++needs[x];
        int ti = T.size() - 1;
        int slow = 0;
        int minl = INT_MAX;
        int ind = 0;
        unordered_map<char,int> found;
        for(int i = 0 ; i < S.size(); ++i){
            ++found[S[i]];
            char c = S[i];
            if(needs.find(c) == needs.end() || needs[c] < found[c])
                ++ti;
            if(i < ti) continue;
            while(needs.find(S[slow]) == needs.end() || found[S[slow]] > needs[S[slow]]){
                --found[S[slow++]];
            }
            ++ti;
            if(ti - slow  < minl){
                minl = ti - slow ;
                ind = slow;
            }
            //--found[slow++]; // last bug here. 
	    --found[S[slow++]]; 
	    cout << found[slow-1] << endl;
        }
        if(minl == INT_MAX) return "";
        return S.substr(ind,minl);
    }
int findnth(int ind, int A[], int m, int B[], int n){
  
  
}

double findMedianSortedArrays(int A[], int m, int B[], int n){
  

}

vector<string> fullJustify(vector<string> &words, int L) {
  vector<string> res;
  if(words.empty()) return res;
  if(L == 0) return words;
  vector<pair<int,vector<string> > > grp;
  grp.push_back(make_pair(0,vector<string>()));
  for(auto & s : words){
    if(grp.back().first + s.size() + grp.back().second.size() > L){
      grp.push_back(make_pair(s.size(),vector<string>(1,s)));
    }else{
      grp.back().second.push_back(s);
      grp.back().first += s.size();
    }
  }
  
  function<string(pair<int,vector<string> >, bool)> func = [&]
    (pair<int,vector<string> > s, bool islast){
    if(s.second.size() == 1) return s.second.front() + string(L-s.second.front().size(), ' ');
    int spac = islast? 1 : ((L - s.first)/(s.second.size() - 1));
    int res  = L - spac* (s.second.size() - 1) - s.first;
    vector<string> sp(s.second.size()-1, string(spac,' '));
    if(!islast)
      for(int i = 0; i < res; ++i)
	sp[i] += " ";
    string r;
    for(int i = 0; i < s.second.size(); ++i){
      r += s.second[i];
      if(i < sp.size()) r += sp[i];
    }
    return islast ? r + string(L-r.size(),' ') :r;
  };
        
  for(int i = 0 ; i < grp.size()-1; ++i){
    res.push_back(func(grp[i],false));
  }
  res.push_back(func(grp.back(),true));
  return res;
}


char *strStr(char *haystack, char *needle) {
  if(*haystack == '\0') return *needle == '\0' ? haystack : nullptr;
  if(strlen(haystack) < strlen(needle) ) return nullptr; // this is marginal. without would be slower still pass the test.
  int p = 0;
  while(*haystack != '\0'){
    while(*(haystack+p)!='\0' && *(needle + p) != '\0' && *(haystack + p ) == *(needle + p))
      ++p;
    if(*(needle+p) == '\0') return haystack;
    if(*(haystack+p) == '\0') return nullptr; // skip scanning if rest of the haystack is not of enough length.. last bug here.
    p = 0;
    ++haystack;
  }
  return nullptr;
}

int numDecodings(string s) {
  if(s.empty()) return 0;
  vector<int> ways(s.size()+1,0);
  ways.back() = 1;
  ways[s.size()-1] = s.back() != '0' ? 1 : 0;
  for(int i = s.size()-2; i>=0; --i){
    if(s[i] == '0'){ // if current char is '0' set to zero, last bug here.
      ways[i] = 0;
      continue;
    }
    ways[i] += s[i] == '0' ? 0 : ways[i+1];
    ways[i] += stoi(s.substr(i,2)) > 0 && stoi(s.substr(i,2)) < 27 ? ways[i+2] : 0;
  }
  return ways[0];
}
bool isPalindrome(int x) {
  if(x < 0) return false;
  if(x == 0) return true; // boundary case x == 0, bug here.
  x = x < 0 ? -x : x;
  if(x % 10 == 0) return false;
  int z = 0;
  while(z <= x){
    int d = x % 10;
    x /= 10;
    if(z == x) return true;
    z = 10 * z + d;
    if(z == x) return true;
  }
  return false;
}

char *strStr0(char *haystack, char *needle) { // KMP algorithm.
  int n = strlen(needle);
  if(n == 0) return haystack ;
  if(*haystack == '\0') return nullptr;
  vector<int> ff(n+1,0);
  for(int i = 2; i < n ; ++i){
    int j = ff[i-1]; // for this to match to some prefix, 
    while(true){
      if(*(needle+j) == *(needle + i-1)){ 
	ff[i] = j + 1; break;
      }
      if(j == 0) break;
      j = ff[j];
    }
  }
  int fi = 0;
  int hi = 0;
  while(*(haystack+hi) != '\0'){
    if(*(needle+fi) == *(haystack+hi)){
      ++fi;++hi; 
      if(fi == n ) return haystack + hi - n;
    }else if (fi > 0)
      fi = ff[fi];
    else
      ++hi;
  }
  return nullptr;


}

char *strStr(char *haystack, char *needle) {
  // KMP algorithm 
  int size = strlen(needle);
  int ff[size+1];
  memset(ff, 0, sizeof ff);
  ff[1] = 1;
  for(int i = 2; i <= size; ++i){
    int j = ff[i - 1];
    while(true){
      if(*(needle + i -1) == *(needle + j)){
	ff[i] = j+1; break;
      }else if(j == 0){
	ff[i] = 0; break;
      }else{
	j = ff[j];
      }
    }
  }
  int j = 0;
  int i = 0;
  while(*(haystack + i) != '\0'){
    if(*(haystack+i) == *(needle + j)){
      ++i; ++j;
      if(j == size) return haystack + i - j;
    }else if (j == 0){
      ++i;
    }else{
      j = ff[j];
    }
  }
  return nullptr;
}
