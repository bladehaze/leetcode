#include "Solutions.hpp"
#include <map>
#include <iostream>
// -----------------
int Solutions::reverse(int x){
  int rx = 0;
  while(x!=0){
    int dig = x%10;
    x = x/10;
    rx = rx*10+dig;
  }
  return rx;
}

// -----------------
bool Solutions::isPalindrome(int y){
  int x = y;
  int rx = 0;
  while(x!=0){
    int dig = x%10;
    x = x/10;
    rx = rx*10+dig;
  }
  rx *= y > 0? -1 : 1;
  return y==rx;
}
typedef vector<string>::iterator itors;

typedef std::string::iterator itorc;

vector<string> Solutions::lc(string digits,map<char,string> maps){
  vector<string> res0;
  if(digits.size()==0){
    res0.push_back("");
    return res0;
  }
  vector<string> res = lc(digits.substr(1),maps);
  string ss = maps[digits[0]];
  for(itors i = res.begin(); i!= res.end(); ++i){
    for(itorc j = ss.begin(); j!= ss.end(); ++j){
      res0.push_back(*j + *i);
    }
  }
  return res0;
}

vector<string> Solutions::letterCombinations(string digits){
  map<char,string> maps;
  maps['2'] = "abc";
  maps['3'] = "def";
  maps['4'] = "ghi";
  maps['5'] = "jkl";
  maps['6'] = "mno";
  maps['7'] = "pqrs";
  maps['8'] = "tuv";
  maps['9'] = "wxyz";
  return lc(digits, maps);
}

int Solutions::atoi(const char *str){
  int p = 0;
  int res = 0 ;
  int s = 1;
  char c ;
  int z = 0;
  

  while((c = *(str+ p)) != '\0'){
    if(c == ' '){
      ++p;
      //started = true;
      continue;
    }
    else
      break;
  }
  while((c = *(str+ (p++))) != '\0'){
    ++z;
    if(c == '-'){
      s = -1;
      if(z>1)
	break;
      continue;
    }
    if(c == '+'){
      if(z>1)
	break;
      continue;
    }
    if(c<'0' || c>'9')
      break;
    z++;
    //started = true;
    int t = c - '0';
    if(z>5){
    if(res!=0)
      cout << res << " " <<((int)INT_MIN-t*s)  << endl;
    
    if(res != 0 && s>0 && ((int)INT_MAX-t*s)/res <10)
      return (int)INT_MAX;
    if(res != 0 && s<0 && ((int)INT_MIN-t*s)/(res) <10)
      return (int)INT_MIN;}
    res = res*10 + t*s;
  }
  return res;
}

bool Solutions::isMatch(const char *s, const char *p){
  if(*p == '\0' ) 
    return *s == '\0';
  if(*(p+1) != '*') 
    return *s != '\0' && (*s==*p||*p=='.') && isMatch(s+1,p+1);
  return isMatch(s,p+2) 
    || ((*s != '\0' && (*s==*p||*p=='.') && isMatch(s+1,p)));
}


bool isMatch(const char *s, const char *p){
  if(p[0]=='\0') return s[0]=='\0';
  if(p[1] != '*') return (p[0] == s[0] || (p[0] == '.' && s[0] != '\0')) && isMatch(s+1,p+1);
  while(*s == *p || (*p == '.' && *s != '\0')){
    if(isMatch(s,p+2)) return true;
    ++s;
  }
  return isMatch(s,p+2);
}

vector<vector<int> > Solutions::generate(int numRows){
  vector<vector<int> > vec;
  vector<int> tmp ; 
  for(int i =0; i< numRows; ++i){
    vector<int> zz;
    zz.push_back(1);
    for(int j = 0; j<(int)tmp.size()-1; ++j){
      int zzzz = tmp[j]+tmp[j+1];
      zz.push_back(zzzz);
    }
    if(i!=0)
      zz.push_back(1);
    tmp = zz;
    vec.push_back(zz);
  }
  return vec;
}

vector<int> Solutions::getRow(int rowIndex){
  vector<int> tmp ; 
  for(int i =0; i< rowIndex; ++i){
    vector<int> zz;
    zz.push_back(1);
    for(int j = 0; j<(int)tmp.size()-1; ++j){
      int zzzz = tmp[j]+tmp[j+1];
      zz.push_back(zzzz);
    }
    if(i!=0)
      zz.push_back(1);
    tmp = zz;
  }
  return tmp;  
}
bool Solutions::isValidSudokuIII(vector<string > &board){
          int mark = 0;
        for(int i = 0 ; i < 9; ++i){
            mark = 0;
            for(int j = 0 ; j < 9; ++j){
                char c  = board[i][j];
                if (c=='.') continue;
                int num = c -'1';
                if(mark & (1<<num) ) return false;
                mark ^= num;
            }
        }

        for(int i = 0 ; i < 9; ++i){
            mark = 0;
            for(int j = 0 ; j < 9; ++j){
                char c  = board[j][i];
                if (c=='.') continue;
                int num = c -'1';
                if(mark & (1<<num)) return false;
                mark ^= num;
            }
        }
        
        for(int i = 0 ; i < 3; ++i){
            for(int j = 0 ; j < 3; ++j){
                mark = 0;
                for(int k = 0 ; k < 9; ++k){
                    char c = board[3*i + k/3][3*j + k%3];
                    if(c=='.')continue;
                    int num = c -'1';
                    if(mark & (1<<num)) return false;
                    mark ^= num;
                }
            }
        }
        return true;

}
bool Solutions::isValidSudokuII(vector<string > &board){
  auto checkone = [&board](int m, int n){
    char thisc = board[m][n];
    if(thisc=='.') return true;
    for(int i = 1 ; i < 9 ; ++i){
      char c = board[m][(i+n)%9];
      if(c==thisc) {
	//cout << "FALSE1" <<endl;
	return false;
      }
    }
    for(int i = 1 ; i < 9 ; ++i){
      char c = board[(i+m)%9][n];
      if(c==thisc){
	//cout << "FALSE2" <<endl;
	return false;
      }
    }
    int I = m / 3;
    int J = n / 3;
    for(int i = 0 ; i < 9 ; ++i){
      int x = I*3 + (i/3);
      int y = J*3 + (i%3);
      if(x== m && y == n) continue;
      if(board[x][y] == thisc) {
	//cout << "FALSE3 " <<thisc << " " <<  m <<" " << n  << " " << x << " " << y<< endl;
	return false;
      }
    }
    return true;
  };
  for(int i = 0 ; i < 9 ; ++i)
    for(int j = 0 ; j < 9 ; ++j)
      if(!checkone(i,j)) return false;
  return true;
}

bool Solutions::isValidSudoku(vector<vector<char> > &board){
  //  typedef vector<vector<char> >::iterator itor;
  auto isvalid = [](char c){return (c>= '1' && c<='9') || c =='.';};
  
  array<bool,10> myarray;
  auto checkblock  = [&](const vector<char> & row)
    {
      if(!all_of(row.begin(), row.end(), isvalid))
	return false;
      myarray.fill(false);
      for(auto & c : row){
	if(c == '.') continue;
	int ind = c - '0';
	if(!myarray[ind])
	  myarray[ind] = true;
	else
	  return false;
      }
      return true;
    };
  
  //Rule1, row consistency
  if(!all_of(board.begin(), board.end(), checkblock))
    return false;
  
  //Rule2, column consistency
  int N = board.size();
  for(int i = 0 ; i< N; ++i){
    vector<char> vec;
    for(int j = 0; j < N ; ++j){
      vec.push_back(board[j][i]);
    }
    if(!checkblock(vec)) return false;
  }
  
  //Rule3, block consistency

  // for(int i = 0; i < 9; i+=3)
  //   for(int j = 0 ; j < 9 ; j+=3)
  //     {
  // 	vector<char> vec;
  // 	for(int k = 0; k < 3 ; ++k)
  // 	  for(int l = 0; l < 3; ++l)
  // 	    vec.push_back(board[i+k][j+l]);
  // 	if(!checkblock(vec)) return false;
  //     }
  for(int i = 0; i < 9; i+=3)
    for(int j = 0 ; j < 9 ; j+=3)
      {
	vector<char> vec;
	for(int id = 0; id < 9 ; ++id)
	  vec.push_back(board[i+(id/3)][j+(id%3)]);
	if(!checkblock(vec)) return false;
      }
  
  return true;

}

int Solutions::searchInsert(int A[], int n, int target){
  int i;
  for(i = 0 ; i < n ; ++i){
    if(A[i]>=target)
      return i;
  }
  return i;
}

vector<int> Solutions::searchRange(int A[], int n, int target){
  vector<int> res(2) ;
  fill(res.begin(), res.end(), -1);
  if(n == 0)
    return res;
  if(n == 1)
    {
      if(*A==target)
	fill(res.begin(), res.end(), 0);
      return res;
    }
  int c = n/2-1;
  if(*(A+c) < target){
    int* p = A+c+1;
    n -= (c+1);
    auto pres = searchRange(p, n, target);
    if(pres[0] <0) return res;
    pres[0] += c+1;
    pres[1] += c+1;
    return pres;
  }
  if(*(A+c) > target){
    auto pres = searchRange(A, (c+1), target);
    if(pres[0] <0) return res;
    return pres;
  }
  if(*(A+c) == target){
    int* p = A+c+1;
    int m = n - (c+1);
    auto rpres = searchRange(p, m, target);
    auto lpres = searchRange(A, c+1, target);
    if(rpres[0] < 0) return lpres;
    lpres[1] = rpres[1]+c+1;
    return lpres;
  }
  return res;
}

int Solutions::search(int A[], int n, int target){// O(n) solution, 
  int i ;
  for(i =0 ; i < n ; ++i){
    if(A[i] == target) return i;
  }
  return -1;
}

ListNode* Solutions::mergeKLists(vector<ListNode *> &lists){
  ListNode* p = NULL;
  ListNode* q = NULL;
  auto findmaxindex = [](vector<ListNode *> list)
    {
      int i = -1;
      int result = INT_MAX;
      int j = 0;
      for(auto & pp : list){
	if(pp!=NULL && pp->val <= result )
	  {
	    i = j;
	    result = pp->val;
	  }
	++j;
      }
      return i;
    };
  while(any_of(lists.begin(),lists.end(),[](ListNode* p){return p !=NULL;})){
    int i ;
    if((i = findmaxindex(lists)) != -1){
      if(p==NULL)
	{
	  p = new ListNode(lists[i]->val);
	  q = p;
	}else
	{
	  q->next = new ListNode(lists[i]->val);
	  q = q->next;
	}
      lists[i] = lists[i]->next;
    }
  }
  return p;
}

bool Solutions::isValid(string s){
  stack<char> pa;
  auto changestack = [&pa](char c, char starts, char ends)
    {
      if(c!=starts && c!=ends) return true;

      if(c == starts)
	pa.push(c);
      if(c == ends)
	{
	  if(pa.size() == 0 ) 
	    return false;
	  if(pa.top() != starts)
	    return false;
	  pa.pop();
	}
      return true;
    };
  for(auto & c : s){
    if(changestack(c,'(',')')
       &&changestack(c,'[',']')
       &&changestack(c,'{','}'))
      continue;
    return false;
  }
  return pa.size()==0?true:false;
}

bool Solutions::isBalanced(TreeNode *root) {
  function<bool(int&,TreeNode*)> func;
  func = [&func](int& deepth,TreeNode* node){
    deepth = 0;
    if(node==NULL)return true;
    int leftd=0;
    int rightd=0;
    if(func(leftd,node->left) && func(rightd,node->right)){
	deepth = max(leftd,rightd)+1;
	if(abs(leftd-rightd)<=1)
	  return true;
    }
    return false;
  };
  int i;
  return func(i,root);
}

int Solutions::maxDepth(TreeNode *root) {
  function<int(TreeNode*)> func;
  func = [&func](TreeNode* node){
    if(node==NULL)return 0;
    return max(func(node->left), func(node->right))+1;
  };
  return func(root);
}

bool isSymmetricI(TreeNode *root){
  typedef map<int,int>::iterator itor;
  function<bool(TreeNode*,int, int)> check;
  map<int,int> valuemap;
  if(root!=NULL) valuemap[1] = root->val;
  check = [&valuemap,&check](TreeNode* root, int x, int mirrox){
    if(root==NULL) return true;
    itor ij;
    if((ij=valuemap.find(mirrox))!= valuemap.end()){
      if(ij->second != root->val)return false;
      valuemap.erase(ij);
    }else{
      valuemap[x] = root->val;
    }
    return check(root->left ,x<<1,((mirrox<<1) +1) )&& 
    check(root->right,((x<<1) + 1),mirrox<<1);
  };
  return check(root,1,1)&&valuemap.size()==0;
}

bool Solutions::isSymmetric(TreeNode* root){
  function<bool(TreeNode*,TreeNode*)> func 
    = [&func](TreeNode* left, TreeNode* right){
    if(left==NULL ^ right ==NULL) return false;
    if(left == NULL && right == NULL) return true;
    if(left->val != right->val) return false;
    return func(left->left, right->right) && func(left->right, right->left);
  };
  return func(root,root);
}


bool Solutions::isSameTree(TreeNode *p, TreeNode *q) {
  if((p == NULL) && ( q==NULL )) return true;
  if((p == NULL) ^  ( q==NULL )) return false;
  if(p->val != q->val) return false;
  return isSameTree(p->left,q->left) && isSameTree(p->right,q->right);
}

vector<vector<int> > Solutions::levelOrder(TreeNode *root){
  vector<vector<int> > res;
  function<void(TreeNode*,int)> doit;
  doit = [&res,&doit](TreeNode* root,int level){
    if(root==NULL) return;
    if(res.size()<level)
      {
	vector<int> tmp;
	tmp.push_back(root->val);
	res.push_back(tmp);
      }
    else{
      res[level-1].push_back(root->val);
    }
    doit(root->left,level+1);
    doit(root->right,level+1);
  };
  doit(root,1);
  return res;
}

vector<vector<int> > Solutions::zigzagLevelOrder(TreeNode *root){
  vector<vector<int> > res;
  function<void(TreeNode*,int)> doit;
  doit = [&res,&doit](TreeNode* root,int level){
    if(root==NULL) return;
    if(res.size()<level)
      {
	vector<int> tmp;
	tmp.push_back(root->val);
	res.push_back(tmp);
      }
    else{
      res[level-1].push_back(root->val);
    }
    doit(root->left,level+1);
    doit(root->right,level+1);
  };
  doit(root,1);
  int j = 0;
  for(auto& v : res){
    if(j%2 !=0)std:: reverse(v.begin(),v.end());
    ++j;
  }
  return res;
}

vector<int> Solutions::plusOne(vector<int> &digits){
  std::reverse(digits.begin(),digits.end());
  int carry = 1;
  for(auto & i : digits){
    int z = i + carry;
    carry = z/10;
    i = z%10;
    if(carry == 0) break;
  }
  if(carry!=0)
    digits.push_back(carry);
  std::reverse(digits.begin(),digits.end());
  return digits;
}

int Solutions::removeElement(int A[], int n, int elem){
  auto swap = [](int& x, int& y){x = x + y; y = x-y; x = x-y;};
  int offset = 0;
  for(int i = 0; i<n; ++i){
    if(A[i]==elem){
      ++offset;
      continue;
    }
    if(offset>0)swap(A[i],A[i-offset]);
  }
  return n-offset;
}

int Solutions::removeDuplicates(int A[], int n){
  if(n==0) return n;
  auto swap = [](int& x, int& y){x = x + y; y = x-y; x = x-y;};
  int offset = 0;
  int last = A[0];
  for(int i = 1; i < n ; ++i){
    if(A[i] == last){
      ++offset;
      continue;
    }
    last = A[i];
    if(offset>0)swap(A[i],A[i-offset]);
  }
  return n-offset;
}

int Solutions::maxPathSum(TreeNode *root){
  int maxn = INT_MIN;
  function<int(TreeNode*)> func;
  func = [&func,&maxn](TreeNode* r){
    if(r == NULL) return INT_MIN;
    int ll = func(r->left);
    int z =0;
    if(ll>INT_MIN){
      ll += r->val;
      maxn = ll>maxn?ll:maxn;
    }
    int rr = func(r->right);
    if(rr>INT_MIN){
      rr += r->val;
      maxn = rr>maxn?rr:maxn;
    }
    if(rr>INT_MIN && ll>INT_MIN)
    {
      z = -r->val + ll + rr;
      maxn = z>maxn?z:maxn;
    }
    z = r->val;
    maxn = z>maxn?z:maxn;

    int p1 = ll > rr?(ll):(rr);
    return p1 > z? p1 : z;
  };
  func(root);
  return maxn;
}

ListNode* Solutions:: removeNthFromEnd(ListNode *head, int n){
  if(head == NULL) return head;
  ListNode* firstp = head;
  ListNode* secondp = head;
  int z = 0;
  while(secondp->next!=NULL && z < n)
  {
    secondp = secondp->next;
    ++z;
  }  
  while(secondp->next!=NULL){
    secondp = secondp->next;
    firstp  = firstp ->next;
  }
  if(z<n-1) return head;
  if(z==n-1){
    head = head->next;
    delete firstp;
    return head;
  }
  ListNode* zz = firstp->next->next;
  delete firstp->next;
  firstp->next = zz;
  return head;
}

bool Solutions::isValidBST(TreeNode *root){
  function<bool(TreeNode*,int,int)> 
  func = [&func](TreeNode* r,int min, int max){
    if(r==NULL) return true;
    if(r->val>min && r->val<max){
      return func(r->left, min, r->val) &&
      func(r->right, r->val, max);
    }
    return false;
  };
  return func(root, INT_MIN,INT_MAX);
}

string addBinary(string a, string b){
  int la = a.size();
  int lb = b.size();
  std::reverse(a.begin(),a.end());
  std::reverse(b.begin(),b.end());

  int bmax = la>lb?la:lb;
  int carry = 0;

  string res;
  for(int i = 0; i < bmax ; ++i){
    int abit = 0;
    int bbit = 0;
    abit = i < la? (a[i]-'0'):0;
    bbit = i < lb? (b[i]-'0'):0;
    int z = abit + bbit + carry;
    int s = z%2;
    carry = z/2;
    res.push_back('0'+s);
  }
  if(carry>0)
    res.push_back('1');
  std::reverse(res.begin(),res.end());
  return res;
}

int Solutions::sqrt(int x) {
  auto func = [&x](double y){return (y+x/y)/2.0;};
  double x0 = x/2.0;
  double y0 = x;
  while(abs(x0-y0)>1e-10){
    y0 = func(x0);
    std::swap(y0,x0);
  }
  return y0;
}

bool Solutions::hasPathSum(TreeNode *root, int sum){
  function<bool(TreeNode*,int)> 
    func = [&func](TreeNode* root, int sum){
    if(root==NULL) return false;
    if(root->left==NULL && root->right==NULL) return sum==root->val;
    return func(root->left,sum-root->val) || func(root->right,sum-root->val);    
  };
  return root!=NULL && func(root,sum);
}

int Solutions::minDepth(TreeNode *root){
  function<int(TreeNode*)> 
    func=[&func](TreeNode* root){
    if(root==NULL) return 0;
    if(root->left==NULL && root->right==NULL) return 1;
    int l = func(root->left);
    int r = func(root->right);
    if(l==0)return r+1;
    if(r==0)return l+1;
    return min(l,r)+1;
  };
  return func(root);
}

vector<vector<int> > Solutions:: pathSum(TreeNode *root, int sum){
  vector<vector<int> > res;
  list<int> tmp;
  function<bool(TreeNode*,int)> 
    func = [&func,&res,&tmp](TreeNode* root,int sum){
    if(root==NULL) return false;
    if(root->left==NULL && root->right==NULL && sum == root->val){
      tmp.push_back(root->val);
      vector<int> tt(tmp.begin(),tmp.end());
      res.push_back(tt);
      tmp.pop_back();
    }
    tmp.push_back(root->val);
    if(!(func(root->left,sum - root->val) || func(root->right,sum-root->val)))
      {
	tmp.pop_back();
	return false;
      }
    return true;
  };
  func(root,sum);
  return res;
}

void Solutions::flatten(TreeNode *root){
  if(root==NULL) return;
  flatten(root->left);
  flatten(root->right);
  auto right = root->right;
  root->right = root->left;
  root->left = NULL;
  while(root->right !=NULL) root = root->right;
  root->right = right;
}

string Solutions::longestCommonPrefix(vector<string> &strs) {
  string s = "";
  if(strs.size()==0)return s;
  for(int i = 0 ; i < strs.front().size(); ++i){
    char c = strs[0][i];
    if(all_of(strs.begin(),strs.end(),[&c,&i](string s){return i< s.size() && s[i]==c;}))
      s.push_back(c);
    else
      break;
  }
  return s;
}

string longestCommonPrefix(vector<string> &strs) {
  string res;
  if(strs.size()==0) return res;
  int i = 0;
  while(all_of(strs.begin(),strs.end(),[&i](string str){return str.size()>i;})){
    int j = i;
    vector<char> thisrow;
    for(auto & ss : strs){
      thisrow.push_back(ss[j]);
    }
    if(!all_of(thisrow.begin(),thisrow.end(),[&thisrow](char c){return c == thisrow[0];}))
      break;
    res.push_back(thisrow[0]);
    ++i;
  }
  return res;
}

int climbStairs(int n){
  vector<int> memorization;
  memorization.push_back(1);
  memorization.push_back(2);
  function<int(int)> func = [&func,&memorization](int n){
    if(n<=memorization.size())
      return memorization[n-1];
    int z = func(n-2)+func(n-1);
    memorization.push_back(z);
    return z;
  };
  return func(n);
}

double Solutions::pow(double x, int n){
  if(n==0) return 1;
  function<double(double,int)> func
    =[&func](double x, int n){
    if(n==0) return 1.0;
    if((n & 1) == 0){ // even
      double q = func(x,n>>1);
      return q*q;
    }
    else{
      return x*func(x,n-1);
    }
  };
  if(n>0) 
    return func(x,n);
  return 1.0/(func(x,-n));

}

bool Solutions::isInterleave(string s1, string s2, string s3){
  map<pair<int,int>,bool> memo;
  typedef map<pair<int,int>,bool>::iterator itor;
  function<bool(int,int,int)> 
    func = [&func, &s1,&s2,&s3,&memo](int ai, int bi, int ci){
    if(ai >= s1.size()) return s2.substr(bi) == s3.substr(ci);
    if(bi >= s2.size()) return s1.substr(ai) == s3.substr(ci);
    pair<int,int> key(ai,bi);
    itor ij;
    if((ij = memo.find(key))!= memo.end()) return ij->second;
    bool res = (s1[ai] == s3[ci] && func(ai+1,bi,ci+1)) || (s2[bi]==s3[ci] && func(ai,bi+1,ci+1));
    memo[key] = res;
    return res;
  };
  return func(0,0,0);
}

vector<string> Solutions::restoreIpAddresses(string s){
  vector<string> res;
  std::list<string> ips;
  
  auto isvalidsubip = [](string s){ 
    if(s.size()>3) return false;
    if(s[0]=='0'&&s.size()>1)  return false;
    int z = 0;
    for(auto& c : s){z = z*10+(c-'0');}
    return z<256;
  };

  auto combine = [](list<string> x){
    std::stringstream sss;
    bool first = true;
    for(auto ss : x){
      sss<<(first?"":".")<<ss; 
      first =false;
    }
    return sss.str();
  };

  function<void(string,int)> 
    func = [&func, &res, &ips, &isvalidsubip, &combine](string s, int numcomplete){
    if(s.size() == 0 ) return ;
    if(numcomplete==3 && isvalidsubip(s))
      {
     	ips.push_back(s);
     	res.push_back(combine(ips));
     	ips.pop_back();
     	return;
      }
    int minz = s.size()>3 ? 3 : s.size();
    for(int i = 0 ; i < minz; ++i){
      string subs = s.substr(0,i+1);
      if(!isvalidsubip(subs)) continue;
      ips.push_back(subs);
      func(s.substr(i+1),numcomplete+1);
      ips.pop_back();
    }
  };
  if(s.size()<13) func(s,0);
  return res;
}

void Solutions::rotate(vector<vector<int> > &matrix) {
  int L = matrix.size();
  int M, N;
  if( (L & 1) == 0){
    M = L/2; N = L/2;
  }else{
    M = L/2+1; N = L/2;
  }
  auto rotateonce = [&L](int& x, int& y){int z = x; x = L - y - 1; y = z;};
  for(int i = 0; i < M; ++i)
    for(int j = 0; j < N; ++j){
      int x1 = i; int y1 = j ; 
      int x0 = i; int y0 = j ;
      for(int k = 0; k < 3; ++k){
	rotateonce(x1,y1);
	swap(matrix[x1][y1],matrix[x0][y0]);
	x0 = x1; y0 = y1;
      }
    }
}

ListNode * Solutions:: swapPairs(ListNode *head) {
  if(head == NULL) return head;
  if(head->next == NULL) return head;
  ListNode* p = head->next;
  head->next = swapPairs(p->next);
  p->next = head;
}

ListNode* Solutions::mergeTwoLists(ListNode *l1, ListNode *l2){
  ListNode* p = NULL;
  ListNode* q = NULL;
  vector<ListNode *> lists ;
  lists.push_back(l1);
  lists.push_back(l2);
  auto findmaxindex = [](vector<ListNode *> list)
    {
      int i = -1;
      int result = INT_MAX;
      int j = 0;
      for(auto & pp : list){
	if(pp!=NULL && pp->val <= result )
	  {
	    i = j;
	    result = pp->val;
	  }
	++j;
      }
      return i;
    };
  while(any_of(lists.begin(),lists.end(),[](ListNode* p){return p !=NULL;})){
    int i ;
    if((i = findmaxindex(lists)) != -1){
      if(p==NULL)
	{
	  p = new ListNode(lists[i]->val);
	  q = p;
	}else
	{
	  q->next = new ListNode(lists[i]->val);
	  q = q->next;
	}
      lists[i] = lists[i]->next;
    }
  }
  return p;

}
int Solutions::maxProfit(vector<int> &prices){
  int lastmin = INT_MAX;
  int maxprofit = 0;
  for(auto &x : prices){
    if(x < lastmin) lastmin = x;
    maxprofit = max(maxprofit, x - lastmin);
  }
  return maxprofit;
}

int maxProfit(vector<int> &prices){
  if(prices.size()==0) return 0; 
  int low = prices[0];
  int maxprofit = 0;
  for(auto &pr : prices){
    low = min(low, pr);
    maxprofit = max(maxprofit, pr -low);
  }
  return maxprofit;
}

int Solutions::maxProfitII(vector<int> &prices){
  if(prices.size()==0) return 0;
  int profit = 0;
  for(int i = 1 ; i < prices.size(); ++i){
    profit += max(prices[i]-prices[i-1],0);
  }
  // function<int(int,int)> func 
  //   = [&func,&prices](int i,int cum){
  //   if(i == prices.size()) 
  //     return cum;
  //   return func(i+1,max(prices[i]-prices[i-1],0)+cum);
  // }
  // return prices.size()<=1?0:func(1,0);
  return profit;
}

void Solutions::recoverTree(TreeNode *root){
  vector<int> elements;
  function<void(TreeNode*)> flatten = [&flatten,&elements]
    (TreeNode* root){
    if(root == NULL) return;
    flatten(root->left);
    elements.push_back(root->val);
    flatten(root->right);
  };
  flatten(root);
  sort(elements.begin(),elements.end());
  int index = 0;
  function<void(TreeNode*)> reassign = [&index,&elements,&reassign]
    (TreeNode* root){
    if(root == NULL) return;
    reassign(root->left);
    root->val = elements[index];
    ++index;
    reassign(root->right);
  };
  reassign(root);
}

int Solutions::minPathSum(vector<vector<int> > &grid){
  typedef map<pair<int,int>, int> :: iterator itor;
  int m = grid.size();
  int n = grid[0].size();
  map<pair<int,int>, int> memo;
  function<int(int,int)> func = 
    [&func,&grid,&memo,&m,&n](int row,int col){
    pair<int,int> key(row,col);
    itor ij;
    if((ij = memo.find(key))!= memo.end()) return ij->second;
    int z = min(row+1<m ? func(row+1,col):INT_MAX, col+1<n? func(row,col+1):INT_MAX);
    z = z==INT_MAX? grid[row][col] : (grid[row][col] + z);
    memo[key] = z;
    return z;
  };
  return func(0,0);
}

int Solutions::uniquePaths(int m, int n){
  double res =1.0;
  for(int i = m ; i <= m+n-2 ; ++i)
    res *= i/(i-m+1.0);
  return res;
}

TreeNode* Solutions::sortedArrayToBST(vector<int> &num){
  auto findindex = [](int start,int end){
    if(start>end) return -1;
    int z = (end-start+1)/2;
    return start + z;
  };
  function<TreeNode* (int,int)> func = 
    [&findindex, &num,&func](int minid, int maxid){
    int zz = findindex(minid,maxid);
    TreeNode* head = NULL;
    if(zz<0) return head;
    head = new TreeNode(num[zz]);
    if(minid==maxid) return head;
    head->left = func(minid,zz-1);
    head->right = func(zz+1, maxid);
    return head;
  };
  return func(0,num.size()-1);
}

vector<vector<int> > Solutions::subsets(vector<int> &S){
  sort(S.begin(),S.end());
  typedef vector<vector<int> > resulttype;
  typedef vector<int> inputtype;
  
  function<resulttype(inputtype&)> func = [&func](inputtype& S){
    vector<vector<int> > res;
    if(S.size()==0) {
      res.push_back(S);
      return res;
    }
    vector<int> q(S.begin()+1,S.end());
    vector<vector<int> > next = func(q);
    
    for(auto x : next){
      res.push_back(x);
      x.insert(x.begin(),S[0]);
      res.push_back(x);
    }
    return res;
  };
  return func(S);
}

string Solutions::simplifyPath(string path){
  stringstream ss(path);
  vector<string> cache;
  string item;
  while(getline(ss,item,'/')){
    if(item.empty()) continue;
    if(item.compare(".")==0) continue;
    if(item.compare("..")==0){
      if(!cache.empty()) // without, run time error.
	cache.pop_back();
      continue;
    }
    cache.push_back(item);
  }
  string s = accumulate(cache.begin(),cache.end(),string(), [](string s, string v){return s + "/" + v;});
  return s.empty()?"/":s;
}

int Solutions::ladderLength(string start, string end, unordered_set<string> &dict){
  queue<pair<string,int> > nextkey;
  nextkey.push(make_pair(start,1));
  while(!nextkey.empty()){
    auto head = nextkey.front(); nextkey.pop();
    for(int i = 0 ; i < head.first.size(); ++i){
      char old = head.first[i];
      for(char c = 'a' ; c <='z'; ++c){
	if(c==old) continue;
	head.first[i] = c;
	if(head.first.compare(end) == 0) return head.second+1;
	if(dict.find(head.first) == dict.end()) continue;
	head.second+=1;
	nextkey.push(head);
	head.second-=1;
	dict.erase(head.first);
      }
      head.first[i] = old;
    }
  }
  return 0; 
}


int ladderLengthI(string start, string end, unordered_set<string> &dict){

  auto isgood = [](string s, string e){
    if(s.size()!=e.size()) 
      return false;
    for(int i = 0 ; i < s.size(); ++i)
      s[i] -= e[i];
    int z = count(s.begin(),s.end(),0);
    return z == s.size()-1;
  };
  queue<unordered_set<string> > lastpath;
  queue<string> lastword;
  unordered_set<string> s;
  s.insert(start);
  lastpath.push(s);
  lastword.push(start);
  dict.erase(start);
  while(true){
    auto set = lastpath.front();
    lastpath.pop();
    auto lw  = lastword.front();
    lastword.pop();
    if(lw == end) return set.size();
    for(int j = 0 ; j < lw.size() ; j++){
      string sss = lw;
      for(char k = 'a' ; k <= 'z' ; k++){
	sss[j] = k ;
	if(set.find(sss)!=set.end()) continue;
	if(dict.count(sss) == 0) continue;
	if(sss==end) return set.size()+1;
	unordered_set<string> p = set;
	p.insert(sss);
	lastpath.push(p);
	lastword.push(sss);
	dict.erase(sss);
      }
    }
    if(lastpath.size()==0) return 0;
  }
  
}

void Solutions::findladdershelper(string & start, string& end, unordered_set<string> &dict, 
			      vector<string> &adjwordlist, vector<vector<int> > &adjlist,
			      int& init, int& target){
  
  unordered_map<string,int> mapz;
  int ind = 0;
  for(auto &x : dict){
    mapz[x] = ind;
    adjwordlist.push_back(x);
    if(x == end) target = ind;
    if(x == start) init = ind;
    ++ind;
  }
  for(auto &x : adjwordlist){
    adjlist.push_back(vector<int>());
  }
  int indx = 0 ;
  for(auto &x :adjwordlist){
    for(int i = 0 ; i < x.size() ; ++i){
      char thisc = x[i];
      for(char c = 'a' ; c <= 'z' ; ++c){
	if(c==thisc) continue;
	x[i] = c;
	if(dict.count(x)) {
	  int id = mapz[x];
	  adjlist[indx].push_back(id);
	  if(indx < id)
	    adjlist[id].push_back(indx);
	}
      }
      x[i] = thisc;
    }
    dict.erase(x);
    ++indx;
  }
    
}

vector<vector<string>> Solutions::findLadders(string start, string end, unordered_set<string> &dict){

  
  vector<vector<string> > res;
  vector<vector<int> > adjlist;
  vector<string> adjwordlist;
  int init = -1;
  int target = -1 ;
  findladdershelper(start, end,dict,adjwordlist,adjlist,init,target);
  if(init < 0 || target < 0) return res;
  auto getstring  = [&adjwordlist](vector<int> inp){
    vector<string> res(inp.size());
    transform(inp.begin(),inp.end(),res.begin(),[&adjwordlist](int x){return adjwordlist[x];});
    return res;
  };
  
  queue<vector<int> > lastpath;
  lastpath.push(vector<int>(1,init));
  //dict.erase(adjwordlist[init]);
  unordered_set<int> mapz;
  mapz.insert(init);
  int minlen = INT_MAX;
  int lastpathlength; 
  vector<int> removewordlist;
  while(!lastpath.empty()){
    auto thispath = lastpath.front(); lastpath.pop();
    if(thispath.size()>=minlen) break;
    if(thispath.size()>lastpathlength){
      for(auto &y:removewordlist)
       	mapz.insert(y);
      removewordlist.clear();
    }
    int lastw = thispath.back();
    //cout << " last " << lastw << endl;
    for(auto & x : adjlist[lastw]){
      vector<int> tmp = thispath;
      if(mapz.count(x)) continue;
      tmp.push_back(x);
      if(x==target){
	res.push_back(getstring(tmp));
	minlen = tmp.size();
      }else if (tmp.size()<minlen){
	lastpath.push(tmp);
	removewordlist.push_back(x);
      }
    }
  }
  return res;

}

vector<vector<int> > Solutions::levelOrderBottom(TreeNode *root){
  vector<vector<int> > res;
  function<void(TreeNode*,int)> doit 
    = [&res,&doit](TreeNode* root,int level){
    if(root==NULL) return;
    if(res.size()<level)
      {
	vector<int> tmp;
	tmp.push_back(root->val);
	res.push_back(tmp);
      }
    else{
      res[level-1].push_back(root->val);
    }
    doit(root->left,level+1);
    doit(root->right,level+1);
  };
  doit(root,1);
  std::reverse(res.begin(), res.end());
  return res;
}
void Solutions::setZeroes(vector<vector<int> > &matrix){ // constant space solution.
  bool shouldsetrow = any_of(matrix.front().begin(),matrix.front().end(),[](int x){return x ==0 ;});
  bool shouldsetcol = any_of(matrix.begin(), matrix.end(),[](vector<int> x){return x[0]==0;});
  // mark boundary 
  for(auto & x : matrix)
    for(int i = 0; i < x.size();++i)
      if(x[i]==0) {
	x[0] = 0; 
	matrix[0][i]=0;
      };

  // set 0 by boundary
  for(int i = 1; i < matrix.size(); ++i)
    if(matrix[i][0] == 0 ) 
      fill(matrix[i].begin(), matrix[i].end(),0);
  for(int i = 1; i < matrix.front().size(); ++i)
    if(matrix[0][i] == 0 ) 
      for(int j = 1 ; j < matrix.size(); ++j)
	matrix[j][i] = 0;

  // set first column and first row.
  if(shouldsetrow) 
    fill(matrix[0].begin(),matrix[0].end(),0);
  if(shouldsetcol)
    for(int i = 0; i < matrix.size(); ++i)
      matrix[i][0] = 0;
}
void setZeroesI(vector<vector<int> > &matrix){ // O(m+n) space solution.
  unordered_set<int> row;
  unordered_set<int> col;
  for(int i = 0 ; i < matrix.size(); ++i)
    for(int j = 0 ; j < matrix.front().size(); ++j){
      if(matrix[i][j]==0){
	row.insert(i);
	col.insert(j);
      }
    }
  for(auto r : row){
    for(auto &p : matrix[r])
      p = 0;
  }
  for(auto r : col){
    for(auto &p : matrix)
      p[r] = 0;
  }
}

int Solutions::maxSubArray(int A[], int n){
  // if(n==1) return A[0];
  // int cumsum = 0; 
  // for(int i = 0 ; i < n ; ++i){
  //   cumsum = A[i] += cumsum ;
  // }
  // int max = INT_MIN;
  // for(int i = 0 ; i < n ; ++i){
  //   int tmp = A[i];
  //   for(int j = 0 ; j < i ; ++j){
  //     int z = A[i] - A[j];
  //     if(z > tmp) tmp = z;
  //   }
  //   if(tmp>max) max = tmp;
  // }
  // return max;
  int maxhere, maxsofar;
  maxhere = maxsofar = INT_MIN;
  for(int i = 0 ; i < n ; ++ i){
    maxhere = maxhere < 0 ? A[i] : (maxhere += A[i]);
    if(maxhere > maxsofar) maxsofar = maxhere;
  }
  return maxsofar;
}

void Solutions::solve(vector<vector<char> > &board){
  int M = board.size();
  if(M==0) return;
  int N = board.front().size();
  if(N == 0) return;
  struct pair_hash{
    inline std::size_t operator()(const pair<int,int>& v) const{
      return v.first*31 + v.second;
    }
  };
  typedef unordered_set<pair<int,int>, pair_hash> MySet;
  MySet knownkept;
  queue<pair<int,int> > inputs;
  for(int i = 0 ; i < M ; ++i)
    for(int j = 0; j < N ; ++j){
      if(i==0 || j == 0 || i == M-1 || j == N-1){
	if(board[i][j]=='O') {auto key = make_pair(i,j); inputs.push(key); knownkept.insert(key);}
      }
    }
  auto isgood = [&board, &knownkept, &M,&N](pair<int,int> & p ){
    return p.first >= 0 && p.first < M && p.second>=0 && p.second < N && (knownkept.find(p)==knownkept.end())
    && board[p.first][p.second] == 'O';
  };



  while(inputs.size() != 0){
    auto edge = inputs.front();
    inputs.pop();
    {
      auto up = make_pair(edge.first+1,edge.second);
      auto dn = make_pair(edge.first-1,edge.second);
      auto rt = make_pair(edge.first,edge.second+1);
      auto lt = make_pair(edge.first,edge.second-1);
      if(isgood(up)) {knownkept.insert(up);inputs.push(up);}
      if(isgood(dn)) {knownkept.insert(dn);inputs.push(dn);}
      if(isgood(lt)) {knownkept.insert(lt);inputs.push(lt);}
      if(isgood(rt)) {knownkept.insert(rt);inputs.push(rt);}
    }
  }
  for(int i = 0 ; i < M ; ++i)
    for(int j = 0; j < N ; ++j){
      auto key = make_pair(i,j);
      if(isgood(key) && (knownkept.find(key)==knownkept.end()))
	board[i][j]='X';
    }
}

vector<vector<string> > Solutions::solveNQueens(int n){
  vector<vector<int> > numsolution;
  vector<vector<string> > solution;
  auto generatevalid = [&n](vector<int> x){
    int M  = x.size();
    unordered_set<int> res;
    for(int i = 0 ; i < x.size(); ++i){
      if(x[i] - (M-i) >= 0 ) res.insert(x[i]-M+i);
      res.insert(x[i]);
      if(x[i] + (M-i) <  n ) res.insert(x[i]+M-i);
    }
    // cout << "num of aval" << res.size() << endl;
    vector<int> ress;
    for(int i = 0 ; i < n ; ++i)
      if(res.find(i) == res.end()) ress.push_back(i);
    return ress;
  };
  function<void(vector<int>) > func = 
    [&numsolution,&n,&generatevalid,&func](vector<int> known){
    int N = known.size();
    if(N == n) numsolution.push_back(known);
    auto aval =  generatevalid(known);
    //cout << aval.size() << endl;
    if(aval.size() == 0) return;
    for(auto & x : aval){
      known.push_back(x);
      func(known);
      known.pop_back();
    }
  };
  vector<int> k1;
  func(k1);
  for(auto & so : numsolution){
    vector<string> tmps;
    for(auto & q : so){
      string p(n,'.');
      p[q] = 'Q';
      tmps.push_back(p);
    }
    solution.push_back(tmps);
  }
  //cout <<"NUM" <<  solution.size() << endl;
  return solution;
}

int Solutions::totalNQueens(int n){
  int cnt = 0 ;
  int upper = (1<<n)-1 ; // all ones. length of the board.
  function<void(int,int,int)> Queen= [&cnt,&upper,&Queen](int row,int ld,int rd){
    int pos,p;
    if(row!=upper){
	pos = upper & (~(row | ld |rd)); // use bit to represent position of queens. available positions.
	while(pos!=0){
	  p = pos & (-pos); // what's this? 
	  pos = pos - p;
	  Queen(row+p,(ld+p)<<1,(rd+p)>>1); // avaliable positions.
	}
    }
    else ++cnt;
  };
  Queen(0,0,0);
  return cnt;
}

int Solutions::jump(int A[], int n){//BSF
  vector<int> count(n,0);
  int furthestpos = 0;
  for(int i = 0 ; i <= furthestpos ; ++i){
    int canreach = i + A[i];
    if (canreach >= n-1 ) return i == (n-1)? 0: (count[i]+1);
    if (canreach > furthestpos){
      fill(count.begin()+furthestpos+1, count.begin()+canreach+1,count[i]+1);
      furthestpos = canreach;
    }
  }
  return INT_MIN;
}
bool Solutions::canJump(int A[], int n){
  int canjump = 0;
  for(int i = 0; i < n; ++i){
    if(canjump >= n-1) return true;
    if(i>canjump) return false;
    canjump = max(A[i] + i,canjump);
  }
  return false;
}
bool canJumpI(int A[], int n){
  vector<int> count(n,0);
  int furthestpos = 0;
  for(int i = 0 ; i <= furthestpos ; ++i){
    int canreach = i + A[i];
    if (canreach >= n-1 ) return true;
    if (canreach > furthestpos){
      fill(count.begin()+furthestpos+1, count.begin()+canreach+1,count[i]+1);
      furthestpos = canreach;
    }
  }
  return false;
}

vector<int> Solutions::spiralOrder(vector<vector<int> > &matrix){
  vector<int> res;
  int M,cM,N,cN,j;
  cM = M = matrix.size(); 
  if(M==0) 
    return res;
  cN = N = matrix.front().size(); 
  if(N==0) 
    return res;
  int numofs = (int)ceil(min(M,N)/2.0);
  for(int i = 0 ; i < numofs ; ++i){
    for(j = 0 ; j < N ; ++j) // top
      res.push_back(matrix[i][i+j]);
    for(j = 1 ; j < M ; ++j) // right
      res.push_back(matrix[i+j][cN-1-i]);
    if(cM-1-i != i)
      for(j = N-2; j > -1 ; --j) // bottom
	res.push_back(matrix[cM-1-i][i+j]);
    if(cN-1-i != i)
      for(j = M-2; j > 0; --j) // left
	res.push_back(matrix[i+j][i]);
    N -= 2; 
    M -= 2;
  }
  return res;
}

void Solutions::sortColors(int A[], int n){
  // recursive, not good. 
  if(n<=1) return;
  int first = A[0];
  int index = 0;
  bool hasmax = false;
  for(int i = 1 ; i < n ; ++i){
    if( A[i] >   first ) {if(!hasmax) ++index; hasmax = true; continue;}
    if( A[i] <=  first ) {swap(A[i],A[index++]);}
  }
  sortColors(A+index,n-index); // Right;
  if(index<n)sortColors(A,index); // left;
  // one pass solution, when get a new number figure out what need to be swapped.
}

vector<vector<int> > Solutions::generateMatrix(int n){
  int M,cM,N,cN,j;
  vector<vector<int> > res;
  for(int i = 0 ; i < n ; ++i){
    vector<int> q(n,0);
    res.push_back(q);
  }
  cM = M = n;
  cN = N = n;
  int numofs = (int)ceil(min(M,N)/2.0);
  int assign = 1;
  for(int i = 0 ; i < numofs ; ++i){
    for(j = 0 ; j < N ; ++j) // top
      res[i][i+j] = assign++;
    for(j = 1 ; j < M ; ++j) // right
      res[i+j][cN-1-i] = assign++;
    if(cM-1-i != i)
      for(j = N-2; j > -1 ; --j) // bottom
	res[cM-1-i][i+j] = assign++;
    if(cN-1-i != i)
      for(j = M-2; j > 0; --j) // left
	res[i+j][i] = assign++;
    N -= 2; 
    M -= 2;
  }
  return res;
}

void Solutions::merge(int A[], int m, int B[], int n){
  int lastindex = m + n -1;
  int i = m - 1;
  int j = n - 1;
  while(i>=0 && j >=0){
    if(A[i] > B[j] ) 
      A[lastindex--] = A[i--];
    else 
      A[lastindex--] = B[j--];
  }
  while(j>=0){
    A[lastindex--] = B[j--];
  }
}

vector<string> Solutions::generateParenthesis(int n){
  vector<string> res;
  function<void(int,int,string)> func = 
    [&res,&func](int total, int left, string current){
    if(total == 0 ) res.push_back(current);
    if(left-1 >= 0)
      func(total-1,left-1, current+')');
    if(left+1 <= total)
      func(total  ,left+1, current+'(');
  };
  func(n,0,"");
  return res;
}

int Solutions::removeDuplicatesII(int A[], int n){
  if(n==0) return 0;
  int last = INT_MIN;
  int tmpcount = 1;
  int i;
  int j = 0;
  for(i = 0 ; i < n ; ++i){
    if(A[i] == last) { 
      if(++tmpcount <= 2){;
	swap(A[i],A[j++]);
      }
    }
    //if(A[i] != last) { // this would give wrong answer, the fuck? 
    //because you swap A[i] already, it is not the same any more!!!.
    else{ // this is right.
      last = A[i];
      tmpcount = 1;
      swap(A[i],A[j++]);
    }
  } 
  return j;
}
// go up means previous high at that position is not usable any more
// go down update maximum.
int Solutions::longestValidParentheses(string s){
  vector<int> maxs(s.size()+1,0);
  int cumsum = 0;
  int maxm = 0 ;
  for(auto &c : s){
    if(c == '(') 
      maxs[++cumsum] = 0;
    if(c == ')'){
      if(cumsum > 0) {
	--cumsum;
	maxs[cumsum] += maxs[cumsum+1]+2;
	if(maxs[cumsum] > maxm) 
	  maxm = maxs[cumsum];
      }else
	maxs[cumsum] = 0;
    }
  }
  return maxm;
}

ListNode* Solutions::deleteDuplicates(ListNode *head){
  ListNode* p = head;
  while(p!=NULL && p->next != NULL){
    if(p->next->val == p->val){
      ListNode* t = p->next;
      p->next = p->next->next;
      delete t;
    }else{
      p = p->next;
    }
  }
  return head;
}
// solution on line seems to have memory leaks.
ListNode * Solutions::deleteDuplicatesII(ListNode *head){
  if(head == NULL) return head;
  ListNode* q = new ListNode(head->val + 1); // NOT really good.
  q->next = head;
  head = q;
  ListNode* p = head->next;
  while(p!=NULL && p->next != NULL){
    if(p->next->val == p->val){
      int v = p->val;
      while(p!=NULL && p->val == v){
	ListNode* t = p;
	p = p->next;
	delete t;
      }
      q->next = p;
    }else{
      q = q->next;
      p = p->next;
    }
  }
  ListNode* z = head;
  head = head->next;
  delete z;
  return head;
}

ListNode * Solutions::rotateRight(ListNode *head, int k){
  if(head == NULL) return head; // boundary case.
  ListNode* fast = head;
  ListNode* slow = head;
  int count = 1;
  while(fast->next !=NULL) {
    fast = fast->next;
    ++count;
  }
  if((k = k%count)==0) return head; // boundary case
  fast = head;
  int lag = 0;
  while(fast->next !=NULL){
    fast = fast->next;
    if(lag++ < k) continue;
    slow = slow->next;
  }
  fast->next = head; // make a circle
  head = slow->next; // cut at slow
  slow->next = NULL;
  return head;
}
// This method is over kill, original problem is simpler, you are able to
// rearrange the matrix to a 1-d sorted array, however this can solve a more general problem
// where you only require each col and row are sorted with no penalty on performance.
bool Solutions::searchMatrix(vector<vector<int> > &matrix, int target){
  if(matrix.size()==0)         return false; 
  if(matrix.front().size()==0) return false;
  int li = 0; 
  int hj = matrix.front().size()-1;
  while(true){
    auto high = lower_bound(matrix.begin(),matrix.end(),target,[&hj]( vector<int> v ,int t){return v[hj]<t;});
    if(high == matrix.end())      return false; // not found, target is bigger than any in matrix.
    if((*high)[hj] == target)     return true; 
    li = high - matrix.begin(); // first row cannot be deleted.
    auto low  = upper_bound(matrix[li].begin(),matrix[li].end(),target); // first strictly bigger
    if(low == matrix[li].begin()) return false; // if this is first, it is also strictly bigger, we are done.
    if(*(--low) == target)        return true;  // check previous 
    hj = low - matrix[li].begin(); // update index;
  }
}
bool searchMatrix(vector<vector<int> > &matrix, int target) {
  if(matrix.empty() || matrix.front().empty()) return false;
  int M = matrix.size(); 
  int N = matrix.front().size();
  int x0 = 0;
  int y1 = N-1;
  while(x0 <= M-1 && 0 <= y1){
    if(matrix[x0][0] > target || matrix[M-1][y1] < target) return false;
    if(matrix[x0][0] == target || matrix[M-1][y1] == target || matrix[x0][y1] == target || matrix[M-1][0] == target) return true;
    x0 = lower_bound(matrix.begin()+x0,matrix.end(),target,[&](vector<int> x,int t){return x[y1] < t;}) - matrix.begin();
    y1 = upper_bound(matrix[x0].begin(),matrix[x0].end(), target) - matrix[x0].begin()-1;
  }
  return false;
}


vector<string> Solutions::fullJustify(vector<string> &words, int L){
  vector<string> res;
  auto canadd = [&L](vector<string> s, string input){
    s.push_back(input);
    int sx = accumulate(s.begin(),s.end(),0,[](int x, string s1){return x + s1.size();});
    if(s.size()==1) return sx<=L;
    return sx + s.size()-1 <= L;
  };
  vector<vector<string> > tmplist;
  vector<string> tmp;
  for(auto & s : words){
    if(canadd(tmp,s)) 
      tmp.push_back(s);
    else{
      tmplist.push_back(tmp);
      tmp.clear();
      tmp.push_back(s); // need to if you can add s, but if not, the function input is wrong.
    }
  }
  tmplist.push_back(tmp);
  // Done grouping, combine to result. this is nasty.1)last line is special
  auto combine = [&L](vector<string> s, bool islast){
    int wordl   = accumulate(s.begin(),s.end(),0,[](int x, string s1){return x + s1.size();});
    int ll = L - wordl;
    int spacs   = (L - wordl)/ (std::max((int)s.size()-1,1));
    int sspaces =  L - wordl -  spacs*(std::max((int)s.size()-1,1));
    string p = accumulate(s.begin(),
  		      s.end(),
  		      string(), // empty string
			  [&sspaces,&spacs,&ll,&islast](string cum, string v ) {
			    int x = spacs + ( sspaces-- >0?1:0);
			    string s1(ll>0?x:0,' ');
			    if(islast&&ll>0) s1 = " " ;
			    ll -=x;
			    return cum + v + s1;
			  });
    if(p.size() < L) 
      return p + string((L-(int)p.size()),' ');
    return p;
  };
  int cc = 0;
  for(auto & l : tmplist)
    res.push_back(combine(l,++cc==tmplist.size()));
  return res;
}
// alternatively, you can all strlen find length, go backward. it will use less than O(n) operations. Your method is guaranteed to use n operations.
int Solutions::lengthOfLastWord(const char *s){ 
  int fast = (int)strlen(s)-1; // this needs #include<cstring>
  while(fast>=0 && isspace(s[fast])) --fast;
  int slow = fast;
  while(slow>=0 && !isspace(s[slow])) --slow;
  return fast - slow;
}
vector<Interval> Solutions::merge(vector<Interval> &intervals){
  vector<Interval> res;
  if(intervals.size()==0) return res;
  sort(intervals.begin(), intervals.end(),[](Interval a, Interval b) { return a.end< b.start || a.start < b.start ; });
  int st = intervals.front().start;
  int ed = intervals.front().end;
  for(auto &x : intervals){
    if(x.start > ed){
      res.push_back(Interval(st,ed));
      st = x.start;
      ed = x.end;
      continue;
    }
    ed = max(ed, x.end);
  }
  res.push_back(Interval(st,ed));
  return res;
}
vector<Interval> mergeI(vector<Interval> &intervals){
  vector<Interval> res;
  sort(intervals.begin(),
       intervals.end(),
       [](Interval x,Interval y){ 
	 // change to x.end<=y.end cause problem. why?
	 return (x.start < y.start) || (x.start==y.start && x.end < y.end); 
       });
  bool first = true;
  Interval rest;
  for(auto & x : intervals){
    if(first){rest = x; first = false; continue;}
    if(x.start > rest.end){res.push_back(rest); rest = x; } 
    if(x.end>rest.end)rest.end = x.end;
  }
  if(!first)res.push_back(rest);
  return res;
}
// This algorithm is O(log(n)) if you use list instead of vector, however.. if using list, can you
// still do binary search??? no. because you cannot jump.
vector<Interval> Solutions::insert(vector<Interval> &intervals, Interval newInterval){
  auto compare = [](Interval x, Interval y){ return x.end < y.start;};
  auto first = lower_bound(intervals.begin(),intervals.end(),newInterval,compare);
  auto second = upper_bound(first,intervals.end(),newInterval,compare);
  if(first != second){ // this could be that both come out to be interval.end(); 
    auto minele = min_element(first,second,[](Interval x, Interval y){ return x.start < y.start ;});
    auto maxele = max_element(first,second,[](Interval x, Interval y){ return x.end   < y.end   ;}); 
    newInterval.start = min(minele->start,newInterval.start);
    newInterval.end   = max(maxele->end,newInterval.end);
  }
  // iterator is gone,(although it still works as input follows, the current method is better?
  int j = first-intervals.begin(); 
  intervals.erase(first,second);
  intervals.insert(intervals.begin()+j,newInterval);
  return intervals;
}

int Solutions::numDecodings(string s){ // number of ways to decode.
  if(s.size()==0) return 0;
  vector<int> ways(s.size()+1,0);
  ways[s.size()] = 1;
  for(int i = s.size()-1;i>=0;--i){
    if(s[i]=='0') continue;
    if(i==s.size()-1) {ways[i] = 1; continue;}
    ways[i] = ways[i+1] + ((s[i]-'0')*10 + (s[i+1]-'0')<27 ? ways[i+2]:0);
  }
  return ways[0];
}

vector<int> Solutions::grayCode(int n){
  vector<int> res;
  res.push_back(0);
  if(n==0) return res;
  int m = 1;
  for(int i = 0 ; i < n ; ++i){
    vector<int> tmp = res;
    std::reverse(tmp.begin(),tmp.end());
    for(auto & p : tmp){
      p ^= m;
    }
    m = (m<<1);
    res.insert(res.end(),tmp.begin(),tmp.end());
  }
  return res;
}

string Solutions::longestPalindrome(string s){
  auto check = [&s](int index, bool iseven){ // 0 to n-2
    int i = index - (iseven?0:1);
    int j = index+1;
    int z = iseven?0:1;
    while(i>=0 && j < s.size()){
      if(s[i--] == s[j++]) 
	z+=2;
      else
	break;
    }
    return z;
  };
  int maxs = INT_MIN;
  int in = 0;
  bool iseven = false;
  for(int i = 0; i < s.size(); ++i){
    int maxodd  = check(i,false);
    int maxeven = check(i,true);
    int p = max(maxodd,maxeven);
    if(p>maxs) {
      maxs = p;
      in = i;
      iseven = maxeven == p;
    }
  }
  int i = iseven?(in-(maxs/2-1)):(in-(maxs/2));
  return s.substr(i,maxs);
}

int sumNumbers(TreeNode *root){
  int sums = 0;
  function<void(TreeNode*,int)> run = 
    [&sums,&run](TreeNode* root,int tmpsum){
    if(root == NULL) return;
    int thisn = root->val + tmpsum*10;
    if(root->left == NULL && root->right == NULL){
      sums += thisn;
      return;
    }
    run(root->left,thisn);
    run(root->right,thisn);
  };
  run(root,0);
  return sums;
}
//BSF, this doesn't work.
bool exist(vector<vector<char> > &board, string word){
  auto isgood =[&board](pair<int,int> in){
    return !(in.first<0 || in.first >= board.size()||
	     in.second <0 || in.second>= board.front().size());
  };
  auto func = [&board,&isgood,&word](int i, int j){
    queue<vector<pair<int,int> > > cache;
    if(board[i][j] != word[0]) return false;
    if(word.size()==1) return true;
    vector<pair<int,int> > p1;
    p1.push_back(make_pair(i,j));
    cache.push(p1);
    while(cache.size()!=0){
      auto path = cache.front(); 
      cache.pop();
      int x = path.back().first;
      int y = path.back().second;
      
      vector<pair<int,int> > nextm;
      nextm.push_back(make_pair(x+1,y));
      nextm.push_back(make_pair(x,y+1));
      nextm.push_back(make_pair(x-1,y));
      nextm.push_back(make_pair(x,y-1));
      for(auto nm : nextm){
	if(!isgood(nm)) continue; // move is valid.
	if(find(path.begin(),path.end(),nm)!=path.end()) continue;
	if(word[path.size()] != board[nm.first][nm.second]) continue; // move match word character.
	if(word.size()-1==path.size()) return true; // done with word?
	vector<pair<int,int> > tmp = path;
	tmp.push_back(nm);
	cache.push(tmp);
      }
    }
    return false;
  };
  for(int i = 0; i < board.size(); ++i)
    for(int j = 0; j < board.front().size(); ++j)
      if(func(i,j)) return true;
  
  return false;
}
//DSF
bool Solutions::exist(vector<vector<char> > &board, string word){
  auto isgood =[&board](pair<int,int> in){
    return !(in.first<0 || in.first >= board.size()||
	     in.second <0 || in.second>= board.front().size());
  };
  function<bool(int,int,int)>
    func = [&func, &board,&isgood,&word](int i, int j, int k){//prob i,j with k char in word.
    if(board[i][j] != word[k]) return false;
    if(word.size()-1==k) return true;
    
    board[i][j] = '\0'; // remember to change back when pop out.
    vector<pair<int,int> > nextm;
    nextm.push_back(make_pair(i+1,j));
    nextm.push_back(make_pair(i,j+1));
    nextm.push_back(make_pair(i-1,j));
    nextm.push_back(make_pair(i,j-1));
    for(auto & nm : nextm){
      if(!isgood(nm)) continue; // move is valid.
      if(func(nm.first,nm.second,k+1)) return true;
    }
    board[i][j] = word[k];
    return false;
  };
  for(int i = 0; i < board.size(); ++i)
    for(int j = 0; j < board.front().size(); ++j)
      if(func(i,j,0)) return true;
  
  return false;

}

int Solutions::minimumTotal(vector<vector<int> > &triangle){
  for(int i = triangle.size()-2 ; i>=0 ; --i)
    for(int j = 0 ; j < triangle[i].size(); ++j)
      triangle[i][j] += min(triangle[i+1][j],triangle[i+1][j+1]);
  return triangle[0][0];
}
// make a set, for each element find the max internal by expanding, erase element from the set
// in the process. 
int Solutions::longestConsecutive(vector<int> &num){
  unordered_set<int> mapz;
  for(auto x : num)
    mapz.insert(x);
  int maxm = 0;
  while(mapz.size()>0){
    int p = *mapz.begin();
    int count = 0;
    int z = p+1;
    while(mapz.find(z)!=mapz.end()){ // p itself is not counted here.
      ++count;
      mapz.erase(z++);
    }
    while(mapz.find(p) != mapz.end()){// p is counted here.
      mapz.erase(p--);
      ++count;
    }
    maxm = max(count,maxm); 
  }
  return maxm;
}

int Solutions::firstMissingPositive(int A[], int n){
  unordered_set<int> mapz;
  for(int i = 0 ; i < n ; ++i)
    mapz.insert(A[i]);
  int p = 1;
  while(mapz.find(p) != mapz.end())
    ++p;
  return p;
}

TreeNode* Solutions::sortedListToBST(ListNode *head){
  if(head == NULL) return NULL;
  ListNode* p = head;
  int N = 0;
  while(p != NULL){
    p = p->next;
    ++N;
  }
  function<TreeNode*(ListNode*&,int,int)> func = 
    [&func](ListNode* &p, int min, int max){
    TreeNode* q = NULL;
    if(p == NULL || min == max) return q;
    if(min == max-1){
      auto l = new TreeNode(p->val);
      p = p->next;
      return l;
    }
    int mid = (min+max)/2;
    auto left = func(p,min,mid); // p changed here.
    if(p == NULL) return left;
    auto z = new TreeNode(p->val);
    z->left = left;
    p = p->next;
    if(p !=NULL)
      z->right = func(p,mid+1,max);
    return z;
  };
  return func(head, 0, N);
}
//slow->next is the first element that's larger than or equal to x;
ListNode* Solutions::partition(ListNode *head, int x){
  if(head == NULL) return head;
  ListNode* z = new ListNode(INT_MIN);
  z->next = head;
  ListNode* slow = z;
  ListNode* fast = z;
  while(slow->next !=NULL && slow->next->val < x){
    slow = slow->next;
    fast = fast->next;
  }

  while(fast->next != NULL){
    if(fast->next->val < x){
      ListNode* t = fast->next;
      fast->next = fast->next->next;
      t->next = slow->next;
      slow->next = t;
      slow = slow->next;
      continue;
    }
    fast = fast->next;
  }
  head = z->next;
  delete z;
  return head;
}

vector<int> Solutions::inorderTraversal(TreeNode *root){
  vector<int> res;
  function<void(TreeNode*)> func =
    [&res,&func](TreeNode* r){
    if(r == NULL) return;
    if(r->left !=NULL) func(r->left);
    res.push_back(r->val);
    if(r->right !=NULL) func(r->right);
  };
  func(root);
  return res;
}

vector<vector<int> > Solutions::permute(vector<int> &num) {
  vector<vector<int> > res;
  sort(num.begin(),num.end());
  do{
    res.push_back(num);
  }while(next_permutation(num.begin(),num.end()));
  return res;
}

void Solutions::nextPermutation(vector<int> &num){
  next_permutation(num.begin(),num.end());
}

string Solutions::getPermutation(int n, int k){
  string seq = "123456789";
  seq = seq.substr(0,n);
  long x = 1;
  for(int i = 1 ; i <= n ; ++i)
    x*=i;
  --k;
  string res = "";
  do{
    k %=x;
    x /= seq.size();
    int i = k/x;
    res.push_back(seq[i]);
    seq.erase(seq.begin() + i);
  }while(!seq.empty());
  return res;
}

string getPermutationI(int n, int k){
  string vect = "123456789";
  vect = vect.substr(0,n);
  int mod = 1;
  for(int i = 1 ; i <n ; ++i)
    mod = (i*mod);
  --k;
  vector<int> q;
  for(int i = n; i >= 0 ; --i){
    if(i==0) mod = 1;
    int z = k/mod;
    q.push_back(z);
    k = k % mod;
    cout << z << endl;
    if(i==1) continue;
    mod = mod/(i-1);
  }
  string s =vect;
  int id = 0;
  for(auto & x : q){
    s[id++] = vect[x];
    vect.erase(x,1);
  }
  return s;
}

void Solutions::solveSudoku(vector<string > &board){
  auto check = [&board](int m, int n){
    char thisc = board[m][n];
    if(thisc=='.') return true;

    for(int i = 1 ; i < 9 ; ++i)
      if(board[m][(i+n)%9]==thisc) return false;

    for(int i = 1 ; i < 9 ; ++i)
      if(board[(i+m)%9][n]==thisc)return false;

    int I = (m / 3)*3;
    int J = (n / 3)*3;
    for(int i = 0 ; i < 9 ; ++i){
      int x = I + (i/3);
      int y = J + (i%3);
      if(x== m && y == n) continue;
      if(board[x][y] == thisc) return false;
    }
    return true;
  };

  function<bool()> func = [&func,&check,&board]{
    int x,y;
    for(x = 0; x < 9 ; ++x)
      for(y = 0 ; y < 9; ++y) 
	if(board[x][y] == '.') goto gos;
  gos:
    if(x==9&&y==9)return true;
    for(int k = 1; k <= 9 ; ++k){
      board[x][y] = k + '0';
      if(check(x,y)&&func())return true;
      board[x][y] = '.';
    }
    return false;
  };
  func();
}

int Solutions::threeSumClosest(vector<int> &num, int target){
  if(num.size() < 3) return 0;
  sort(num.begin(),num.end());
  int MAXZ = INT_MAX;
  int minres = 0;
  int lastI = INT_MIN;
  for(int i = 0 ; i < num.size()-1; ++i){
    int k = i+1;
    int l = num.size() - 1;
    int z = target-num[i];
    int sums = num[k] + num[l];
    while(k < l){
      int tmpmax = abs(sums + num[i] - target);
      if(tmpmax < MAXZ){
	MAXZ = tmpmax;
	minres = sums + num[i];
      }
      if(sums == z){
	return target;
	int lnum = num[k];
	int rnum = num[l];
	while(k < num.size() && num[k] == lnum) 
	  ++k;
	while(l > i && num[l] == rnum)
	  --l;
	sums = num[k] + num[l];
	continue;
      }
      if(sums < z) ++k;
      if(sums > z) --l;
      sums = num[k] + num[l];
    }
  }
  return minres;
}

vector<vector<int> > Solutions::threeSum(vector<int> &num){
  vector<vector<int> > res;
  if(num.size() < 3) return res;
  sort(num.begin(),num.end());
  int lastI = INT_MIN;
  for(int i = 0 ; i < num.size()-1; ++i){
    if(lastI == num[i]) continue;
    lastI = num[i];
    if(num[i] > 0 ) break;
    int k = i+1;
    int l = num.size() - 1;

    int z = -num[i];
    int sums = num[k] + num[l];
    while(k < l){
      if(sums == z){
	vector<int> q;
	q.push_back(num[i]); q.push_back(num[k]); q.push_back(num[l]);
	res.push_back(q);
	int lnum = num[k];
	int rnum = num[l];
	while(k < num.size() && num[k] == lnum) 
	  ++k;
	while(l > i && num[l] == rnum)
	  --l;
	sums = num[k] + num[l];
	continue;
      }
      if(sums < z) ++k;
      if(sums > z) --l;
      sums = num[k] + num[l];
    }
  }
  return res;

}

vector<vector<int> > Solutions::fourSum(vector<int> &num, int target){
  sort(num.begin(),num.end());
  vector<vector<int> > res2;
  auto func2sum = [&num](int i,int j, int t){
    vector<vector<int> > res;
    while(i < j){
      if(num[i]+num[j]==t){
	vector<int> tmp;
	tmp.push_back(num[i]); tmp.push_back(num[j]); res.push_back(tmp);
	int z = num[i];
	while(num[i] == z)
	  ++i;
      }else if(num[i]+num[j]>t){
	--j;
      }
      else{
	++i;
      }
    }
    return res;
  };
  int lasttargeti = INT_MAX;
  for(int i = num.size()-1 ; i >=3 ; --i){
    if(num[i] == lasttargeti) continue;
    lasttargeti = num[i];
    int lasttargetj = INT_MAX;
    for(int j = i - 1; j >= 2; --j){
      int t1 = target - (num[i] + num[j]);
      if(num[j] == lasttargetj  ) continue;
      lasttargetj = num[j];
      auto v = func2sum(0,j-1,t1);
      for(auto & x : v){
	x.push_back(num[j]);
	x.push_back(num[i]);
	res2.push_back(x);
      }
    }
  }
  return res2;
}

ListNode* Solutions::reverseKGroup(ListNode *head, int k){
  if(head == NULL) return head;
  if(head->next == NULL) return head;
  if(k<2) return head;
  ListNode* fast = head ;
  ListNode* slow = head ;
  
  int z = 0;
  while(fast != NULL){
    fast = fast->next;
    ++z;
    if(z == k) break;
  }
  if(z < k) return head;
  auto nexthead = reverseKGroup( fast, k);
  
  do{
    auto p = slow;
    slow = slow->next;
    p->next = nexthead;
    nexthead = p;
  }while(slow->next != fast);
  slow->next = nexthead;
  return slow;

}

ListNode* Solutions::reverseBetween(ListNode *head, int m, int n){
  if(head == NULL) return head;
  if(m==n) return head;
  ListNode* p = new ListNode(0);
  p->next = head;

  ListNode* fast = p;
  ListNode* slow = p;

  int z = 0;
  while(slow !=NULL && z <m-1){
    slow = slow->next;
    ++z;
  }
  z = 0;
  while(fast !=NULL && z <n){
    fast = fast->next;
    ++z;
  }
  auto nexthead = fast;
  do{
    auto x = slow->next; 
    slow->next = x->next;
    x->next = fast->next;
    fast->next = x;
  }while(slow->next != fast);
  head = p->next;
  delete p;
  return head;

}

int Solutions::uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
  int M = obstacleGrid.size();
  int N = obstacleGrid.front().size();
  vector<vector<int> > ma = obstacleGrid;
  for(int i = 0 ; i < M ; ++i)
    for(int j = 0; j < N ; ++j)
      ma[i][j] = 0;  
  ma[M-1][N-1] = obstacleGrid[M-1][N-1]==0?1:0;
  for(int i = M-2 ; i >=0 ; --i)
    if(obstacleGrid[i+1][N-1]==0 && obstacleGrid[i][N-1]==0) 
      ma[i][N-1] = ma[i+1][N-1];
    else
      ma[i][N-1] = 0;

  for(int i = N-2 ; i >=0 ; --i)
    if(obstacleGrid[M-1][i+1]==0 && obstacleGrid[M-1][i]==0) 
      ma[M-1][i] = ma[M-1][i+1];
    else
      ma[M-1][i] = 0;


  int step = min(M,N);
  for(int l = 0 ; l < step-1 ; ++l){
    int x = M-l-2;
    int y = N-l-2;
    // row
    for(int j = y ; j >=0 ; --j){
      if(obstacleGrid[x][j]==1) continue;
      ma[x][j] += obstacleGrid[x][j+1] == 0? ma[x][j+1] : 0;
      ma[x][j] += obstacleGrid[x+1][j] == 0? ma[x+1][j] : 0;
    }
    // col
    for(int i = x-1 ; i >=0 ; --i){ // starts at x-1 to avoid double counting.
      if(obstacleGrid[i][y]==1) continue;
      ma[i][y] += obstacleGrid[i+1][y] == 0? ma[i+1][y] : 0;
      ma[i][y] += obstacleGrid[i][y+1] == 0? ma[i][y+1] : 0;
    }
    
  }
  return ma[0][0];

}

vector<string> Solutions::anagrams(vector<string> &strs){ //09-30/2013
  unordered_map<string, vector<string> > mapz;
  vector<string> res;
  for(auto &x : strs){
    auto s = x;
    sort(s.begin(),s.end());
    mapz[s].push_back(x);
  }
  for(auto &x : mapz){
    if(x.second.size()>1)
      for(auto & z : x.second)
	res.push_back(z);
  }
  return res;
}

vector<vector<int> > Solutions::combine(int n, int k){
  vector<vector<int> > res;
  function<void(int,int,vector<int>)> func = 
    [&res,&func,&n](int s,int left, vector<int> pre){
    if(left == 0 ) {res.push_back(pre);return;}
    for(int i = s ; i <= n-left+1 ; ++i){
      pre.push_back(i);
      func(i+1, left-1,pre);
      pre.pop_back();
    }
  };
  func(1,k,vector<int>());
  return res;
}


vector<vector<int> > Solutions::subsetsWithDup(vector<int> &S){
  sort(S.begin(),S.end());
  vector<vector<int> > res;
  function<void(int,vector<int> )> func = [&func,&res,&S](int ind, vector<int> pre){
    if(ind == S.size()) return;
    int z = S[ind];
    int k = 0;
    while(ind+k < S.size() && S[ind+k] == z) ++k;
    func(ind+k,pre);
    for(int i = 0 ; i < k ; ++i){
      pre.push_back(z);
      res.push_back(pre);
      func(ind+k,pre);
    }
  };
  vector<int> tmp;
  res.push_back(tmp);
  func(0,tmp);
  return res;
}

int Solutions::largestRectangleArea(vector<int> &height){
  stack<pair<int,int> > states;
  int maxz = 0;
  for(auto & x : height){
    if(states.empty()) {states.push(make_pair(x,1));continue;}
    int sum = 0 ;
    while(!states.empty() && states.top().first >= x){
      sum += states.top().second;
      maxz = max(maxz,sum*states.top().first);
      states.pop();
    }
    states.push(make_pair(x,sum+1));
  }
  int heights = 0;
  while(!states.empty()){
    heights += states.top().second;
    maxz =  max(states.top().first * heights, maxz);
    states.pop();
  }
  return maxz;
}
// This is faster than move check max approach because it multiplies less., well.. this is
// certainly messier.
int Solutions::maxArea(vector<int> &height){
  int i, j ;
  i = 0;
  j = height.size()-1;
  int maxarea = 0; 
  while(i<j){
    if(height[i] == height[j]){
      maxarea = max((j-i)*height[i],maxarea);
      int z = height[i];
      while(i<=j && height[i] <= z)++i;
      while(j>=i && height[j] <= z)--j;
      continue;
    }
    if(height[i] < height[j]){
      int z = height[i];
      maxarea = max((j-i)*z,maxarea);
      while(i<=j &&height[i]<=z) 
	++i;
      continue;
    }
    if(height[i] > height[j]){
      int z = height[j];
      maxarea = max((j-i)*z,maxarea);
      while(j>= i && height[j]<=z) 
	--j;
      continue;
    }
  }
  return maxarea;

}


vector<vector<int> > Solutions::combinationSum(vector<int> &candidates, int target) {
  vector<vector<int> > res;
  sort(candidates.begin(),candidates.end());
  function<void(int,int, vector<int>)> func = 
    [&target, &func, &res,&candidates](int ind,int presum, vector<int> pre){
    if(presum == target) {res.push_back(pre);return;}
    if(presum > target) return;
    if(ind == candidates.size()) return;
    int z = target - presum;
    func(ind+1,presum,pre);
    for(int ss = candidates[ind] ; ss <= z; ss+=candidates[ind]){
      pre.push_back(candidates[ind]);
      func(ind+1,presum+ss,pre);
    }
  };
  func(0,0,vector<int>());
  return res;
}

// the catch [1,1], target =1 should only output [[1]]
// so has to make sure, the number is in combination 
vector<vector<int> > Solutions::combinationSum2(vector<int> &num, int target){
  // DO NOT write int main() function
  sort(num.begin(),num.end());
  vector<vector<int> > res;
  function<void(int,int,vector<int>&)> func = 
    [&func,&num,&res](int i, int presum, vector<int>& hist){
    if(presum == 0) {res.push_back(hist); return;}
    if(presum < 0) return;
    for(int k = i ; k< num.size(); ++k){
      // this is the key, make sure only take first and skip the rest, 
      // especially 1,1,1,3,3 case, so you need to compare this one with 
      // the last, instead of the first element.
      if(i!=k && num[k]==num[k-1]) continue; 
      hist.push_back(num[k]);
      func(k+1, presum - num[k], hist);
      hist.pop_back();
    }
  };
  vector<int> hist;
  func(0,target,hist);
  return res;
}

vector<vector<int> > combinationSum2(vector<int> &num, int target){
  vector<vector<int> > res;
  sort(num.begin(), num.end());
  function<void(int,int,vector<int>)> func = 
    [&num,&target,&func,&res](int ind, int presum, vector<int> pre){
    if(presum == target){res.push_back(pre); return;}
    if(presum > target) return;
    if(ind == num.size()) return;
    func(ind+1,presum,pre);
    int s = num[ind];
    while(ind< num.size() && num[ind]==s){
      pre.push_back(s);
      presum += num[ind];
      ++ind;
    }
    func(ind,presum,pre);
  };
  func(0,0,vector<int>());
  return res;
}

int Solutions::numTrees(int n) {
  int res = 1;
  for(int j = 0 ; j < n ; ++j)
    res = res*(2*j+1)*2/(j+2);
  return res;
}
vector<TreeNode *> Solutions::generateTrees(int n){
  function< vector<TreeNode*> (int,int) > func = [&func](int s, int e){
    TreeNode* p = NULL;
    if(s>e) return vector<TreeNode*> (1,p);
    vector<TreeNode*> res;
    for(int i = s; i <=e ; ++i){
      auto left = func(s,i-1);
      auto right = func(i+1,e);
      for(auto x : left)
	for(auto y : right){
	  auto r = new TreeNode(i);
	  r->left = x;
	  r->right = y;
	  res.push_back(r);
	}
    }
    return res;
  };
  return func(1,n);
}
vector<TreeNode *> generateTrees(int n){
  function<TreeNode*(TreeNode*)> copyfunc = [&copyfunc]
    (TreeNode* root){
    TreeNode* p = NULL;
    if(root == NULL) return p;
    p = new TreeNode(root->val);
    p->left = copyfunc(root->left);
    p->right = copyfunc(root->right);
    return p;
  };
  function<void(TreeNode*)> deletex = [&deletex]
    (TreeNode* root){
    if(root == NULL) return;
    deletex(root->left);
    deletex(root->right);
    delete root;
  };
  vector<TreeNode*> res;
  function<vector<TreeNode*>(int,int)> generate = 
    [&generate,&copyfunc,&deletex] (int i, int j){
    TreeNode* tmp = NULL;
    vector<TreeNode*> resx;
    if(j<i) resx.push_back(tmp);
    for(int k = i; k <=j ; ++k){
      tmp = new TreeNode(k);
      auto rexleft = generate(i, k-1);
      auto rexrite = generate(k+1, j);
      for(auto x : rexleft)
	for(auto y : rexrite){
	  tmp = new TreeNode(k);
	  tmp->left = copyfunc(x);
	  tmp->right = copyfunc(y);
	  resx.push_back(tmp);
	}
      for(auto x: rexleft)
	deletex(x);
      for(auto y: rexrite)
	deletex(y);
    }
    return resx;
  };
  return generate(1,n);
}

int Solutions::minDistance(string word1, string word2){
  //meaning match index [i,j],step to match first i chars from word1 to first j chars from word2
  int DP[word1.size()+1][word2.size()+1];
  for(int i = 0 ; i < word1.size()+1; ++i)
    DP[i][0] = i;
  for(int i = 0 ; i < word2.size()+1; ++i)
    DP[0][i] = i;
  // delete current, replace or leave unchange, insert matching character, but match the rest.
  for(int j = 1 ; j < word2.size()+1; ++j)
    for(int i = 1 ; i < word1.size()+1; ++i)
      DP[i][j] = min({DP[i-1][j]+1, 
	    DP[i-1][j-1]+(word1[i-1] == word2[j-1]?0:1),
	    DP[i][j-1]+1});
  return DP[word1.size()][word2.size()];
}

int Solutions::maximalRectangle(vector<vector<char> > &matrix){ // Still O(mn) algorithm
  if(matrix.empty()) return 0;
  if(matrix.front().empty()) return 0;
  int m = matrix.size();
  int n = matrix.front().size();
  int H[m][n];
  int L[m][n];
  int R[m][n];
  // get vertical distance.
  for(int i = 0; i< m; ++i)
    for(int j = 0 ; j < n ; ++j)
      if(matrix[i][j] == '1')
	H[i][j] = i==0? 1 : (H[i-1][j] +1);
      else
	H[i][j] = 0;      

  // get distance from left;
  for(int j = 0 ; j < n ; ++j)
    for(int i = 0; i < m ; ++i)
      if(matrix[i][j] == '1')
	L[i][j] = j==0?1:(L[i][j-1] +1);
      else
	L[i][j] = 0;

  // get distance from right
  for(int j = n-1 ; j >=0 ; --j)
    for(int i = 0; i < m ; ++i)
      if(matrix[i][j] == '1')
	R[i][j] = j==n-1?1:(R[i][j+1] +1);
      else
	R[i][j] = 0;

  int maxarea = 0;  
  for(int j = 0 ; j < n ; ++j){
    int LL = L[0][j]==0?INT_MAX: L[0][j];
    int RR = R[0][j]==0?INT_MAX: R[0][j];
    maxarea = max(maxarea, (LL+RR-1)*H[0][j]);
    for(int i = 1 ; i < m ; ++i){
      if(matrix[i][j]=='1'){
	LL = min(LL,L[i][j]);
	RR = min(RR,R[i][j]);
	maxarea = max(maxarea, (LL+RR-1)*H[i][j]);
      }else{
	LL = INT_MAX;
	RR = INT_MAX;
      }
    }
  }
  return maxarea;
}

int Solutions::trap(int A[], int n){ // similar to max area.
  int i, j;
  i = 0 ; j = n - 1;
  int w = 0 ;
  while(i<j){
    if(A[i] <= A[j]){
      int z = A[i];
      while(i<=j && A[i]<=z)
	w += z-A[i++];
    }else{
      int z = A[j];
      while(i<=j && A[j]<=z)
	w += z-A[j--];
    }
  }
  return w;
}

string Solutions::multiply(string num1, string num2){
  std::reverse(num1.begin(),num1.end());
  std::reverse(num2.begin(),num2.end());
  string s;
  int l, k; 
  int carry = 0; 
  for(int i = 0; i < num1.size()+num2.size()-1 ; ++i){
    l = 0;
    int sum1 = carry;
    for(l = 0 ; l <= i ; ++l){
      if(l<num1.size() && i-l<num2.size())
	sum1 += (num1[l]-'0')*(num2[i-l]-'0');
    }
    s.push_back((sum1%10 +'0'));
    carry = sum1/10;
  }
  while(carry > 0){
    s.push_back((carry%10 +'0'));
    carry /=10;
  }
  std::reverse(s.begin(),s.end());
  while(s.front() == '0' && s.size()>1){
    s.erase(0,1);
  }
  return s;
}

bool Solutions::isNumber(const char *s){
  //regex r("((\\+|-)?[[:digit:]]+)(\\.[:digit:]+)?((e|E)((\\+|-)?)[:digit:]+)?");
  //return regex_match(s,r);
  enum inputs{space=0,dot=1,num=2,signs=3,exp=4,inv=5};
  enum state{start=0,integer=1,floats=2,exponent=3,exponentnum=4,spaceafternumb =5,
	     justdotted=6,dottedwnonumber=7,justsigned=8,signedafterexp=9};
  int Transition[][6] = {
    {0 , 7, 1, 8,-1,-1}, //from starts 0
    {5 , 6, 1,-1, 3,-1}, //from interger 1
    {5 ,-1, 2,-1, 3,-1}, //from floats 2
    {-1,-1, 4, 9,-1,-1}, //exponets=3
    {5 ,-1, 4,-1,-1,-1}, //exponentnum=4
    {5 ,-1,-1,-1,-1,-1}, //spaceafternum=5
    {5 ,-1, 2,-1, 3,-1}, //justdotted=6
    {-1,-1, 2,-1,-1,-1}, //dotted no number=7
    {-1, 7, 1,-1,-1,-1}, //just signed = 8
    {-1,-1, 4,-1,-1,-1}  //signedafterexp = 9
    };
  int stx = start;
  int i = 0 ;
  while(s[i] != '\0'){
    inputs y = inv;
    if(isdigit(s[i])) 
      y = num;
    if(s[i] == '.')
      y = dot;
    if(s[i] == 'e' || s[i] =='E')
      y = exp;
    if(s[i] == '-' || s[i] =='+')
      y = signs;
    if(isspace(s[i]))
      y = space;
    stx = Transition[stx][y];
    if(stx <0) return false;
    ++i;
  }
  return stx!=0 && stx!=3 && stx!=7 && stx!=8 && stx!=9;
	     
}

int Solutions::romanToInt(string s){
  unordered_map<string,int> d = 
    {{"I",1},
     {"IV",4},
     {"V",5},
     {"IX",9},
     {"X",10},
     {"XL",40},
     {"L",50},
     {"XC",90},
     {"C",100},
     {"CD",400},
     {"D",500},
     {"CM",900},
     {"M",1000}};
  int i = 0 ;
  int z = 0;
  while(i < s.size()){
    string t =s.substr(i,2);
    if(d.find(t) != d.end()){
      z += d[t];
      i+=2;
      continue;
    }
    t = s.substr(i,1);
    if(d.find(t) != d.end()) 
      t+= d[t];
    ++i;
  }
  return z;
}

string Solutions::intToRoman(int num){
  stringstream s;
  vector<pair<string,int> > d = 
    {{"I",1},
     {"IV",4},
     {"V",5},
     {"IX",9},
     {"X",10},
     {"XL",40},
     {"L",50},
     {"XC",90},
     {"C",100},
     {"CD",400},
     {"D",500},
     {"CM",900},
     {"M",1000}};
  std::reverse(d.begin(),d.end());
  int i = 0;
  while(num>0){
    int z = num/d[i].second;
    num %=d[i].second;
    while(z-- >0) s << d[i].first;
    ++i;
  }
  return s.str();
}

void Solutions::connectI(TreeLinkNode *root){
  if(root==NULL) return;
  function<void(TreeLinkNode*,TreeLinkNode*)> func =
    [&func](TreeLinkNode* left, TreeLinkNode* right){
    if(left==NULL) return;
    left->next = right;
    if(right==NULL) return;
    func(left->left , left->right);
    func(left->right, right->left);
    func(right->left, right->right);
  };
  if(root->left == NULL) connect(root->right);
  func(root->left,root->right);
}

void Solutions::connect(TreeLinkNode *root){
  if(root==NULL) return ;

  function<void(TreeLinkNode*,vector<TreeLinkNode*>&, bool,int)> getside
    =[&getside](TreeLinkNode* root, vector<TreeLinkNode*>& history, bool isleft,int level){
    if(root == NULL) return;
    if(level+1>history.size()){
      history.push_back(root);
    }
    if(isleft){
      getside(root->left,history,isleft,level+1);
      getside(root->right,history,isleft,level+1);
    }else{
      getside(root->right,history,isleft,level+1);
      getside(root->left,history,isleft,level+1);
    }
  };


  // this function connect two connected trees.
  function<void(TreeLinkNode*,TreeLinkNode*)>connecttwo = 
    [&getside](TreeLinkNode* left, TreeLinkNode* right){
    vector<TreeLinkNode*> leftn,rightn;
    getside(left,leftn,false,0);
    getside(right,rightn,true,0);
    int M = min(leftn.size(),rightn.size());
    for(int i = 0 ; i < M ; ++i){
      leftn[i]->next = rightn[i];
    }
  };
  function<void(TreeLinkNode*)> connectone = 
    [&connectone,&connecttwo](TreeLinkNode* root){
    if(root == NULL) return;
    connectone(root->left);
    connectone(root->right);
    connecttwo(root->left, root->right);
  };
  connectone(root);
}

char *Solutions::strStr(char *haystack, char *needle){
  // more efficient algorithm available.
  // recursive solution doesn't work for big judge.
  char* hayp = haystack;
  char* np   = needle;
  int p = 0;
  while(*(hayp+p) != '\0'){
    if(*np == '\0') return hayp;
    if(*(hayp+p) == *np){
      ++p; ++np;
    }else{
      np = needle;
      ++hayp;
      p = 0;
    }
  }
  hayp = *np == '\0' ? hayp : NULL;
  return hayp;
}

string Solutions::countAndSay(int n){
  string x = "1"; 
  function<string(string,int)> func = [&func](string s, int start){
    if(start == s.size()) return string();
    if(start == s.size()-1) return string("1") + s.back();
    int i = start+1;
    for(;s[i]==s[i-1];++i);
    return std::to_string(i-start) + s[i-1] + func(s,i);
  };
  for(int i = 1 ; i < n ; ++i){
    cout << x << endl;
    x = func(x,0);
  }
  return x;
}

int Solutions::divide(int dividend, int divisor){
  auto absi = [](int x){
    int mask = x >>31;
    return (x^mask)-mask;
  };
  int adivident = absi(dividend);
  int adivisor  = absi(divisor );
  cout << adivident << " " << adivisor << endl;
  int res = 0; 
  while(adivident >= adivisor){
    int c = adivisor;
    for(int i = 0 ; adivident >= c; c <<=1){
      adivident -= c;
      res += 1<<i;
      ++i;
    }
  }
  return ((dividend & divisor)>>31) ? (-res):res;
}

vector<int> Solutions::findSubstring(string S, vector<string>&L){
  unordered_map<string,int> mapz;
  for(auto &x : L){
    if(mapz.find(x) != mapz.end()) 
      mapz[x] +=1;
    else
      mapz[x] = 1;
  }
  int strl = L.front().size();
  function<bool(int,int,unordered_map<string,int>&)> func = [&func,&S,&strl]
    (int i,int needtofind, unordered_map<string,int>& nextmap){
    if(needtofind == 0) return true;
    auto tmps = S.substr(i,strl);
    if(nextmap.find(tmps) == nextmap.end() || nextmap[tmps] <= 0) return false;
    nextmap[tmps] -=1;
    return func(i+strl,needtofind-1,nextmap);
  };
  vector<int> res;
  for(int i = 0 ; i < S.size()-strl*L.size()+1; ++i){
    if(mapz.find(S.substr(i,strl)) == mapz.end()) continue;
    auto x = mapz;
    if(func(i,L.size(),x)) res.push_back(i);
  }
  return res;
}

vector<int> findSubstringI(string S, vector<string> &L){
  vector<int> res;
  if(L.empty()) return res;
  size_t strlen = L.back().size();
  // pre-calculate candidates.
  vector<int> pos(S.size(),-1); // positions of interest
  unordered_map<int,int> allwords; //word code + count;
  {
    unordered_map<string,int> mapx;
    for(int i = 0 ; i < L.size(); ++i){
      if(mapx.find(L[i])==mapx.end()){
	allwords[i]=1;
	mapx[L[i]] = i;
      }else{
	allwords[mapx[L[i]]] += 1;
	continue;
      }
    }
    for(int i = 0 ; i < S.size(); ++i){
      string sums= S.substr(i,strlen);
      if(mapx.find(sums) != mapx.end())
	pos[i] = mapx[sums];
    }
  }
  for(int i = 0 ; i < pos.size()-(strlen*L.size()-1); ++i){ // the minus range part makes a difference.
    if(pos[i]<0) continue;
    auto z = allwords;
    bool found = true;
    for(int j = 0 ; j < L.size(); ++j){
      int key = j*strlen + i;
      int q = pos[key];
      if(z[q]>0 )
	z[q] -=1;
      else{
	found =false; break;
      }
    }
    if(found) res.push_back(i);
  }
  return res;
  
}

bool Solutions::isMatchWild(const char *s, const char *p){
   //feasibility check. core algorithm is following.
   // without this large test wont pass, 
  {  int ms_max = 1;//size of *s
  const char* ss = s;
  while(*ss){ ++ms_max;++ss;}
  int np_max = 1;
  const char* pp = p;
  while(*pp){if(*pp!='*')++np_max;++pp;}
  if(ms_max < np_max) return false;
  }



  //core algorithm
  int ms = strlen(s);
  int ps = strlen(p);
  while(ps > 1 && *p == '*'){
    if(*(p+1) != '*')  break;
    ++p;
    --ps;
  }
  bool DP[2][ps+1];
  DP[0][0] = true;
  DP[1][0] = false;
  
  for(int i = 1; i <= ps ; ++i)
    DP[0][i] = (i==1 && p[0]=='*')? true : false;
  
  for(int i = 1; i <= ms ; ++i){
    int I2 = i%2;
    int In2 = (i-1)%2;
    for(int j = 1; j <= ps ; ++j){ 
      DP[I2][j] = 
	   (p[j-1] == '*'    && (DP[In2][j] ||DP[In2][j-1]||DP[I2][j-1]))
	|| (p[j-1] == '?'    && DP[In2][j-1])
	|| (p[j-1] == s[i-1] && DP[In2][j-1]);
    }
    // this is important, otherwise DP[0][0] is never updated.
    // the fact that I can get here means that I have at least one letter, so this is always false.
    DP[I2][0] = false; 
  }
  return DP[ms%2][ps];
}

vector<vector<int> > Solutions::permuteUnique(vector<int> &num){
  vector<vector<int> > res;
  vector<int> path;
  bool used[res.size()];
  for(int i = 0 ; i < num.size(); ++i)
    used[i] = false;
  sort(num.begin(),num.end());
  function<void(vector<int>&)> func = 
    [&func,&res,&num,&used](vector<int>& path){
    if(path.size() == num.size()){
      res.push_back(path);
      return;
    }
    for(int i = 0 ; i < num.size(); ++i){
      if(used[i]) continue; // if used not included
      if((i!=0 && num[i]==num[i-1] && used[i-1]))
	break;
      used[i] = true;
      path.push_back(num[i]);
      func(path);
      used[i] = false;
      path.pop_back();
    }
  };
  func(path);
  return res;
  
}

string Solutions::minWindow(string S, string T){
  unordered_map<char,int> needtofind;
  unordered_map<char,int> hasfound  ;
  for(int i = 0 ; i < T.size(); ++i)
    if(needtofind.find(T[i]) == needtofind.end())
      needtofind[T[i]] = 1;
    else
      needtofind[T[i]] += 1;
  int slow = 0;
  int s;
  int minlength = INT_MAX;
  int targetindex = T.size()-1;

  for(int i = 0 ; i < S.size(); ++i){
    if(hasfound.find(S[i]) == hasfound.end())
      hasfound[S[i]] = 1;
    else
      hasfound[S[i]] += 1;
    if(needtofind.find(S[i]) == needtofind.end()
       || hasfound[S[i]] > needtofind[S[i]] ){ // not in the wanted list or extra letter found.
      ++targetindex;
      continue;
    }
    if(i < targetindex) continue;
    while(needtofind.find(S[slow]) == needtofind.end()
	  || hasfound[S[slow]] > needtofind[S[slow]]){
      --hasfound[S[slow]];
      ++slow;
    }
    ++targetindex;
    if(targetindex - slow < minlength){
      minlength = targetindex - slow; 
      s = slow;
    }
    --hasfound[S[slow]];
    ++slow;
  }
  //cout << needtofind.bucket_count() << endl;
  if(minlength > S.size()) return string();
  return S.substr(s,minlength);
}

int numDistinct(string S, string T){
  // the question really is how many ways you can delete characters of S to get T.
  vector<int> DP(T.size()+1,0); 
  DP[T.size()] = 1; // means nothing more to match, there is one solution.
  for(int j = S.size()-1; j>=0; --j)
    for(int i = 0 ; i < T.size(); ++i)
      DP[i] += (S[j]==T[i])*DP[i+1];
  return DP[0];
}
bool Solutions::isPalindrome(string s){
  int i = 0;
  int j = s.size()-1;
  while(i<j){
    while(!isalpha(s[i]) && !isdigit(s[i]) && i < s.size()) ++i;
    while(!isalpha(s[j]) && !isdigit(s[j]) && j>= 0) --j;
    if(i>=j) break;
    if(tolower(s[i++]) != tolower(s[j--])) return false;
  }
  return true;
}
TreeNode* Solutions::buildTreePreOrder(vector<int> &preorder, vector<int> &inorder){
  // build a map from inorder seqence. 
  unordered_map<int,int> q;
  for(int i = 0 ; i < inorder.size() ; ++i ){
    q[inorder[i]] = i ;
  }
  int i = 0 ;
  function<TreeNode*(int,int)> func = [&i,&q,&func,&preorder](int left, int right){
    TreeNode* p = NULL;
    if(i==preorder.size()) return p;
    int x = preorder[i];
    int ind = q[x];
    if(ind >=right || ind <= left) return p;
    ++i;
    p = new TreeNode(x);
    p->left = func(left, ind);
    p->right = func(ind, right);
    
  };
  return func(-1,inorder.size());
}
TreeNode* Solutions::buildTreePostOrder(vector<int> &inorder, vector<int> &postorder){
  std::reverse(postorder.begin(),postorder.end());
  vector<int> preorder = postorder;
  unordered_map<int,int> q;
  for(int i = 0 ; i < inorder.size() ; ++i ){
    q[inorder[i]] = i ;
  }
  int i = 0 ;
  function<TreeNode*(int,int)> func = [&i,&q,&func,&preorder](int left, int right){
    TreeNode* p = NULL;
    if(i==preorder.size()) return p;
    int x = preorder[i];
    int ind = q[x];
    if(ind >=right || ind <= left) return p;
    ++i;
    p = new TreeNode(x);
    p->right = func(ind, right); // this two line switch order from preorder.
    p->left = func(left, ind);
  };
  return func(-1,inorder.size());
}
bool Solutions::searchRotate(int A[], int n, int target){
  int l = 0; 
  int r = n-1;
  int m;
  while(l <= r){
    while(l < r && A[l]==A[r] && A[l] != target){
      if(A[l] < target) ++l;
      if(A[r] > target) --r;
    }
    m = (l+r)/2;
    if(A[m]==target) return true;
    if(A[l] < A[r]){ // monotonic
      if(A[m] < target) 
	goto RIGHT;
      goto LEFT;
    }else{ // not monotonic
      if(target == A[r] || target == A[l]) return true;
      if(target > A[r] && target < A[l]) return false;
      if(target > A[l]){
	if(A[m] > target || A[m] < A[l]  )
	  goto LEFT;
	else 
	  goto RIGHT;
      }
      if(target < A[r]){
	if(A[m] < target || A[m] > A[r] )
	  goto RIGHT;
	else
	  goto LEFT;
      }
    }
  LEFT:
    r = m - 1; m = (r+l)/2; continue;
  RIGHT:
    l = m + 1; m = (r+l)/2; continue;
  }
  return A[m] == target;

}
int Solutions::maxProfitIII(vector<int> &prices){
  vector<int> history(prices.size(),0);
  int his = 0;
  int trough = INT_MAX;
  for(int i = 0 ; i< prices.size(); ++i){
    if(prices[i] < trough)
      trough = prices[i];
    his = max(prices[i] - trough, his);
    history[i] = his;
  }
  trough = INT_MIN;
  for(int i = prices.size()-1; i>=0; --i){
    if(prices[i] > trough)
      trough = prices[i];
    his = max(his, history[i] + trough - prices[i]);
  }
  return his;
}

bool Solutions::isScramble(string s1, string s2){
  if(s1.compare(s2)==0) return true;
  if(s1.size() != s2.size()) return false;
  int n = s1.size();
  bool DP[n][n][n];
  // DP(i,j,k), string1 at i, string2 at j or length k+1 is a scramble.
  for(int i = 0 ; i < n ; ++i)
    for(int j = 0 ; j < n ; ++j)
      DP[i][j][0] = (s1[i] == s2[j]);
  // then 
  for(int l = 1; l < n ; ++l) //l is the length of the match
    for(int i = 0; i < n - l ; ++i)
      for(int j = 0 ; j < n - l; ++j){
	DP[i][j][l] = false;
	for(int k = 0 ; k < l ; ++k) // if any of the cut is a binary tree we are good.
	  if((DP[i][j][k] && DP[i+k+1][j+k+1][l-1-k]) // no exchange needed.
	     || (DP[i][j-k+l][k] && DP[i+k+1][j][l-1-k])){
	     DP[i][j][l] = true;
	     break;
	  }
      }
  return DP[0][0][n-1];
}

vector<vector<string> > Solutions::partitionPalindrome(string s){
  vector<vector<string> > res;
  
  int len = s.size();
  //bool palin[len][len];
  vector<vector<bool> > palin(len,vector<bool>(len,false)); // cannot use array here, compiler error when pass in lambda. why?
  //memset(palin,0,len*len*sizeof(bool));
  for(int i = 0; i < len ; ++i)
    for(int j = 0 ; j < len ; ++j)
      palin[i][j] = false;

  for(int i = len-1; i >=0 ;--i)
    for(int j = i ; j < len; ++j)
      if(s[i] == s[j] &&(j-i < 2 || palin[i+1][j-1]))
	palin[i][j] = true;

  function<void(int,vector<string> ) > func = [&func,&res,&s,&palin]
    (int k,vector<string> his){
    int len = s.size();
    if(k==len) 
      res.push_back(his);
    for(int i = k; i < len; ++i)
      if(palin[k][i]){
  	his.push_back(s.substr(k,i-k+1));
  	func(i+1,his);
  	his.pop_back();
      }
  };
  
  vector<string> hist;
  func(0,hist);
  return res;
}
int Solutions::minCut(string s){
  int len = s.size();
  int DP[len+1];
  for(int i = 0 ; i <=len ; ++i)
    DP[i] = len-i;
  bool palin[len][len];
  memset(palin,0,len*len*sizeof(bool));
  for(int i = len-1; i >=0 ;--i)
    for(int j = i ; j < len; ++j)
      if(s[i] == s[j] &&(j-i < 2 || palin[i+1][j-1])){
	palin[i][j] = true;
	DP[i] = min(DP[i],DP[j+1]+1);
      }
  return DP[0]-1;
}


int minCutI(string s){
  unordered_set<int> oddindex;
  unordered_set<int> evenindex;
  for(int i = 0 ; i < s.size(); ++i){
    if(s[i]==s[i+1])
      evenindex.insert(i);
    if(i!=0 && i < s.size()-1 && s[i-1]==s[i+1])
      oddindex.insert(i);
  }
  
  auto checkpalindrome = [](string& s, int i, int j){
    while(i < j)
      if(s[i++] != s[j--]) 
	return false;
    
    return true;
  };
  int mincut = INT_MAX;
  function<void(int,int)> func = 
    [&func,&s,&checkpalindrome,&oddindex,&evenindex,&mincut](int index,int presum){
    // cout << index << " x " << presum << endl;
    if(presum > mincut) return; // no need to continue going down this road;
    if(checkpalindrome(s,index,s.size()-1)){
      mincut = min(mincut,presum);
      return;
    }
    for(int z = s.size()-2; z >= index; --z){
      if((z-index) % 2 == 0){ // odd index;
	if(z!=index && oddindex.count((z+index)/2)==0)
	  continue;
	if(checkpalindrome(s,index,z))
	  func(z+1,presum+1);
      }else{
	if(z!=index && evenindex.count((z+index)/2)==0)
	  continue;
	if(checkpalindrome(s,index,z))
	  func(z+1,presum+1);
      }
    }
  };
  func(0,0);
  return mincut;
}
UndirectedGraphNode * Solutions::cloneGraph(UndirectedGraphNode *node){
  if(node==NULL) return NULL;
  unordered_map<int,UndirectedGraphNode*> haveseen;
  queue<pair<UndirectedGraphNode*,UndirectedGraphNode*> > q;
  auto r = new UndirectedGraphNode(node->label);
  haveseen[node->label]=r;
  q.push(make_pair(r,node));
  while(!q.empty()){
    auto last = q.front(); q.pop();
    for(auto next : last.second->neighbors){
      auto p = haveseen.find(next->label);
      if(p==haveseen.end()){
	auto temp = new UndirectedGraphNode(next->label);
	last.first->neighbors.push_back(temp);
	haveseen[temp->label] = temp;
	q.push(make_pair(temp,next));
      }else{
	last.first->neighbors.push_back(p->second);
      }
    }
  }
  return r;
}
