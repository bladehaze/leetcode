#pragma once
#include<regex>
#include <cstring>
#include<queue>
#include<unordered_set>
#include<unordered_map>
#include<sstream>
#include<list>
#include<functional>
#include<string>
#include<map>
#include<vector>
#include <climits>
#include <set>
#include <array>
#include <algorithm>
#include <stack>
using namespace std;
    struct TreeLinkNode {
      TreeLinkNode *left;
      TreeLinkNode *right;
      TreeLinkNode *next;
    };
struct ListNode {
     int val;
     ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
};

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
struct Interval {
  int start;
  int end;
  Interval() : start(0), end(0) {}
  Interval(int s, int e) : start(s), end(e) {}
};

struct UndirectedGraphNode {
  int label;
  vector<UndirectedGraphNode *> neighbors;
  UndirectedGraphNode(int x) : label(x) {};
};

class Solutions{
public:
  static int reverse(int x);
  static bool isPalindrome(int x);
  static vector<string> letterCombinations(string digits);
  static int atoi(const char *str);
  static bool isMatch(const char *s, const char *p);//**, 
  static vector<vector<int> > generate(int numRows);
  static vector<int> getRow(int rowIndex);
  static bool isValidSudoku(vector<vector<char> > &board);
  static int searchInsert(int A[], int n, int target);
  static vector<int> searchRange(int A[], int n, int target);
  static int search(int A[], int n, int target);
  static ListNode *mergeKLists(vector<ListNode *> &lists);
  static bool isValid(string s);
  static bool isBalanced(TreeNode *root) ;
  static int maxDepth(TreeNode *root) ;
  static bool isSymmetric(TreeNode *root);
  static bool isSameTree(TreeNode *p, TreeNode *q) ;
  static vector<vector<int> > levelOrder(TreeNode *root);
  static vector<vector<int> > zigzagLevelOrder(TreeNode *root);
  static vector<int> plusOne(vector<int> &digits);
  static int removeElement(int A[], int n, int elem);
  static int removeDuplicates(int A[], int n);
  static int maxPathSum(TreeNode *root);
  static ListNode *removeNthFromEnd(ListNode *head, int n);
  static bool isValidBST(TreeNode *root);
  static string addBinary(string a, string b);
  static int sqrt(int x) ;
  static bool hasPathSum(TreeNode *root, int sum);
  static int minDepth(TreeNode *root);
  static vector<vector<int> > pathSum(TreeNode *root, int sum);
  static void flatten(TreeNode *root);
  static string longestCommonPrefix(vector<string> &strs) ;
  static int climbStairs(int n);
  static double pow(double x, int n);
  static bool isInterleave(string s1, string s2, string s3);
  static vector<string> restoreIpAddresses(string s);
  static void rotate(vector<vector<int> > &matrix) ;
  static ListNode *swapPairs(ListNode *head) ;
  static ListNode *mergeTwoLists(ListNode *l1, ListNode *l2);
  static int maxProfit(vector<int> &prices);
  static int maxProfitII(vector<int> &prices);
  static void recoverTree(TreeNode *root);
  static int minPathSum(vector<vector<int> > &grid);
  static int uniquePaths(int m, int n);
  static TreeNode *sortedArrayToBST(vector<int> &num);
  static vector<vector<int> > subsets(vector<int> &S);
  static string simplifyPath(string path);
  static int ladderLength(string start, string end, unordered_set<string> &dict);
  static vector<vector<string>> findLadders(string start, string end, unordered_set<string> &dict);
  static vector<vector<int> > levelOrderBottom(TreeNode *root);
  static void setZeroes(vector<vector<int> > &matrix);
  static int maxSubArray(int A[], int n);
  static void solve(vector<vector<char>> &board);
  static vector<vector<string> > solveNQueens(int n);
  static int totalNQueens(int n);
  static int jump(int A[], int n);
  static bool canJump(int A[], int n);
  static vector<int> spiralOrder(vector<vector<int> > &matrix);
  static void sortColors(int A[], int n);
  static vector<vector<int> > generateMatrix(int n);
  static void merge(int A[], int m, int B[], int n);
  static vector<string> generateParenthesis(int n);
  static int removeDuplicatesII(int A[], int n);
  static int longestValidParentheses(string s);
  static ListNode *deleteDuplicates(ListNode *head);
  static ListNode *deleteDuplicatesII(ListNode *head);
  static ListNode *rotateRight(ListNode *head, int k);
  static bool searchMatrix(vector<vector<int> > &matrix, int target);
  static vector<string> fullJustify(vector<string> &words, int L);
  static int lengthOfLastWord(const char *s);
  static vector<Interval> merge(vector<Interval> &intervals);
  static vector<Interval> insert(vector<Interval> &intervals, Interval newInterval);
  static int numDecodings(string s);
  static vector<int> grayCode(int n);
  static string longestPalindrome(string s);
  static int sumNumbers(TreeNode *root); // Sum Root to Leaf Numbers.
  static bool exist(vector<vector<char> > &board, string word);//word search
  static int minimumTotal(vector<vector<int> > &triangle);//Triangle
  static int longestConsecutive(vector<int> &num);
  static int firstMissingPositive(int A[], int n);
  static TreeNode *sortedListToBST(ListNode *head);
  static ListNode *partition(ListNode *head, int x);
  static vector<int> inorderTraversal(TreeNode *root);
  static vector<vector<int> > permute(vector<int> &num);
  static void nextPermutation(vector<int> &num);// stl library call,how to implement?
  static string getPermutation(int n, int k);
  // Aug 24
  static void solveSudoku(vector<string > &board);
  static bool isValidSudokuII(vector<string > &board);
  static int threeSumClosest(vector<int> &num, int target);
  static vector<vector<int> > threeSum(vector<int> &num);
  static ListNode *reverseKGroup(ListNode *head, int k);
  static vector<vector<int> > fourSum(vector<int> &num, int target);
  static ListNode *reverseBetween(ListNode *head, int m, int n);
  static int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) ;
  static vector<string> anagrams(vector<string> &strs);
  static vector<vector<int> > combine(int n, int k);
  static string minWindow(string S, string T);
  static vector<vector<int> > subsetsWithDup(vector<int> &S);
  static int largestRectangleArea(vector<int> &height);
  static int maxArea(vector<int> &height);
  static vector<vector<int> > combinationSum(vector<int> &candidates, int target) ;
  static vector<vector<int> > combinationSum2(vector<int> &num, int target);
  static int numTrees(int n);
  static vector<TreeNode *> generateTrees(int n);
  static int minDistance(string word1, string word2);
  static int maximalRectangle(vector<vector<char> > &matrix);
  static int trap(int A[], int n);
  static string multiply(string num1, string num2);
  static bool isNumber(const char *s);
  static int romanToInt(string s);
  static string intToRoman(int num);
  static void connectI(TreeLinkNode *root);
  static void connect(TreeLinkNode *root);
  static char* strStr(char *haystack, char *needle);
  static vector<vector<int> > permuteUnique(vector<int> &num);
  static string countAndSay(int n);
  static int divide(int dividend, int divisor);
  static vector<int> findSubstring(string S, vector<string> &L);
  static bool isMatchWild(const char *s, const char *p);
  static bool isPalindrome(string s);
  static TreeNode* buildTreePreOrder(vector<int> &preorder, vector<int> &inorder);
  static TreeNode* buildTreePostOrder(vector<int> &inorder, vector<int> &postorder);
  static bool searchRotate(int A[], int n, int target);
  static int maxProfitIII(vector<int> &prices);
  static int ConnectII(TreeNode *root);
  static bool isScramble(string s1, string s2);
  static vector<vector<string>> partitionPalindrome(string s);
  static int minCut(string s);
  static UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node);
  static bool isValidSudokuIII(vector<string > &board);
  //static string simplifyPath(string path);
private:
  static vector<string> lc(string digits,map<char,string> maps);
  static void findladdershelper(string & start, string& end, unordered_set<string> &dict, 
			      vector<string> &adjwordlist, vector<vector<int> > &adjlist,
				int& init, int& target);
} ;
