#include "algo.hpp"
#include <vector>

vector<pair<double,int> > merge(vector<pair<double,int> > l1, vector<pair<double,int> > l2){
  if(l1.empty()) return l2;
  if(l2.empty()) return l1;
  vector<pair<double,int> > res;
  int i,j;
  i = j = 0;
  double currentvalue = min(l1[0].first,l2[0].first)-1;
  bool first = true;
  while(i < l1.size() && j < l2.size()){ // insert the smaller item only if smaller item has a value bigger than currentvalue.
    if(l1[i].second <  l2[j].second){
      if(l1[i].first > currentvalue){ 
	res.push_back(l1[i]);
	currentvalue = l1[i].first;
      }
      ++i; 
      continue;
    } 
    if(l1[i].second ==  l2[j].second){
      auto x = l1[i].first < l2[j].first? l2[j]:l1[i];
      if(x.first > currentvalue){
	res.push_back(x); 
	currentvalue = x.first;
      }
      ++i; ++j;
      continue;
    }
    if(l1[i].second >  l2[j].second){
      if(l2[j].first > currentvalue) {
	res.push_back(l2[j]);
	currentvalue = l2[j].first;
      }
      ++j; 
      continue;
    }
  }
  if( i == l1.size())
    for( ; j<l2.size(); ++j){
      if(l2[j].first < currentvalue) continue; // since l1 l2 are already has increasing order in value, we dont need to check the rest.
      res.push_back(l2[j]);
    }
  if( j == l2.size())
    for( ; i<l1.size(); ++i){
      if(l1[i].first < currentvalue) continue;
      res.push_back(l1[i]);
    }
  return res;
}

double knapsack(vector<pair<double,int> > packages, int W){
  vector<pair<double,int> > cache;
  for(auto &x : packages){
    vector<pair<double,int> > tmp; 
    for(auto & y : cache){
      if(y.second + x.second > W) break;
      tmp.push_back(make_pair(x.first+y.first,x.second+y.second));
    }
    tmp.insert(tmp.begin(),x); // this is garanteed to be ordered.
    cache = merge(cache, tmp); // sorted with extra term removed. 
  }
  return cache.back().first;
}
