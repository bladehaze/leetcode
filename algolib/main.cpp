#include "algo.hpp"
#include <fstream>
#include <iostream>
#include <ctime>
using namespace std;

int main(){
  ifstream infile("knapsack_big.txt");// result: 4243395
  //ifstream infile("knapsack1.txt");// result: 2493893
  double a;
  int b;
  bool first = true;
  int W;
  vector<pair<double,int> > packs;
  while(infile >> a >> b){
    if(first){
      W = (int)a;
      first = false;
      continue;
    }
    packs.push_back(make_pair(a,b));
  }
  time_t tstart, tend; 
  tstart = time(0);
  cout << (int)knapsack(packs,W) << endl;
  tend = time(0); 
  cout << "It took "<< difftime(tend, tstart) <<" second(s)."<< endl;
  return 0;
}
