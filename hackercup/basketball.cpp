#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<algorithm>
using namespace std;

struct Player{
  string name;
  int per;
  int hei;
  int min;
  int rank;
  Player(vector<string> s){
    name = s[0];
    per = stoi(s[1]);
    hei = stoi(s[2]);
    min = 0;
  }
};

vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}
vector<Player> runoneteam(vector<Player> team, int M, int P){
  auto comparecourt = [](Player a, Player b){
    return a.min < b.min || (a.min == b.min && a.rank < b.rank);
  };
  auto comparebench = [](Player a, Player b){
    return a.min > b.min || (a.min == b.min && a.rank > b.rank);
  };
  vector<Player> court;
  vector<Player> bench;
  for(int i = 0 ; i < team.size() ; ++i){
    if(i < P)
      court.push_back(team[i]);
    else
      bench.push_back(team[i]);
  }
  if(bench.empty()) return team;
  make_heap(court.begin(),court.end(),comparecourt);
  make_heap(bench.begin(),bench.end(),comparebench);
  for(int i = 0 ; i < M; ++i){
    for(auto & p : court){
      ++p.min;
    }
    pop_heap(court.begin(),court.end(),comparecourt);
    auto exch = court.back();
    court.pop_back();
    pop_heap(bench.begin(),bench.end(),comparebench);
    auto sub  = bench.back();
    bench.pop_back();
    court.push_back(sub); push_heap(court.begin(),court.end(),comparecourt);
    bench.push_back(exch);push_heap(bench.begin(),bench.end(),comparebench);
  }
  return court;
}

string runcase(vector<Player> players, int N, int M, int P){
  sort(players.begin(),players.end(),
       [](Player a, Player b){return a.per > b.per || (a.per == b.per && a.hei > b.hei);});
  for(int i = 0 ; i < players.size(); ++i){
    players[i].rank = i+1;
  }
  vector<Player> team1;
  vector<Player> team2;
  for(int i = 0 ; i < players.size() ; ++i){
    if(i%2==0){
      team1.push_back(players[i]);
    }else{
      team2.push_back(players[i]);
    }
  }
  team1 = runoneteam(team1,M,P);
  team2 = runoneteam(team2,M,P);
  team1.insert(team1.end(),team2.begin(),team2.end());
  vector<string> oncourt;
  for(auto x : team1){
    oncourt.push_back(x.name);
  }
  sort(oncourt.begin(),oncourt.end(),[](string p,string q){
      p[0] = tolower(p[0]); q[0] = tolower(q[0]);
      return p < q;
    });

  return accumulate(oncourt.begin(),oncourt.end(),string(),[](string cur, string name){return cur + " " + name;});
}




int main(int argc, char** argv){
  //string filename = "basketball_game_example_input.txt";
  if(argc != 2) { cout << "inputs wrong" << endl;}
  string filename = argv[1];
  string line;
  ifstream file(filename);
  getline(file,line);
  int numberofcases = stoi(line);
  for(int i = 0 ; i < numberofcases ; ++i){
    int N,M,P; // N students, M minutes, P players in the game.
    getline(file,line);
    stringstream ss(line);
    ss >> N >> M >> P;
    vector<Player> players;
    for(int i = 0; i < N; ++i){
      getline(file,line);
      auto vstr = split(line);
      players.push_back(Player(vstr));
    }
    cout << "Case #" << (i+1) <<":"<< runcase(players,N,M,P) << endl;
  }
  
}
