#include <iostream>
#include <string>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <limits.h>
#include <unordered_map>
#include <unordered_set>
using namespace std;


//#define DEBUG

#define LineN 500
#ifdef DEBUG
#define mgetline(str) getline(file,str)
#else
#define mgetline(str) cin.getline(&str[0],LineN)
#endif


vector<string> split(string str, char del){
  stringstream ss(str);
  string item;
  vector<string> res;
  while(getline(ss,item,del))
    res.push_back(item);
  return res;
}

int minCost(vector<vector<int> >& nodes, vector<vector<int> >& conn){
  unordered_map<int,vector<int> > net;
  for(auto &x : conn){
    if(nodes[x[0]][0] == nodes[x[1]][0]) continue;
    net[x[0]].push_back(x[1]);
    net[x[1]].push_back(x[0]);
  }
  int mincost = INT_MAX; 
  function<void(int,int,unordered_set<int > &) > func
    = [&] (int city, int cost, unordered_set<int > & used){
    if(city == nodes.size()) { mincost = min(mincost,cost); return;}
    if(cost > mincost) return;
    if(used.find(city) != used.end() || net.find(city) == net.end()) {func(city+1,cost,used);return;}
    // attack.
    used.insert(city);
    func(city+1, cost + nodes[city][1], used);
    used.erase(city);
    // dont attack
    vector<int> cache;
    int thiscost = 0;
    for(auto & c : net[city]){
      if(used.find(c) != used.end()) continue;
      cache.push_back(c);
      used.insert(c);
      thiscost += nodes[c][1];
    }
    func(city+1,thiscost+cost, used);
    for(auto & c : cache)
      used.erase(c);
  };
  unordered_set<int> setz;
  func(0,0,setz);
  return mincost;
}


int main(){
#ifdef DEBUG
  ifstream file;
  file.open("input001.txt", std::ifstream::in);
#endif
  int ln = LineN;
  string line;
  line.resize(ln);
  //cin.getline(&line[0],ln);
  mgetline(line);

  int num = stoi(line);
  for(int i = 0 ; i < num; ++i){
    //cin.getline(&line[0],ln);
    mgetline(line);
    int N, M;
    stringstream ss(line);
    ss >> N >> M;
    vector<vector<int> > nodes;
    for(int j = 0; j < N; ++j){
      //cin.getline(&line[0],ln);
      mgetline(line);
      auto xl = split(line, ' ');
      nodes.push_back({xl[0] == "A",stoi(xl[1])});
    }
    vector<vector<int> > conn;
    for(int k = 0 ; k < M; ++k){
      //cin.getline(&line[0],ln);
      mgetline(line);
      auto xl = split(line, ' ');
      conn.push_back({stoi(xl[0])-1,stoi(xl[1])-1});
    }
    cout << minCost(nodes,conn) << endl;
  }

  return 0;
}
