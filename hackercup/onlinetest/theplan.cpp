#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <functional>
#include <unordered_set>
#include <limits.h>
#include <unordered_map>
using namespace std;

vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}

int solve(vector<vector<int> > cities, vector<vector<int> > roads){
  vector<vector<int> > conflict;
  for(auto & x: roads){
    if(cities[x[0]][0] == cities[x[1]][0]) continue;
    conflict.push_back(x);
  }
  if(conflict.empty()) return 0;
  unordered_map<int,unordered_set<int> > paths; // city - cities;
  for(int i = 0 ; i < conflict.size(); ++i){
    paths[conflict[i][0]].insert(conflict[i][1]);
    paths[conflict[i][1]].insert(conflict[i][0]);
    
  }
  int mincost = INT_MAX;

  function<void(int,unordered_set<int>&,int)> func = [&](int index, unordered_set<int>& killed, int currentcost){
    if(index == cities.size()) {mincost = min(mincost,currentcost); return;}
    if(currentcost > mincost) return;
    if(killed.find(index) != killed.end()){
      func(index+1,killed,currentcost);
      return;
    }
    // if keep current, then I should attack all other nodes.
    vector<int> deled;
    int totalc = 0;
    for(auto & c : paths[index]){
      if(killed.find(c) != killed.end()) continue;
      totalc += cities[c][1];
      deled.push_back(c);
      killed.insert(c);
    }
    func(index+1, killed,currentcost+totalc);
    for(auto& c : deled){
      killed.erase(c);
    }
    // if delete current, then I can just go on.
    killed.insert(index);
    func(index+1,killed, currentcost+cities[index][1]);
    killed.erase(index);
  };
  unordered_set<int> kill;
  func(0,kill,0);
  return mincost;
}

int main(int argc, char** argv){
  //string filename = "input000.txt";
  if(argc != 2) { cout << "inputs wrong" << endl;}
  string filename = argv[1];

  string line;
  ifstream file(filename);
  getline(file,line);
  int cases = stoi(line);
  for(int i = 0 ; i < cases; ++i){
    getline(file,line);
    int N, M;
    stringstream ss(line);
    ss >> N >> M;
    vector<vector<int> > cities; // cities, index city index, 0 A, 1, B, int cost.
    for(int i = 1 ; i <= N; ++i){ // cities.
      getline(file,line);
      auto o = split(line);
      cities.push_back({o[0] == "A"? 0 : 1, stoi(o[1])});
    }
    vector<vector<int> > roads;
    for(int i = 0 ; i < M ; ++i){
      getline(file,line);
      auto o = split(line);
      roads.push_back({stoi(o[0])-1,stoi(o[1])-1});
    }
    cout << solve(cities,roads) << endl;
  }

  return 0;
}
