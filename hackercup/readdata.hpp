#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
using namespace std;

// read input compute.
// 4 //number ofcases
// 1 2 3 //number parameter
//

void readandcompute(string filename, function<void(vector<string>,vector<int>) > func){
  ifstream file(filename);
  string line;

  // first line, number of cases.
  getline(file,line);
  int numofcases = stoi(line);
  for(int i = 0 ; i < numofcases; ++i){
    getline(file,line);
    stringstream ss(line);
    int N, M, P;
    ss >> N >> M >> P;
    vector<string> tmp;
    for(int i = 0 ; i < N; ++i){
      getline(file,line);
      tmp.push_back(line);
    }
    vector<int> pa = {N,M,P};
    func(tmp,pa);
  }

}
