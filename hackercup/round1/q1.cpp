#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<algorithm>
#include<math.h>
using namespace std;
long long findm(string valid, long long num){
  long double x1 = num*((valid.size() - 1.0)/valid.size()) + 1;
  x1 = log(x1)/ log((double)valid.size());
  return x1;
}

long long calcsum(long double len, long double k){
  return (pow(len, k+1) - 1)/(len - 1);
}

string getlabel(string valid, long long num){
  if(valid.size() == 1) return string((int)num, valid[0]);
  long long k = findm(valid, num);
  long long ind = num - calcsum(valid.size(), k) ;
  long long factor = pow((long double) valid.size(), (long double) k);
  string res;
  while(factor > 0){
    res.push_back(valid[ind/factor]);
    ind %= factor;
    factor /= valid.size();
  }
  return res;
}

vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}
int main(int argc, char** argv){
  //string filename = "labelmaker_example_input.txt";
  if(argc != 2) { cout << "inputs wrong" << endl;}
  string filename = argv[1];
  string line;
  ifstream file(filename);
  getline(file,line);
  int numberofcases = stoi(line); // num of cases.
  for(int i = 0 ; i < numberofcases ; ++i){
    long long N; 
    string S;
    getline(file,line);
    stringstream ss(line);
    ss >> S >> N; // each cases parameter
    cout << "Case #" << (i+1) <<": " << getlabel(S, N) << endl;
  }
  
}
