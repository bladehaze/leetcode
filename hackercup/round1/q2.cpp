#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<algorithm>
#include<math.h>
#include<limits.h>
using namespace std;
int returnp(int N, int K, int C){
  int minp = INT_MAX;
  for(int e = max(0, N - K); e < N; ++e){
    int tmpp = e; // at least this much extra attemps.
    int covered = (N-e) * (K / (N-e)); // after cover. 
    //cout << covered << endl;
    //break;
    if(covered >= C){ // if covered greater than K, then we are set.
      if(tmpp + C < minp){
	minp = tmpp + C;
      }
      //break;
    }else{
      int newempty =  N - e - K % ( N - e);
      //cout << newempty << endl;
      int tmpK = newempty + tmpp + C;
      if(tmpK < minp){
	minp = tmpK;
      }
    }
  }
  return minp;
}

vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}
int main(int argc, char** argv){
  string filename = "coins_game_example_input.txt";
  //if(argc != 2) { cout << "inputs wrong" << endl;}
  //string filename = argv[1];
  string line;
  ifstream file(filename);
  getline(file,line);
  int numberofcases = stoi(line); // num of cases.
  for(int i = 0 ; i < numberofcases ; ++i){
    int N, K, C; 
    getline(file,line);
    stringstream ss(line);
    ss >> N >> K >> C; // each cases parameter
    cout << "Case #" << (i+1) <<": " << returnp(N, K, C) << endl;
  }
  
}
