#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<algorithm>
#include<math.h>
// #include<limits.h>
// #define INT_MIN 0
using namespace std;
char Empty = '.';
char Car   = '#';

int lengthofque(vector<string> pl, int M, int N){
  // top down meaning using only d and right, what's the maximum num of people in the que to get to i,j
  vector<vector<int> > td(M, vector<int>(N,0)); // top down.
  td[0][0] = 1; 
  for(int i = 1 ; i < M; ++i){
    if(pl[i][0] == Car) break;
    td[i][0] = (td[i-1][0] + 1);
  }
  for(int j = 1 ; j < N; ++j){
    if(pl[0][j] == Car) break;
    td[0][j] = (td[0][j-1] + 1);
  }
  for(int i = 1; i < M; ++i)
    for(int j = 1; j < N; ++j){
      if(pl[i][j] == Car) continue;
      td[i][j] = max(td[i-1][j], td[i][j-1]);
      td[i][j] += td[i][j] == 0? 0 : 1;
    }
  // bottom up, from this point on only use down right, what's the maximum people in the que.
  vector<vector<int> > bu(M, vector<int>(N,0)); // bottom up.
  bu[M-1][N-1] = pl[M-1][N-1] == Car ? 0 : 1 ;
  for(int i = M-2; i >=0; --i){
    if(pl[i][N-1] == Car) continue;
    bu[i][N-1] = bu[i+1][N-1] + 1;
  }
  for(int j = N-2; j >=0; --j){
    if(pl[M-1][j] == Car) continue;
    bu[M-1][j] = bu[M-1][j+1] + 1;
  }
  for(int i = M-2; i>=0; --i)
    for(int j = N-2; j>=0; --j){
      if(pl[i][j] == Car) 
	continue;
      bu[i][j] = max(bu[i+1][j],bu[i][j+1]) + 1;
    }
  // if only go left is allowed.
  int maxq = 0;
  // inorder to go left at this point,we need i > 0 and td[i-1][j] > 0
  // I probably don't need dp table here.
  for(int i = 1; i < M; ++i){
    int lastv = 0; 
    if(pl[i][0] != Car){
      lastv = i == M - 1 ? 0 : bu[i+1][0] + 1;
      if(td[i-1][0] > 0){
	maxq = max(maxq, lastv + td[i-1][0]);
      }
    }
    for(int j = 1; j < N; ++j){
      if(pl[i][j] == Car) { lastv = 0; continue;}
      lastv = max(lastv+1, (i == M - 1 ? 0 : bu[i+1][j] ) + 1 ); 
      if(td[i-1][j] > 0){
	maxq = max(maxq, lastv + td[i-1][j]);
      }
    }
  }

  // if only go up is allowed.
  for(int j = 1; j < N; ++j){
    int lastv = 0;
    if(pl[0][j] != Car){
      lastv = (j == N - 1 ? 0 : bu[0][j+1]) + 1;
      if(td[0][j-1] > 0){
	maxq = max(maxq, lastv + td[0][j-1]);
      }
    } 
    for(int i = 1 ; i < M; ++i){
      if(pl[i][j] == Car) { lastv = 0; continue;}
      lastv = max(lastv + 1, (j == N - 1 ? 0 : bu[i][j+1]) + 1);
      if(td[i][j-1] > 0){
	maxq = max(maxq, lastv + td[i][j-1]);
      }
    }
  }
  return maxq;
}

vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}
int main(int argc, char** argv){
  //string filename = "aaaaaa_example_input.txt";
  if(argc != 2) { cout << "inputs wrong" << endl;}
  string filename = argv[1];
  string line;
  ifstream file(filename);
  getline(file,line);
  int numberofcases = stoi(line); // num of cases.
  for(int i = 0 ; i < numberofcases ; ++i){
    int M, N;
    getline(file,line);
    stringstream ss(line);
    ss >> M >> N ; // m, row, n, column.
    vector<string> pl;
    for(int j = 0; j < M ; ++j){
      getline(file,line);
      pl.push_back(line);
    }
    cout << "Case #" << (i+1) <<": " << lengthofque(pl, M, N) << endl;
    pl.clear();
  }
  
}
