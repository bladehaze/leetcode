#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<sstream>
#include<algorithm>
#include<math.h>
#include<unordered_set>
#include<limits.h>
using namespace std;
double eps = 1e-2;
vector<int> primes = 
    {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,101,103,107,    109,113,127,131,137,139,149,151,157,163,167,173};
unordered_set<int> primeset =  {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,101,103,107,    109,113,127,131,137,139,149,151,157,163,167,173};

int getnum(double x){
  if( abs(x) < eps) return 1;
  if(abs((int)x - x) < eps) return x;
  else return (int)x + 1;
}
unordered_set<int> primeFactors(int n){
  int i = 0;
  unordered_set<int> set;
  while(primes[i] <= n){
    if( n % primes[i] == 0){
      set.insert(primes[i]);
    }
    while(n % primes[i] == 0){
      n /= primes[i];
    }
    ++i;
  }
  return set;
}
int gcd(int a, int b){
  if(a < b) return gcd(b,a);
  a %= b;
  if(a == 0) 
    return b;
  else
    return gcd(b,a);
}
int getnext(int a, int K){
  //  if(a%K == 0) return a + K;
  return (a/K)*(K+1);
}

int extra(vector<int> ages, int K){
  vector<int> res;
  for(int i = 0 ; i < ages.size(); ++i){
    
  }
}


int extra(vector<int> ages, int K){ // N number of childs,
  int N = ages.size();
  sort(ages.begin(), ages.end());
  vector<int> constr; 
  for(auto & x : ages)
    constr.push_back(getnum(x*1.0/K));
  int incbase = 0;
  for(int i = 0 ; i < constr.size(); ++i){
    incbase += K*constr[i] - ages[i];
  }


  vector<int> upper; // upper bound of search. 
  int last = 0;
  for(auto& x : constr){
    auto itor = upper_bound(primes.begin(), primes.end(), max(x,last));
    upper.push_back(*itor);
    last = *itor;
  }
  int range = *max_element(upper.begin(), upper.end());
  // now want to find the smallest increase coprime chain that satisfies the constraint
  // n_i >= constr[i]
  vector<bool> mark(range+1, true); // true means can use.
  mark[0] = false;
  int mininc = INT_MAX;
  function<void(int,vector<bool>,int,int)> func = [&](int ind, vector<bool> newmark, int presum, int prenum){//ind is the person index, newmark is number available
    if(presum > mininc) return;
    if(ind == ages.size()){ mininc = min(mininc, presum); return;}
    int minx = max(constr[ind],prenum);
    int maxx = range;
    vector<bool>::iterator it;
    int z = minx;
    while((it = find_if(newmark.begin()+z,newmark.begin()+maxx+1,[](bool x){return x;})) != newmark.begin() + maxx + 1){
      z = it - newmark.begin(); // candidate
      auto s = primeFactors(z);
      vector<bool> next = newmark;
      for(auto p : s){
	int i = 1;
	while(p*i < next.size()){
	  next[p*i] = false;
	  ++i;
	}
      }
      func(ind + 1, next, presum + z - minx, z);
      *it = false;
    }
  };

  func(0,mark,0,0);
  return mininc*K + incbase;
}


vector<string> split(string s){
  stringstream ss(s);
  string item;
  vector<string> res;
  while(getline(ss,item,' ')){
    res.push_back(item);
  }
  return res;
}
int main(int argc, char** argv){
  string filename = "preventing_alzheimers_example_input.txt";
  //if(argc != 2) { cout << "inputs wrong" << endl;}
  //string filename = argv[1];
  string line;
  ifstream file(filename);
  getline(file,line);
  int numberofcases = stoi(line); // num of cases.
  for(int i = 0 ; i < numberofcases ; ++i){
    int N, K;
    getline(file,line);
    stringstream ss(line);
    ss >> N >> K ; // 
    vector<int> ages;
    getline(file,line);
    auto z = split(line);
    for(int j = 0; j < N ; ++j){
      ages.push_back(stoi(z[j]));
    }
    cout << "Case #" << (i+1) <<": " << extra(ages, K) << endl;
    ages.clear();
  }
  return 0;
}
