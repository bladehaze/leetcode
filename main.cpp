#include<iostream>
#include "Solutions.hpp"
#include "PolymorphismClass.hpp"
using namespace std;


class myplus{
  static int add( int x,  int y){
    return x+y;
  }
};

template<class T>
T big(T a, T b){return a>b?a:b;}

template<class T, class B>
B bigger(T a, T b){return a>b?a:b;}

template<class T, class B>
T big(B a, T b){return a>b?a:b;}

typedef int (*DoThis)(const char *);
int func(const char * s){
  int i = 0;
  while(*(s+i) != '\0')
    ++i;
  return i;
}

int main(){

  {
    //SHOW(myplus::operater()(1,2));
  }

  SHOW("Sudoku solver");
  {
    //["..9748...","7........",".2.1.9...","..7...24.",".64.1.59.",".98...3..","...8.3.2.","........6","...2759.."] 
    vector<string> board;
//board.push_back("519748632");
    board.push_back("..9748...");
    board.push_back("7........");
    board.push_back(".2.1.9...");
    board.push_back("..7...24.");
    board.push_back(".64.1.59.");
    board.push_back(".98...3..");
    board.push_back("...8.3.2.");
    board.push_back("........6");
    board.push_back("...2759..");
    //Solutions::solveSudoku(board);
    if(Solutions::isValidSudokuIII(board))
      cout << "ISVALID" << endl;
    for(auto & p : board)
      cout << p << endl;
  }


  int x = -1234;
  cout << Solutions::reverse(x) << endl;
  int y = 123432;
  if(Solutions::isPalindrome(y)){
    cout << "Yes" << endl;
  }else{
    cout << "No" << endl;
  }
  string xx = "23";
  vector<string> re = Solutions::letterCombinations(xx);
  for(auto & s : re)
    cout << s << endl;

  //------------------
  int s = (int)INT_MAX;
  cout << s/100 << endl;
  int sdfa = Solutions::atoi("     +004500");
  cout << sdfa << endl;
  
  //------------------
  if(Solutions::isMatch("cab","c**b"))
    cout <<"YES"<<endl;
  else
    cout <<"No"<<endl;

  string xstr = "abcddd";
  size_t found = xstr.find('b');
  cout << xstr.substr(found) << endl;

  size_t p;
  if((p = xstr.find('c')) != string::npos){
    cout << p << endl;
  }
  
  //-------------------
  auto pp = Solutions::generate(5);
  {
    SHOW("Derived pointer call");
  Derived* pc = new Derived();
  pc->Run(); //RUN is virtual 
  pc->RUNX();//RUNX is not virtual
  delete pc;
  SHOW("BASE pointer call");
  Base* ppx = new Derived();
  ppx->Run();
  ppx->RUNX();
  delete ppx;

  ppx = NULL;
  delete ppx;
  }
  SHOW("\n");
  //------------------- 
  {
  int xxe;
  int *px = &xxe;
  int **pppx = &px;

  **pppx = 3;

  cout << xxe << endl;
  }
  {
  DoThis sthtodo = func;
  int xxe = sthtodo("Hello!");
  cout << "length of string " << xxe << endl;
  }
  {
    const void* pvoid;
    double *pDouble;
    pDouble = static_cast<double*>(const_cast<void*>( pvoid));
  }


  {
     try
     {
          throw('c' );
     }
     catch( char& )
     {
          SHOW( "catch #1\n" );
     }
     catch( short& )
     {
          SHOW( "catch #2\n" );
     }
     catch( int& )
     {
          SHOW( "catch #3\n" );
     }
     catch( long& )
     {
          SHOW( "catch #4\n" );
     }

  }
  
  SHOW("Next");
  {
    int A[] = {1,2,3,3,4,5};
    vector<int> res = Solutions::searchRange(A, 6, 3);
    SHOW(res.size());
    SHOW(res[0]);
    SHOW(res[1]);
  }

  {
    string te = "()";
    if(Solutions::isValid(te))
      SHOW("YES");
    else
      SHOW("NO");
  }

  // {
  //   int z = Solutions::sqrt(2);
  //   SHOW(z);
  // }
  // {
  //   double z = Solutions::pow(1.00001,123456);
  //   SHOW(z);
  // }
    SHOW("interleaf");
  {
    string a = "bbbbbabbbbabaababaaaabbababbaaabbabbaaabaaaaababbbababbbbbabbbbababbabaabababbbaabababababbbaaababaa";
    string b = "babaaaabbababbbabbbbaabaabbaabbbbaabaaabaababaaaabaaabbaaabaaaabaabaabbbbbbbbbbbabaaabbababbabbabaab";
    string c = "babbbabbbaaabbababbbbababaabbabaabaaabbbbabbbaaabbbaaaaabbbbaabbaaabababbaaaaaabababbababaababbababbbababbbbaaaabaabbabbaaaaabbabbaaaabbbaabaaabaababaababbaaabbbbbabbbbaabbabaabbbbabaaabbababbabbabbab";

    bool z= Solutions::isInterleave(a,b,c);
    SHOW(z);
  }
  {
    string s ="4";
    SHOW(s.substr(1));
    Solutions::restoreIpAddresses("1234");
    
  }
  SHOW("Roate");
  {
    vector<vector<int> > input ;
    int r1[] = {1,2}; vector<int> rr1(r1,r1+sizeof(r1)/sizeof(r1[0]));
    int r2[] = {3,4}; vector<int> rr2(r2,r2+sizeof(r2)/sizeof(r2[0]));
    input.push_back(rr1);
    input.push_back(rr2);

    cout << input[0][0] <<  input[0][1] << endl;
    cout <<  input[1][0] <<  input[1][1] << endl;
    SHOW("\n");
    Solutions::rotate(input);
    cout << input[0][0] <<  input[0][1] << endl;
    cout <<  input[1][0] <<  input[1][1] << endl;
    
  }

  SHOW("find ladders");
  {
    unordered_set<string> sset({"a","b","c"});
    //unordered_set<string> sset({"hot","cog","dot","dog","hit","lot","log"});
    //unordered_set<string> sset({"hot","dog","dot"});
    //unordered_set<string> sset({"hot","dog","hog","dot"});
    //sset.insert("hot");
    //sset.insert("dot");
    //sset.insert("dog");
    //for(auto p : sset)
    //  SHOW(p);
    //int p = Solutions::ladderLength("hot","dog",sset);
    auto res = Solutions::findLadders("a","c",sset);
    SHOW(res.size());
    for(auto z : res.front())
      cout << z << endl;
    //int p = Solutions::ladderLength("a","c",sset);
    //SHOW(p);

  }
  SHOW("Mark Board X and O");
  {
    vector<vector<char> > board;
    Solutions::solve(board);
  }
  SHOW("9 queens");
  {
    //auto sol = Solutions::solveNQueens(2);
    int x = -2030;
    cout << (x&(-x)) << endl;
    auto to = Solutions::totalNQueens(12);
    cout << "TOTAL solution number" << to << endl;
  }
  SHOW("jump");
  {

    int A[] = {1,2,3};
    int z1 = Solutions::jump(A,3);
    cout << "STEPS " << z1 << endl;
  }
  SHOW("Spiral");
  {
    vector<int> coef(2,1);
    vector<vector<int> > z;
    z.push_back(coef);
    Solutions::spiralOrder(z);
  }
  SHOW("sortcolors");
  {
    int A[2] = {1,0};
    Solutions::sortColors(A,2);
    cout << A[0] << " " << A[1] << endl;
  }
  SHOW("remove duplicates");
  {
    int A[6] = {1,1,1,2,2,3};
    int p = Solutions::removeDuplicatesII(A,6);
    cout << p << endl;
  }
  SHOW("Square search");
  {
    vector<vector<int> > matrix;
    vector<int> a(1,1);
    matrix.push_back(a);
    Solutions::searchMatrix(matrix,0);
  }
  SHOW("Insert Interval");
  {
    
    Interval x(1,5);
    Interval y(6,8);
    vector<Interval> res;
    res.push_back(x);
    res.push_back(y);
    Interval z(5,6);
    Solutions::insert(res,z);
  }
  SHOW("fullyjustify");
  {
    vector<string> s;
    s.push_back("a");
    s.push_back("b");
    s.push_back("c");
    s.push_back("d");
    s.push_back("e");
    Solutions::fullJustify(s,3);
  }

  SHOW("longest palindrome");
  {
    string p = Solutions::longestPalindrome("civilwartestingwhetherthatnaptionoranynartionsoconceivedandsodedicatedcanlongendureWeareqmetonagreatbattlefiemldoftzhatwarWehavecometodedicpateaportionofthatfieldasafinalrestingplaceforthosewhoheregavetheirlivesthatthatnationmightliveItisaltogetherfangandproperthatweshoulddothisButinalargersensewecannotdedicatewecannotconsecratewecannothallowthisgroundThebravelmenlivinganddeadwhostruggledherehaveconsecrateditfaraboveourpoorponwertoaddordetractTgheworldadswfilllittlenotlenorlongrememberwhatwesayherebutitcanneverforgetwhattheydidhereItisforusthelivingrathertobededicatedheretotheulnfinishedworkwhichtheywhofoughtherehavethusfarsonoblyadvancedItisratherforustobeherededicatedtothegreattdafskremainingbeforeusthatfromthesehonoreddeadwetakeincreaseddevotiontothatcauseforwhichtheygavethelastpfullmeasureofdevotionthatweherehighlyresolvethatthesedeadshallnothavediedinvainthatthisnationunsderGodshallhaveanewbirthoffreedomandthatgovernmentofthepeoplebythepeopleforthepeopleshallnotperishfromtheearth");
    cout << p << endl;
  }
  SHOW("Search Word");
  {
    vector<char> q('a',2);
    vector<vector<char> > board; board.push_back(q);
    Solutions::exist(board,"aa");
  }
  SHOW("next Permutation");
  {
    string s = Solutions::getPermutation(2,1);
    cout << s << endl;
  }

  SHOW("three Sum");
  {
    int x[] = {-4,-2,1,-5,-4,-4,4,-2,0,4,0,-2,3,1,-5,0};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    // res.push_back(1);
    // res.push_back(-1);
    // res.push_back(-1);
    // res.push_back(0);
    auto resx =  Solutions::threeSum(res);
    cout << resx.size() << endl;

    cout << "===============" << endl;
    for(auto & y :resx){
      for(auto & z : y){
	cout << z ;
      } 
      cout << endl;
    }
  }

  SHOW("closes 3 sum");
  {
    int x[] = {1,1,1,1};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    cout << Solutions::threeSumClosest(res,0) << endl;

  }
  SHOW("Combinations");
  {
    auto res = Solutions::combine(2,1);
    SHOW(res.size());
  }
  SHOW("subset II");
  {
    int x[] = {1,2};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    auto resx = Solutions::subsetsWithDup(res);
    cout <<"SSize " <<  resx.size() << endl;
  }
  SHOW("largest rectangle");
  {
    int x[] = {2,1,5,6,2,3};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    auto z = Solutions::largestRectangleArea(res);
    cout << z << endl;
  }

  SHOW("simplify path");
  {
    auto si = Solutions::simplifyPath("/..");
    cout << si << endl;
  }
  SHOW("string multiplication");{
    auto ss = Solutions::multiply("9133","0");
    cout << ss << endl;
  }
  SHOW("str str");
  {
    char* z = Solutions::strStr("a","a");
  }
  SHOW("4 sum");
  {
    //int x[] = {-4,-1,-1,0,1,2};
    //int x[] = {1,0,-1,0,-2,2};
    int x[] = {-2,-1,0,0,1,2};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    auto tx = Solutions::fourSum(res,-1);
    cout << tx.size() << endl;
  }
  SHOW("count and say");
  {
    auto ss = Solutions::countAndSay(6);
    cout << ss << endl;
  }
  SHOW("Division ");{
    //    int a = Solutions::divide(-2147483648,-2147483648);
    //SHOW(a);
  }
  SHOW("find substring");
  {
    string s = "barfoothefoobarman";
    vector<string> dd = {"foo","bar"};
    auto q = Solutions::findSubstring(s,dd);
    cout << endl;
    for(auto x : q){
      cout << x << endl;
    }
  }
  SHOW("ismatchwild");
  {
    bool res = Solutions::isMatchWild("bbbaab","a**?***");
    bool mygod;
    bool mygodness[10];
    if(mygod){
      cout << "default is true, this is bizzar" << endl;
      cout << "default value is " << mygod << endl;
      for(int i = 0 ; i < 10 ; ++i)
	cout << mygodness[i] << endl;
    }
  }
  SHOW("unique permutation");
  {
    //vector<int> d(1,1);
    int x[] = {1,1,2};
    vector<int> d(x,x+sizeof(x)/sizeof(x[0]));
    auto res = Solutions::permuteUnique(d);
    for(auto x0 : res){
      for(auto y : x0)
	cout << y ;
      cout << endl;
    }
  }
  SHOW("isnumber");
  {
    
  }
  SHOW("minWindow");{
    auto ss = Solutions::minWindow("acbbaca","aba");
    
  }
  SHOW("search root repeat");{
    int A[] = {1,1,3,1};
    auto x= Solutions::searchRotate(A,4,3);
    if(x)
      cout << "found" << endl;
  }
  SHOW("hello world");
  {
    int A[] = {2,1,2,0,1};
    vector<int> B(A,A+sizeof(A)/sizeof(A[0]));
    int x = Solutions::maxProfitIII(B);
  }
  SHOW("max rectangle");
  {
    
  }
  SHOW("min cut");
  {
    int z = Solutions::minCut("cabababcbc");
    cout << z << endl;
  }

    SHOW("combinations sums");
  {
    int x[] = {1,1,1,3,3,5};
    vector<int> res(x,x+sizeof(x)/sizeof(x[0]));
    auto s = Solutions::combinationSum2(res,8);
    cout << s.size() << endl;
    
  }

}

